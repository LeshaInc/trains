// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::iter::{once, Once};

use rendy::{
    command::{
        CommandBuffer, CommandPool, Family, IndividualReset, InvalidState, NoSimultaneousUse,
        OneShot, PendingState, PrimaryLevel, QueueType, Submit, Transfer,
    },
    factory::Factory,
    frame::Frames,
    graph::{
        gfx_acquire_barriers, gfx_release_barriers, GraphContext, ImageAccess, Node, NodeBuffer,
        NodeDesc, NodeImage, NodeSubmittable,
    },
    hal::{
        command::ImageCopy,
        format::Aspects,
        image::{Access, Offset, Usage},
        memory::{Barrier, Dependencies},
        pso::PipelineStage,
        Backend,
    },
    memory::Dynamic,
    resource::{
        Extent, ImageInfo, Layout, SubresourceLayers, SubresourceRange, Tiling, ViewCapabilities,
    },
};

use failure::Error;

use super::{graph::GraphData, CHUNK_IMAGE_FORMAT, CHUNK_IMAGE_KIND, CHUNK_SIZE};

#[derive(Debug, Default)]
pub struct DownloadDesc;

impl<B: Backend> NodeDesc<B, GraphData<B>> for DownloadDesc {
    type Node = Download<B>;

    fn images(&self) -> Vec<ImageAccess> {
        vec![ImageAccess {
            access: Access::SHADER_READ,
            usage: Usage::TRANSFER_SRC,
            layout: Layout::TransferSrcOptimal,
            stages: PipelineStage::TRANSFER,
        }]
    }

    fn build(
        self,
        ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        family: &mut Family<B, QueueType>,
        _queue: usize,
        _data: &GraphData<B>,
        buffers: Vec<NodeBuffer>,
        mut images: Vec<NodeImage>,
    ) -> Result<Self::Node, Error> {
        assert_eq!(images.len(), 1);
        assert!(buffers.is_empty());

        let command_pool = factory
            .create_command_pool::<IndividualReset>(&family)?
            .with_capability::<Transfer>()
            .unwrap();

        let src_image = images.remove(0);

        Ok(Download {
            command_pool,
            command_buffers: Vec::with_capacity(ctx.frames_in_flight as _),
            src_image,
        })
    }
}

#[derive(Debug)]
pub struct Download<B: Backend> {
    command_pool: CommandPool<B, Transfer, IndividualReset>,
    command_buffers:
        Vec<CommandBuffer<B, Transfer, PendingState<InvalidState>, PrimaryLevel, IndividualReset>>,
    src_image: NodeImage,
}

impl<'a, B: Backend> NodeSubmittable<'a, B> for Download<B> {
    type Submittable = Submit<B, NoSimultaneousUse>;
    type Submittables = Once<Submit<B, NoSimultaneousUse>>;
}

impl<B: Backend> Node<B, GraphData<B>> for Download<B> {
    type Desc = DownloadDesc;
    type Capability = Transfer;

    fn run(
        &mut self,
        ctx: &GraphContext<B>,
        factory: &Factory<B>,
        data: &GraphData<B>,
        _frames: &Frames<B>,
    ) -> <Self as NodeSubmittable<'_, B>>::Submittables {
        let cbuf = if self.command_buffers.len() == ctx.frames_in_flight as usize {
            unsafe { self.command_buffers.remove(0).mark_complete().reset() }
        } else {
            self.command_pool.allocate_buffers(1).remove(0)
        };

        let image = factory
            .create_image(
                ImageInfo {
                    kind: CHUNK_IMAGE_KIND,
                    levels: 1,
                    format: CHUNK_IMAGE_FORMAT,
                    tiling: Tiling::Linear,
                    view_caps: ViewCapabilities::empty(),
                    usage: Usage::SAMPLED | Usage::TRANSFER_DST,
                },
                Dynamic,
            )
            .unwrap();

        let mut cbuf = cbuf.begin(OneShot, ());
        let mut encoder = cbuf.encoder();

        let src_image_res = ctx.get_image(self.src_image.id).unwrap();

        let (mut stages, mut barriers) = gfx_acquire_barriers(ctx, None, Some(&self.src_image));

        stages.start |= PipelineStage::TRANSFER;
        stages.end |= PipelineStage::TRANSFER;

        let subresource_range = SubresourceRange {
            aspects: Aspects::COLOR,
            levels: 0..1,
            layers: 0..1,
        };

        barriers.push(Barrier::Image {
            states: (Access::empty(), Layout::Undefined)
                ..(Access::TRANSFER_WRITE, Layout::TransferDstOptimal),
            target: image.raw(),
            families: None,
            range: subresource_range.clone(),
        });

        unsafe { encoder.pipeline_barrier(stages, Dependencies::empty(), barriers) };

        let params = ImageCopy {
            src_subresource: SubresourceLayers {
                aspects: Aspects::COLOR,
                level: 0,
                layers: 0..1,
            },
            src_offset: Offset::ZERO,
            dst_subresource: SubresourceLayers {
                aspects: Aspects::COLOR,
                level: 0,
                layers: 0..1,
            },
            dst_offset: Offset::ZERO,
            extent: Extent {
                width: CHUNK_SIZE,
                height: CHUNK_SIZE,
                depth: 1,
            },
        };

        unsafe {
            encoder.copy_image(
                src_image_res.raw(),
                self.src_image.layout,
                image.raw(),
                Layout::Undefined,
                Some(params),
            );
        }

        let (mut stages, mut barriers) = gfx_release_barriers(ctx, None, Some(&self.src_image));

        stages.start |= PipelineStage::TRANSFER;
        stages.end |= PipelineStage::BOTTOM_OF_PIPE;

        barriers.push(Barrier::Image {
            states: (Access::TRANSFER_WRITE, Layout::TransferDstOptimal)
                ..(Access::empty(), Layout::ShaderReadOnlyOptimal),
            target: image.raw(),
            families: None,
            range: subresource_range,
        });

        unsafe { encoder.pipeline_barrier(stages, Dependencies::empty(), barriers) };

        let cbuf = cbuf.finish();
        let (submit, cbuf) = cbuf.submit_once();
        self.command_buffers.push(cbuf);

        data.queue
            .push((data.current_chunk_id, image.into()))
            .unwrap();

        once(submit)
    }

    unsafe fn dispose(mut self, factory: &mut Factory<B>, _data: &GraphData<B>) {
        self.command_pool.free_buffers(
            self.command_buffers
                .into_iter()
                .map(|cb| cb.mark_complete()),
        );

        factory.destroy_command_pool(self.command_pool);
    }
}
