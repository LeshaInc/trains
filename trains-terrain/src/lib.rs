// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#[macro_use]
extern crate derivative;

#[macro_use]
extern crate shred_derive;

pub mod chunk;
pub mod download;
pub mod graph;
pub mod pass;

mod config;

pub use self::config::WorldgenConfig;

use std::collections::HashSet;

use rendy::{
    graph::Graph,
    hal::{
        format::{Aspects, Format, Swizzle},
        Backend,
    },
    resource::{Handle, Image, ImageView, ImageViewInfo, Kind, SubresourceRange, ViewKind},
};

use crossbeam::queue::ArrayQueue;
use specs::prelude::*;

use trains_core::gpu::{GpuFactory, GpuFamilies};

use self::{
    chunk::{ChunkId, ChunkMap},
    graph::{build_graph, GraphData},
};

pub const CHUNK_SIZE: u32 = 1000;
pub const CHUNK_IMAGE_KIND: Kind = Kind::D2(CHUNK_SIZE, CHUNK_SIZE, 1, 1);
pub const CHUNK_IMAGE_FORMAT: Format = Format::R16Uint;

pub struct LoadedChunk<B: Backend> {
    pub image: Handle<Image<B>>,
    pub image_view: Handle<ImageView<B>>,
}

#[derive(Derivative)]
#[derivative(Default(bound = ""))]
pub struct Terrain<B: Backend> {
    loaded_chunks: ChunkMap<LoadedChunk<B>>,
    scheduled_chunks: HashSet<ChunkId>,
}

impl<B: Backend> Terrain<B> {
    pub fn get_chunk<I: Into<ChunkId>>(&self, id: I) -> Option<&LoadedChunk<B>> {
        self.loaded_chunks.get(id.into())
    }

    pub fn schedule_chunk<I: Into<ChunkId>>(&mut self, id: I) {
        self.scheduled_chunks.insert(id.into());
    }

    fn insert_chunk(&mut self, id: ChunkId, data: LoadedChunk<B>) {
        self.loaded_chunks.insert(id, data);
    }
}

pub struct TerrainLoadingSystem<B: Backend> {
    graph: Option<Graph<B, GraphData<B>>>,
    graph_data: GraphData<B>,
    config: WorldgenConfig,
}

impl<B: Backend> TerrainLoadingSystem<B> {
    pub fn new() -> TerrainLoadingSystem<B> {
        TerrainLoadingSystem {
            graph: None,
            graph_data: GraphData {
                queue: ArrayQueue::new(2),
                current_chunk_id: ChunkId::new(0, 0),
            },
            config: WorldgenConfig::default(),
        }
    }
}

impl<B: Backend> Default for TerrainLoadingSystem<B> {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(SystemData)]
pub struct Data<'a, B: Backend> {
    terrain: Write<'a, Terrain<B>>,
    factory: WriteExpect<'a, GpuFactory<B>>,
    families: WriteExpect<'a, GpuFamilies<B>>,
    config: Read<'a, WorldgenConfig>,
}

impl<'a, B: Backend> System<'a> for TerrainLoadingSystem<B> {
    type SystemData = Data<'a, B>;

    fn run(&mut self, mut data: Self::SystemData) {
        if *data.config != self.config {
            self.config = *data.config;
            log::debug!("Worldgen config changed, rebuilding graph");
            log::debug!("New config: {:#?}", self.config);

            let instant = std::time::Instant::now();

            self.graph
                .take()
                .unwrap()
                .dispose(&mut data.factory, &self.graph_data);

            data.terrain.loaded_chunks.clear();

            self.graph = Some(
                build_graph(
                    &mut data.factory,
                    &mut data.families,
                    self.config,
                    &self.graph_data,
                )
                .unwrap(),
            );

            log::debug!("elapsed {:?}", instant.elapsed());
        }

        while let Ok((id, image)) = self.graph_data.queue.pop() {
            log::debug!("Loaded chunk {:?}", id);

            let image_view = data
                .factory
                .create_image_view(
                    image.clone(),
                    ImageViewInfo {
                        view_kind: ViewKind::D2,
                        format: CHUNK_IMAGE_FORMAT,
                        swizzle: Swizzle::NO,
                        range: SubresourceRange {
                            aspects: Aspects::COLOR,
                            levels: 0..1,
                            layers: 0..1,
                        },
                    },
                )
                .unwrap()
                .into();

            let new = LoadedChunk { image, image_view };
            data.terrain.scheduled_chunks.remove(&id);
            data.terrain.insert_chunk(id, new);
        }

        let to_load = match data.terrain.scheduled_chunks.iter().next() {
            Some(&chunk_id) => chunk_id,
            None => return,
        };

        log::debug!("Scheduling chunk {:?}", to_load);

        self.graph_data.current_chunk_id = to_load;
        self.graph
            .as_mut()
            .unwrap()
            .run(&mut data.factory, &mut data.families, &self.graph_data);
    }

    fn dispose(self, world: &mut World) {
        let mut factory = WriteExpect::<GpuFactory<B>>::fetch(world);
        self.graph.unwrap().dispose(&mut factory, &self.graph_data);
    }

    fn setup(&mut self, world: &mut World) {
        Self::SystemData::setup(world);

        let mut factory = WriteExpect::<GpuFactory<B>>::fetch(world);
        let mut families = WriteExpect::<GpuFamilies<B>>::fetch(world);

        self.graph =
            Some(build_graph(&mut factory, &mut families, self.config, &self.graph_data).unwrap());
    }
}
