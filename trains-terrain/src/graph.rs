// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use rendy::{
    command::Families,
    factory::Factory,
    graph::{
        render::{RenderGroupBuilder, SimpleGraphicsPipelineDesc},
        Graph, GraphBuilder, Node, NodeDesc,
    },
    hal::Backend,
    resource::{Handle, Image},
};

use crossbeam::queue::ArrayQueue;
use failure::Fallible;

use crate::{
    chunk::ChunkId,
    download::Download,
    pass::{ErosionDesc, NoisePipelineDesc},
    WorldgenConfig, CHUNK_IMAGE_FORMAT, CHUNK_IMAGE_KIND,
};

pub struct GraphData<B: Backend> {
    pub queue: ArrayQueue<(ChunkId, Handle<Image<B>>)>,
    pub current_chunk_id: ChunkId,
}

pub fn build_graph<B: Backend>(
    factory: &mut Factory<B>,
    families: &mut Families<B>,
    config: WorldgenConfig,
    data: &GraphData<B>,
) -> Fallible<Graph<B, GraphData<B>>> {
    let instant = std::time::Instant::now();
    let mut gb = GraphBuilder::new().with_frames_in_flight(2);

    let noise_image = gb.create_image(CHUNK_IMAGE_KIND, 1, CHUNK_IMAGE_FORMAT, None);

    let gen_noise = gb.add_node(
        NoisePipelineDesc::new(config)
            .builder()
            .into_subpass()
            .with_color(noise_image)
            .into_pass(),
    );

    if config.enable_erosion {
        let erosion = gb.add_node(
            ErosionDesc::new(config)
                .builder()
                .with_image(noise_image)
                .with_dependency(gen_noise),
        );

        gb.add_node(
            Download::builder()
                .with_image(noise_image)
                .with_dependency(erosion),
        );
    } else {
        gb.add_node(
            Download::builder()
                .with_image(noise_image)
                .with_dependency(gen_noise),
        );
    }

    let graph = gb.build(factory, families, data)?;
    log::debug!("Built worldgen frame graph in {:?}", instant.elapsed());

    Ok(graph)
}
