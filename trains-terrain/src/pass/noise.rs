// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::mem::{size_of, transmute};

use rendy::{
    command::{QueueId, RenderPassEncoder},
    factory::Factory,
    graph::{
        render::{Layout, SimpleGraphicsPipeline, SimpleGraphicsPipelineDesc},
        GraphContext, NodeBuffer, NodeImage,
    },
    hal::{
        pso::{DepthStencilDesc, Specialization, SpecializationConstant},
        Backend,
    },
    resource::{DescriptorSetLayout, Handle},
    shader::{ShaderSet, ShaderSetBuilder, SpecConstantSet, SpirvReflection},
};

use failure::Fallible;
use trains_shadermgr::{include_shader, Shader};

use crate::{graph::GraphData, WorldgenConfig};

const VERTEX: Shader = include_shader!(vertex, "noise.vert");
const FRAGMENT: Shader = include_shader!(fragment, "noise.frag");

lazy_static::lazy_static! {
    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&VERTEX).unwrap()
        .with_fragment(&FRAGMENT).unwrap();

    static ref SHADER_REFLECTION: SpirvReflection = SHADERS.reflect().unwrap();
}

#[derive(Debug)]
pub struct NoisePipelineDesc {
    config: WorldgenConfig,
}

impl NoisePipelineDesc {
    pub fn new(config: WorldgenConfig) -> NoisePipelineDesc {
        NoisePipelineDesc { config }
    }
}

impl<B: Backend> SimpleGraphicsPipelineDesc<B, GraphData<B>> for NoisePipelineDesc {
    type Pipeline = NoisePipeline;

    fn load_shader_set(&self, factory: &mut Factory<B>, _data: &GraphData<B>) -> ShaderSet<B> {
        let data = unsafe {
            let mut vec = transmute::<_, Vec<u8>>(vec![self.config.noise]);
            vec.set_len(size_of::<WorldgenConfig>());
            vec.into()
        };

        SHADERS
            .build(
                factory,
                SpecConstantSet {
                    fragment: Some(Specialization {
                        constants: vec![
                            SpecializationConstant {
                                id: 0,
                                range: 24..25,
                            },
                            SpecializationConstant { id: 1, range: 0..4 },
                            SpecializationConstant { id: 2, range: 4..8 },
                            SpecializationConstant {
                                id: 3,
                                range: 8..12,
                            },
                            SpecializationConstant {
                                id: 4,
                                range: 12..16,
                            },
                            SpecializationConstant {
                                id: 5,
                                range: 16..20,
                            },
                            SpecializationConstant {
                                id: 6,
                                range: 20..24,
                            },
                        ]
                        .into(),
                        data,
                    }),
                    ..Default::default()
                },
            )
            .unwrap()
    }

    fn depth_stencil(&self) -> Option<DepthStencilDesc> {
        None
    }

    fn layout(&self) -> Layout {
        SHADER_REFLECTION.layout().unwrap()
    }

    fn build(
        self,
        _ctx: &GraphContext<B>,
        _factory: &mut Factory<B>,
        _queue: QueueId,
        _data: &GraphData<B>,
        _buffers: Vec<NodeBuffer>,
        _images: Vec<NodeImage>,
        _set_layouts: &[Handle<DescriptorSetLayout<B>>],
    ) -> Fallible<Self::Pipeline> {
        Ok(NoisePipeline)
    }
}

#[derive(Debug)]
pub struct NoisePipeline;

impl<B: Backend> SimpleGraphicsPipeline<B, GraphData<B>> for NoisePipeline {
    type Desc = NoisePipelineDesc;

    fn draw(
        &mut self,
        _layout: &B::PipelineLayout,
        mut encoder: RenderPassEncoder<B>,
        _index: usize,
        _data: &GraphData<B>,
    ) {
        unsafe {
            encoder.draw(0..6, 0..1);
        }
    }

    fn dispose(self, _factory: &mut Factory<B>, _data: &GraphData<B>) {}
}
