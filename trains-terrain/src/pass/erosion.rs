// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{
    iter::{once, Once},
    mem::{size_of, transmute},
    ops::Range,
};

use rendy::{
    command::{
        CommandBuffer, CommandPool, Compute, ExecutableState, Family, MultiShot, NoIndividualReset,
        PendingState, PrimaryLevel, QueueType, SimultaneousUse, Submit,
    },
    descriptor::{DescriptorSetLayoutBinding, DescriptorType},
    factory::Factory,
    frame::Frames,
    graph::{
        gfx_acquire_barriers, gfx_release_barriers, GraphContext, ImageAccess, Node, NodeBuffer,
        NodeDesc, NodeImage, NodeSubmittable,
    },
    hal::{
        format::{Aspects, Swizzle},
        image::{Access, Usage},
        memory::Dependencies,
        pso::{
            BasePipeline, ComputePipelineDesc, Descriptor, DescriptorSetWrite, EntryPoint,
            PipelineCreationFlags, PipelineStage, ShaderStageFlags, Specialization,
            SpecializationConstant,
        },
        Backend, Device,
    },
    resource::{
        DescriptorSet, DescriptorSetLayout, Escape, Handle, ImageView, ImageViewInfo, Layout,
        SubresourceRange, ViewKind,
    },
    shader::Shader as _,
};

use failure::Error;
use trains_shadermgr::{include_shader, Shader};

use crate::{GraphData, WorldgenConfig};

const COMPUTE: Shader = include_shader!(compute, "erosion.comp");

#[derive(Debug)]
pub struct ErosionDesc {
    config: WorldgenConfig,
}

impl ErosionDesc {
    pub fn new(config: WorldgenConfig) -> ErosionDesc {
        ErosionDesc { config }
    }
}

impl<B: Backend> NodeDesc<B, GraphData<B>> for ErosionDesc {
    type Node = Erosion<B>;

    fn images(&self) -> Vec<ImageAccess> {
        vec![ImageAccess {
            access: Access::SHADER_READ | Access::SHADER_WRITE,
            usage: Usage::STORAGE,
            layout: Layout::General,
            stages: PipelineStage::COMPUTE_SHADER,
        }]
    }

    fn build(
        self,
        ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        family: &mut Family<B, QueueType>,
        _queue: usize,
        _data: &GraphData<B>,
        buffers: Vec<NodeBuffer>,
        mut images: Vec<NodeImage>,
    ) -> Result<Self::Node, Error> {
        assert_eq!(images.len(), 1);
        assert!(buffers.is_empty());

        let module = unsafe { COMPUTE.module(factory)? };

        let set_layout = Handle::from(factory.create_descriptor_set_layout(vec![
            DescriptorSetLayoutBinding {
                binding: 0,
                ty: DescriptorType::StorageImage,
                count: 1,
                stage_flags: ShaderStageFlags::COMPUTE,
                immutable_samplers: false,
            },
        ])?);

        let pipeline_layout = unsafe {
            factory.device().create_pipeline_layout(
                Some(set_layout.raw()),
                None::<(ShaderStageFlags, Range<u32>)>,
            )?
        };

        let data = unsafe {
            let mut vec = transmute::<_, Vec<u8>>(vec![self.config.erosion]);
            vec.set_len(size_of::<WorldgenConfig>());
            vec.into()
        };

        let specialization = Specialization {
            constants: vec![
                SpecializationConstant { id: 0, range: 0..4 },
                SpecializationConstant { id: 1, range: 4..8 },
                SpecializationConstant {
                    id: 2,
                    range: 8..12,
                },
                SpecializationConstant {
                    id: 3,
                    range: 12..16,
                },
                SpecializationConstant {
                    id: 4,
                    range: 16..20,
                },
                SpecializationConstant {
                    id: 5,
                    range: 20..24,
                },
                SpecializationConstant {
                    id: 6,
                    range: 24..25,
                },
                SpecializationConstant {
                    id: 7,
                    range: 25..26,
                },
            ]
            .into(),
            data,
        };

        let pipeline = unsafe {
            factory.device().create_compute_pipeline(
                &ComputePipelineDesc {
                    shader: EntryPoint {
                        entry: "main",
                        module: &module,
                        specialization,
                    },
                    layout: &pipeline_layout,
                    flags: PipelineCreationFlags::empty(),
                    parent: BasePipeline::None,
                },
                None,
            )?
        };

        unsafe { factory.destroy_shader_module(module) };

        let image = images.remove(0);
        let image_res = ctx.get_image(image.id).unwrap();
        let image_view = factory.create_relevant_image_view(
            image_res.clone(),
            ImageViewInfo {
                view_kind: ViewKind::D2,
                format: image_res.format(),
                swizzle: Swizzle::NO,
                range: SubresourceRange {
                    aspects: Aspects::COLOR,
                    levels: 0..0,
                    layers: 0..1,
                },
            },
        )?;

        let descriptor_set = factory.create_descriptor_set(set_layout.clone())?;

        unsafe {
            factory
                .device()
                .write_descriptor_sets(Some(DescriptorSetWrite {
                    set: descriptor_set.raw(),
                    binding: 0,
                    array_offset: 0,
                    descriptors: Some(Descriptor::Image(image_view.raw(), image.layout)),
                }));
        }

        let mut command_pool = factory
            .create_command_pool::<NoIndividualReset>(&family)?
            .with_capability::<Compute>()
            .unwrap();

        let command_buffer = command_pool.allocate_buffers::<PrimaryLevel>(1).remove(0);
        let mut command_buffer = command_buffer.begin(MultiShot(SimultaneousUse), ());

        let mut encoder = command_buffer.encoder();

        encoder.bind_compute_pipeline(&pipeline);

        unsafe {
            encoder.bind_compute_descriptor_sets(
                &pipeline_layout,
                0,
                Some(descriptor_set.raw()),
                None,
            );

            let (stages, barriers) = gfx_acquire_barriers(ctx, None, &images);
            encoder.pipeline_barrier(stages, Dependencies::empty(), barriers);

            encoder.dispatch(100, 1, 1);

            let (stages, barriers) = gfx_release_barriers(ctx, None, &images);
            encoder.pipeline_barrier(stages, Dependencies::empty(), barriers);
        }

        let (submit, command_buffer) = command_buffer.finish().submit();

        Ok(Erosion {
            set_layout,
            pipeline_layout,
            pipeline,
            descriptor_set,
            submit,
            image_view,
            command_pool,
            command_buffer,
        })
    }
}

#[derive(Debug)]
pub struct Erosion<B: Backend> {
    set_layout: Handle<DescriptorSetLayout<B>>,
    pipeline_layout: B::PipelineLayout,
    pipeline: B::ComputePipeline,
    descriptor_set: Escape<DescriptorSet<B>>,
    submit: Submit<B, SimultaneousUse>,
    image_view: ImageView<B>,
    command_pool: CommandPool<B, Compute>,
    command_buffer:
        CommandBuffer<B, Compute, PendingState<ExecutableState<MultiShot<SimultaneousUse>>>>,
}

impl<'a, B: Backend> NodeSubmittable<'a, B> for Erosion<B> {
    type Submittable = &'a Submit<B, SimultaneousUse>;
    type Submittables = Once<&'a Submit<B, SimultaneousUse>>;
}

impl<B: Backend> Node<B, GraphData<B>> for Erosion<B> {
    type Desc = ErosionDesc;
    type Capability = Compute;

    fn run(
        &mut self,
        _ctx: &GraphContext<B>,
        _factory: &Factory<B>,
        _data: &GraphData<B>,
        _frames: &Frames<B>,
    ) -> <Self as NodeSubmittable<'_, B>>::Submittables {
        once(&self.submit)
    }

    unsafe fn dispose(mut self, factory: &mut Factory<B>, _data: &GraphData<B>) {
        drop(self.submit);
        self.command_pool
            .free_buffers(Some(self.command_buffer.mark_complete()));
        factory.destroy_command_pool(self.command_pool);
        factory.destroy_compute_pipeline(self.pipeline);
        factory.destroy_pipeline_layout(self.pipeline_layout);
        factory.destroy_relevant_image_view(self.image_view);
    }
}
