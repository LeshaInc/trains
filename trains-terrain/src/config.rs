// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct WorldgenConfig {
    pub noise: NoiseConfig,
    pub erosion: ErosionConfig,
    pub enable_erosion: bool,
}

impl Default for WorldgenConfig {
    fn default() -> WorldgenConfig {
        WorldgenConfig {
            noise: Default::default(),
            erosion: Default::default(),
            enable_erosion: true,
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
#[repr(C)]
pub struct NoiseConfig {
    pub scale: f32,
    pub initial_weight: f32,
    pub scale_mul: f32,
    pub weight_mul: f32,
    pub exponent: f32,
    pub multiplier: f32,
    pub num_octaves: u8,
}

impl Default for NoiseConfig {
    fn default() -> NoiseConfig {
        NoiseConfig {
            scale: 3.0,
            initial_weight: 0.5,
            scale_mul: 2.0,
            weight_mul: 0.35,
            exponent: 3.0,
            multiplier: 0.7,
            num_octaves: 10,
        }
    }
}

#[derive(Debug, PartialEq, Copy, Clone)]
#[repr(C)]
pub struct ErosionConfig {
    pub inertia: f32,
    pub sediment_capacity_factor: f32,
    pub deposit_speed: f32,
    pub erosion_speed: f32,
    pub gravity: f32,
    pub evaporate_speed: f32,
    pub brush_size: u8,
    pub max_lifetime: u8,
}

impl Default for ErosionConfig {
    fn default() -> ErosionConfig {
        ErosionConfig {
            inertia: 0.1,
            sediment_capacity_factor: 4.0,
            deposit_speed: 0.8,
            erosion_speed: 0.8,
            gravity: 10.0,
            evaporate_speed: 0.01,
            brush_size: 3,
            max_lifetime: 70,
        }
    }
}
