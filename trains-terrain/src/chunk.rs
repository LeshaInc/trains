// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{
    collections::HashMap,
    ops::{Index, IndexMut},
};

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct ChunkId {
    pub x: i32,
    pub y: i32,
}

impl ChunkId {
    pub fn new(x: i32, y: i32) -> ChunkId {
        ChunkId { x, y }
    }
}

impl From<(i32, i32)> for ChunkId {
    fn from((x, y): (i32, i32)) -> ChunkId {
        ChunkId { x, y }
    }
}

#[derive(Debug, Clone, Derivative)]
#[derivative(Default(bound = ""))]
pub struct ChunkMap<T> {
    inner: HashMap<ChunkId, T>,
}

impl<T> ChunkMap<T> {
    pub fn new() -> ChunkMap<T> {
        Default::default()
    }

    pub fn num_chunks(&self) -> usize {
        self.inner.len()
    }

    pub fn clear(&mut self) {
        self.inner.clear();
    }

    pub fn get(&self, index: ChunkId) -> Option<&T> {
        self.inner.get(&index)
    }

    pub fn insert(&mut self, index: ChunkId, chunk: T) {
        self.inner.insert(index, chunk);
    }

    pub fn remove(&mut self, index: ChunkId) -> Option<T> {
        self.inner.remove(&index)
    }
}

impl<T> Index<ChunkId> for ChunkMap<T> {
    type Output = T;

    fn index(&self, index: ChunkId) -> &T {
        self.inner.get(&index).expect("no chunk found")
    }
}

impl<T> IndexMut<ChunkId> for ChunkMap<T> {
    fn index_mut(&mut self, index: ChunkId) -> &mut T {
        self.inner.get_mut(&index).expect("no chunk found")
    }
}
