// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

const int NUM_OCTAVES = 10;
const float SCALE = 3.0;
const float INITIAL_WEIGHT = 0.5;
const float SCALE_MUL = 2.0;
const float WEIGHT_MUL = 0.5;
const float EXPONENT = 3.0;
const float MULTIPLIER = 0.7;

layout(location = 0) in vec2 fPos;
layout(location = 0) out uint outHeight;

vec3 permute(vec3 x) {
    return mod(((x * 34.0) + 1.0) * x, 289.0);
}

float snoise(vec2 v) {
    const vec4 C = vec4(0.211324865405187, 0.366025403784439,
        -0.577350269189626, 0.024390243902439);

    vec2 i = floor(v + dot(v, C.yy));
    vec2 x0 = v - i + dot(i, C.xx);
    vec2 i1;

    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);

    vec4 x12 = x0.xyxy + C.xxzz;

    x12.xy -= i1;
    i = mod(i, 289.0);

    vec3 p = permute(permute(i.y + vec3(0.0, i1.y, 1.0)) +
        i.x + vec3(0.0, i1.x, 1.0));
    vec3 m = max(0.5 - vec3(dot(x0, x0), dot(x12.xy, x12.xy),
        dot(x12.zw, x12.zw)), 0.0);

    m = m * m;
    m = m * m;

    vec3 x = 2.0 * fract(p * C.www) - 1.0;
    vec3 h = abs(x) - 0.5;
    vec3 ox = floor(x + 0.5);
    vec3 a0 = x - ox;

    m *= 1.79284291400159 - 0.85373472095314 * (a0 * a0 + h * h);

    vec3 g;
    g.x = a0.x * x0.x + h.x * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;

    return 130.0 * dot(m, g);
}

float unoise(vec2 p) {
    return snoise(p) * 0.5 + 0.5;
}

void main() {
    float acc = 0.0;

    float mountains = pow(unoise(fPos), 5.0) + 0.03;

    float scale = 1.0;
    float weight = mountains * 0.5;

    for (int i = 0; i < 4; i++) {
        acc += (1.0 - abs(snoise(fPos * scale))) * weight;
        scale *= SCALE_MUL;
        weight *= WEIGHT_MUL;
    }

    for (int i = 0; i < 4; i++) {
        acc += (1.0 - abs(unoise(fPos * scale))) * weight;
        scale *= SCALE_MUL;
        weight *= WEIGHT_MUL;
    }

    outHeight = uint(acc * 65535.0);
}
