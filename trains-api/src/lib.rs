// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

mod error;
mod str;

pub mod graphics2d;

pub use self::{error::ApiError, str::ApiStr};

use specs::prelude::*;
use trains_graphics::Graphics2D;

#[no_mangle]
pub unsafe extern "C" fn tapi_version() -> ApiStr<'static> {
    let version = concat!(env!("CARGO_PKG_NAME"), " ", env!("CARGO_PKG_VERSION"), "\0");
    ApiStr::new_borrowed(version)
}

pub struct EngineContext<'a> {
    g2d: Write<'a, Graphics2D>,
}

impl<'a> EngineContext<'a> {
    pub fn new(world: &'a World) -> Self {
        EngineContext {
            g2d: Write::<Graphics2D>::fetch(world),
        }
    }
}
