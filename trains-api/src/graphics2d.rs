// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::ptr;

use crate::{ApiError, ApiStr, EngineContext};

/// Set the font size to use in further `tg2d_text` and related functions calls.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_font_size(ctx: *mut EngineContext, font_size: f32) {
    (*ctx).g2d.set_font_size(font_size);
}

/// Set the text color (in sRGBA color space) to use in further `tg2d_text` and `tg2d_text_lossy` calls.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_text_color(
    ctx: *mut EngineContext,
    r: f32,
    g: f32,
    b: f32,
    a: f32,
) {
    (*ctx).g2d.set_text_color([r, g, b, a]);
}

/// Set the background color (in sRGBA color space) of the rectangles drawn by `tg2d_rect` call.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_rect_color(
    ctx: *mut EngineContext,
    r: f32,
    g: f32,
    b: f32,
    a: f32,
) {
    (*ctx).g2d.set_rect_color([r, g, b, a]);
}

/// Set the border color (in sRGBA color space) of the rectangles drawn by `tg2d_rect` call.
/// For borders to actually show up the thickness must be set to a value greater than zero.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_border_color(ctx: *mut EngineContext, r: f32, g: f32, b: f32) {
    (*ctx).g2d.set_border_color([r, g, b]);
}

/// Set the border thickness (in pixels) of the rectangles drawn by `tg2d_rect` call.
/// The thickness value must be non-negative.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_border_thickness(ctx: *mut EngineContext, thickness: f32) {
    (*ctx).g2d.set_border_thickness(thickness);
}

/// Set the border radius (in pixels) for the corners of rectangles drawn by `tg2d_rect` call.
/// The radius value must be non-negative.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_border_radius(ctx: *mut EngineContext, radius: f32) {
    (*ctx).g2d.set_border_radius(radius);
}

/// Draw a rectangle at specified coordinates (`x` and `y`) with specified size (`w` and `h`).
/// Appearance is affected by `tg2d_set_border_radius`, `tg2d_set_border_thickness`,
/// `tg2d_set_border_color`, and `tg2d_set_rect_color` functions.
#[no_mangle]
pub unsafe extern "C" fn tg2d_rect(ctx: *mut EngineContext, x: f32, y: f32, w: f32, h: f32) {
    (*ctx).g2d.rect([x, y], [w, h]);
}

/// Draw some text at specified coordinates (`x` and `y`).
/// Appearance is affected by `tg2d_set_text_color`, and `tg2d_set_font_size` functions.
///
/// ## Errors
/// If the specified text is not valid UTF-8 byte sequence, `API_ERROR_UTF8` is returned,
/// otherwise `API_ERROR_OK` (or 0 for short)
#[no_mangle]
pub unsafe extern "C" fn tg2d_text(
    ctx: *mut EngineContext,
    x: f32,
    y: f32,
    text: ApiStr,
) -> ApiError {
    match text.as_str() {
        Ok(v) => {
            (*ctx).g2d.text([x, y], v);
            ApiError::Ok
        }

        Err(e) => {
            log::error!("tg2d_text received wrong UTF-8: {}", e);
            ApiError::Utf8
        }
    }
}

/// Text bounding box obtained by `tg2d_measure_text` or `tg2d_measure_text_lossy` calls.
#[repr(C)]
pub struct TextMeasurement {
    pub width: f32,
    pub height: f32,
}

/// Measure the specified text to get its bounding box. Affected by the font size
/// set using `tg2d_set_font_size` function.
///
/// ## Errors
/// If the specified text is not valid UTF-8 byte sequence, `API_ERROR_UTF8` is returned,
/// otherwise `API_ERROR_OK` (or 0 for short)
#[no_mangle]
pub unsafe extern "C" fn tg2d_measure_text(
    ctx: *mut EngineContext,
    text: ApiStr,
    measurement: *mut TextMeasurement,
) -> ApiError {
    match text.as_str() {
        Ok(text) => {
            let size = (*ctx).g2d.measure_text(text);

            ptr::write(
                measurement,
                TextMeasurement {
                    width: size.width,
                    height: size.height,
                },
            );

            ApiError::Ok
        }

        Err(e) => {
            log::error!("tg2d_measure_text received wrong UTF-8: {}", e);
            ApiError::Utf8
        }
    }
}

/// Works the same way as `tg2d_text`, but instead of returning `API_ERROR_UTF8` replaces
/// wrong characters with unicode replacement character (�)
#[no_mangle]
pub unsafe extern "C" fn tg2d_text_lossy(ctx: *mut EngineContext, x: f32, y: f32, text: ApiStr) {
    let text = text.to_string_lossy();
    (*ctx).g2d.text([x, y], text);
}

/// Works the same way as `tg2d_measure_text`, but instead of returning `API_ERROR_UTF8` replaces
/// wrong characters with unicode replacement character (�)
#[no_mangle]
pub unsafe extern "C" fn tg2d_measure_text_lossy(
    ctx: *mut EngineContext,
    text: ApiStr,
    measurement: *mut TextMeasurement,
) {
    let text = text.to_string_lossy();
    let size = (*ctx).g2d.measure_text(text);

    ptr::write(
        measurement,
        TextMeasurement {
            width: size.width,
            height: size.height,
        },
    );
}

/// Get logical size of the window (not the framebuffer size)
#[no_mangle]
pub unsafe extern "C" fn tg2d_window_size(
    ctx: *mut EngineContext,
    width: *mut f64,
    height: *mut f64,
) {
    let (w, h) = (*ctx).g2d.window_size();
    ptr::write(width, w);
    ptr::write(height, h);
}

/// Get window HiDPI factor
#[no_mangle]
pub unsafe extern "C" fn tg2d_hidpi_factor(ctx: *mut EngineContext) -> f64 {
    (*ctx).g2d.hidpi_factor()
}

#[repr(C)]
pub enum Font {
    Normal,
    Monospace,
}

impl Into<trains_graphics::graphics2d::Font> for Font {
    fn into(self) -> trains_graphics::graphics2d::Font {
        use trains_graphics::graphics2d::Font::*;

        match self {
            Font::Normal => Normal,
            Font::Monospace => Monospace,
        }
    }
}

/// Set the font to use in further `tg2d_text` and `tg2d_text_lossy` calls.
#[no_mangle]
pub unsafe extern "C" fn tg2d_set_font(ctx: *mut EngineContext, font: Font) {
    (*ctx).g2d.set_font(font.into())
}

/// Push current state on the stack to restore it later.
#[no_mangle]
pub unsafe extern "C" fn tg2d_push(ctx: *mut EngineContext) {
    (*ctx).g2d.push()
}

/// Pop state from the stack and set it as the current one.
#[no_mangle]
pub unsafe extern "C" fn tg2d_pop(ctx: *mut EngineContext) {
    (*ctx).g2d.pop()
}
