use std::{
    marker::PhantomData,
    os::raw::*,
    slice,
    str::{self, Utf8Error},
};

#[repr(C)]
pub struct ApiStr<'a> {
    pub data: *const c_char,
    pub length: usize,
    pub is_owned: bool,
    _lifetime: PhantomData<&'a ()>,
}

impl<'a> ApiStr<'a> {
    pub fn new_owned(s: String) -> ApiStr<'static> {
        let boxed = s.into_boxed_str();
        let bytes = boxed.as_bytes();

        ApiStr {
            data: bytes.as_ptr() as _,
            length: bytes.len(),
            is_owned: false,
            _lifetime: PhantomData,
        }
    }

    pub fn new_borrowed(s: &'a str) -> ApiStr<'a> {
        let bytes = s.as_bytes();

        ApiStr {
            data: bytes.as_ptr() as _,
            length: bytes.len(),
            is_owned: false,
            _lifetime: PhantomData,
        }
    }

    pub fn as_str(&self) -> Result<&str, Utf8Error> {
        str::from_utf8(self.as_bytes())
    }

    pub fn as_str_mut(&mut self) -> Result<&mut str, Utf8Error> {
        str::from_utf8_mut(self.as_bytes_mut())
    }

    pub fn to_string_lossy(&self) -> String {
        String::from_utf8_lossy(self.as_bytes()).into_owned()
    }

    pub fn as_bytes(&self) -> &[u8] {
        unsafe { slice::from_raw_parts(self.data as *const u8, self.length) }
    }

    pub fn as_bytes_mut(&mut self) -> &mut [u8] {
        unsafe { slice::from_raw_parts_mut(self.data as *mut u8, self.length) }
    }
}

impl<'a> Drop for ApiStr<'a> {
    fn drop(&mut self) {
        if self.is_owned {
            unsafe { Box::from_raw(self.as_bytes_mut()) };
        }
    }
}

#[no_mangle]
pub extern "C" fn tapi_free_str(s: ApiStr) {
    drop(s)
}
