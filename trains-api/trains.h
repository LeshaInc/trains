/*
 * This file is part of Trains.
 * Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
 *
 * Trains is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Trains is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Trains. If not, see <https://www.gnu.org/licenses/>.
 */

/* Generated with cbindgen:0.9.0 */

typedef enum {
  API_ERROR_OK,
  API_ERROR_UTF8,
} ApiError;

typedef enum {
  FONT_NORMAL,
  FONT_MONOSPACE,
} Font;

typedef struct EngineContext EngineContext;

typedef struct {
  const char *data;
  uintptr_t length;
  bool is_owned;
} ApiStr;

/*
 Text bounding box obtained by `tg2d_measure_text` or `tg2d_measure_text_lossy` calls.
 */
typedef struct {
  float width;
  float height;
} TextMeasurement;

void tapi_free_str(ApiStr s);

ApiStr tapi_version(void);

/*
 Get window HiDPI factor
 */
double tg2d_hidpi_factor(EngineContext *ctx);

/*
 Measure the specified text to get its bounding box. Affected by the font size
 set using `tg2d_set_font_size` function.
 ## Errors
 If the specified text is not valid UTF-8 byte sequence, `API_ERROR_UTF8` is returned,
 otherwise `API_ERROR_OK` (or 0 for short)
 */
ApiError tg2d_measure_text(EngineContext *ctx, ApiStr text, TextMeasurement *measurement);

/*
 Works the same way as `tg2d_measure_text`, but instead of returning `API_ERROR_UTF8` replaces
 wrong characters with unicode replacement character (�)
 */
void tg2d_measure_text_lossy(EngineContext *ctx, ApiStr text, TextMeasurement *measurement);

/*
 Pop state from the stack and set it as the current one.
 */
void tg2d_pop(EngineContext *ctx);

/*
 Push current state on the stack to restore it later.
 */
void tg2d_push(EngineContext *ctx);

/*
 Draw a rectangle at specified coordinates (`x` and `y`) with specified size (`w` and `h`).
 Appearance is affected by `tg2d_set_border_radius`, `tg2d_set_border_thickness`,
 `tg2d_set_border_color`, and `tg2d_set_rect_color` functions.
 */
void tg2d_rect(EngineContext *ctx, float x, float y, float w, float h);

/*
 Set the border color (in sRGBA color space) of the rectangles drawn by `tg2d_rect` call.
 For borders to actually show up the thickness must be set to a value greater than zero.
 */
void tg2d_set_border_color(EngineContext *ctx, float r, float g, float b);

/*
 Set the border radius (in pixels) for the corners of rectangles drawn by `tg2d_rect` call.
 The radius value must be non-negative.
 */
void tg2d_set_border_radius(EngineContext *ctx, float radius);

/*
 Set the border thickness (in pixels) of the rectangles drawn by `tg2d_rect` call.
 The thickness value must be non-negative.
 */
void tg2d_set_border_thickness(EngineContext *ctx, float thickness);

/*
 Set the font to use in further `tg2d_text` and `tg2d_text_lossy` calls.
 */
void tg2d_set_font(EngineContext *ctx, Font font);

/*
 Set the font size to use in further `tg2d_text` and related functions calls.
 */
void tg2d_set_font_size(EngineContext *ctx, float font_size);

/*
 Set the background color (in sRGBA color space) of the rectangles drawn by `tg2d_rect` call.
 */
void tg2d_set_rect_color(EngineContext *ctx, float r, float g, float b, float a);

/*
 Set the text color (in sRGBA color space) to use in further `tg2d_text` and `tg2d_text_lossy` calls.
 */
void tg2d_set_text_color(EngineContext *ctx,
                         float r,
                         float g,
                         float b,
                         float a);

/*
 Draw some text at specified coordinates (`x` and `y`).
 Appearance is affected by `tg2d_set_text_color`, and `tg2d_set_font_size` functions.
 ## Errors
 If the specified text is not valid UTF-8 byte sequence, `API_ERROR_UTF8` is returned,
 otherwise `API_ERROR_OK` (or 0 for short)
 */
ApiError tg2d_text(EngineContext *ctx, float x, float y, ApiStr text);

/*
 Works the same way as `tg2d_text`, but instead of returning `API_ERROR_UTF8` replaces
 wrong characters with unicode replacement character (�)
 */
void tg2d_text_lossy(EngineContext *ctx, float x, float y, ApiStr text);

/*
 Get logical size of the window (not the framebuffer size)
 */
void tg2d_window_size(EngineContext *ctx, double *width, double *height);
