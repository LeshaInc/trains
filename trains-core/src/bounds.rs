// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use nalgebra::Point2;

use crate::Size2;

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Bounds2 {
    pub x: f32,
    pub y: f32,
    pub width: f32,
    pub height: f32,
}

impl Bounds2 {
    pub const ZERO: Bounds2 = Bounds2 {
        x: 0.0,
        y: 0.0,
        width: 0.0,
        height: 0.0,
    };

    pub fn new(x: f32, y: f32, width: f32, height: f32) -> Bounds2 {
        Bounds2 {
            x,
            y,
            width,
            height,
        }
    }

    pub fn with_size(mut self, size: Size2) -> Bounds2 {
        self.width = size.width;
        self.height = size.height;
        self
    }

    pub fn set_size(&mut self, size: Size2) {
        self.width = size.width;
        self.height = size.height;
    }

    pub fn size(&self) -> Size2 {
        Size2::new(self.width, self.height)
    }

    pub fn contains(&self, point: impl Into<Point2<f32>>) -> bool {
        let point = point.into();
        let x = point.coords.x;
        let y = point.coords.y;
        x >= self.x && y >= self.y && x <= self.x + self.width && y <= self.y + self.height
    }
}
