// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use specs::prelude::*;
use std::time::{Duration, Instant};

pub enum Transition {
    None,
    Push(Box<dyn State>),
    Pop,
    Switch(Box<dyn State>),
    Quit,
}

pub trait State {
    fn on_start(&mut self, _world: &mut World) {}

    #[allow(clippy::boxed_local)]
    fn on_stop(self: Box<Self>, _world: &mut World) {}
    fn on_pause(&mut self, _world: &mut World) {}
    fn on_resume(&mut self, _world: &mut World) {}

    fn update(&mut self, _world: &mut World) -> Transition {
        Transition::None
    }

    fn fixed_update(&mut self, _world: &mut World) -> Transition {
        Transition::None
    }
}

pub struct UpdatesCounter {
    ups: f32,
    starting_point: Instant,
    num_updates: usize,
}

impl UpdatesCounter {
    pub fn new() -> UpdatesCounter {
        UpdatesCounter {
            ups: 60.0,
            starting_point: Instant::now(),
            num_updates: 0,
        }
    }

    pub fn updates_per_second(&self) -> f32 {
        self.ups
    }

    pub fn update(&mut self) {
        self.num_updates += 1;

        let elapsed = self.starting_point.elapsed();
        let elapsed = elapsed.as_secs() as f32 + elapsed.as_nanos() as f32 / 1_000_000_000.0;
        self.ups = self.num_updates as f32 / elapsed;

        if elapsed > 1.0 {
            self.starting_point = Instant::now();
            self.num_updates = 0;
        }
    }
}

impl Default for UpdatesCounter {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Copy, Debug, Default, NewType)]
#[repr(transparent)]
pub struct FramesPerSecond(pub f32);

#[derive(Clone, Copy, Debug, Default, NewType)]
#[repr(transparent)]
pub struct UpdatesPerSecond(pub f32);

#[derive(Clone, Copy, Debug, Default, NewType)]
#[repr(transparent)]
pub struct DeltaTime(pub Duration);

pub struct Application {
    stack: Vec<Box<dyn State>>,
    world: World,
    last_fixed_update: Instant,
    fixed_interval: Duration,
    ups_counter: UpdatesCounter,
    fps_counter: UpdatesCounter,
    last_frame: Instant,
}

impl Application {
    pub fn new<S: State + 'static>(mut initial: S) -> Application {
        let mut world = World::new();

        initial.on_start(&mut world);

        Application {
            stack: vec![Box::new(initial)],
            world,
            last_fixed_update: Instant::now(),
            fixed_interval: Duration::from_nanos(16_666_666),
            ups_counter: UpdatesCounter::new(),
            fps_counter: UpdatesCounter::new(),
            last_frame: Instant::now(),
        }
    }

    pub fn fixed_intetrval(&self) -> Duration {
        self.fixed_interval
    }

    pub fn set_fixed_interval(&mut self, interval: Duration) {
        self.fixed_interval = interval;
    }

    pub fn is_running(&self) -> bool {
        !self.stack.is_empty()
    }

    pub fn update(&mut self) {
        self.world
            .insert(FramesPerSecond(self.fps_counter.updates_per_second()));
        self.world
            .insert(UpdatesPerSecond(self.ups_counter.updates_per_second()));
        self.world.insert(DeltaTime(self.last_frame.elapsed()));

        if self.last_fixed_update.elapsed() > self.fixed_interval {
            self.call_fixed_update();
            self.ups_counter.update();
        }

        self.call_update();
        self.world.maintain();
        self.fps_counter.update();
        self.last_frame = Instant::now();
    }

    fn call_fixed_update(&mut self) {
        if let Some(state) = self.stack.last_mut() {
            let trans = state.fixed_update(&mut self.world);
            self.handle_trans(trans);
        }
    }

    fn call_update(&mut self) {
        if let Some(state) = self.stack.last_mut() {
            let trans = state.update(&mut self.world);
            self.handle_trans(trans);
        }
    }

    fn handle_trans(&mut self, trans: Transition) {
        match trans {
            Transition::None => (),
            Transition::Push(mut state) => {
                if let Some(state) = self.stack.last_mut() {
                    state.on_pause(&mut self.world);
                }

                state.on_start(&mut self.world);
                self.stack.push(state);
            }
            Transition::Pop => {
                if let Some(state) = self.stack.pop() {
                    state.on_stop(&mut self.world);
                }

                if let Some(state) = self.stack.last_mut() {
                    state.on_resume(&mut self.world);
                }
            }
            Transition::Switch(mut state) => {
                if let Some(mut state) = self.stack.pop() {
                    state.on_pause(&mut self.world);
                }

                state.on_start(&mut self.world);
                self.stack.push(state);
            }
            Transition::Quit => {
                for state in self.stack.drain(..).rev() {
                    state.on_stop(&mut self.world);
                }
            }
        }
    }
}
