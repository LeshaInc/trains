// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{
    f32::INFINITY,
    ops::{Add, AddAssign, Sub, SubAssign},
};

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Size2 {
    pub width: f32,
    pub height: f32,
}

impl Size2 {
    pub const ZERO: Size2 = Size2 {
        width: 0.0,
        height: 0.0,
    };

    pub const INFINITE: Size2 = Size2 {
        width: INFINITY,
        height: INFINITY,
    };

    pub fn new(width: f32, height: f32) -> Size2 {
        Size2 { width, height }
    }

    pub fn with_width(mut self, width: f32) -> Size2 {
        self.width = width;
        self
    }

    pub fn with_height(mut self, height: f32) -> Size2 {
        self.height = height;
        self
    }

    pub fn map_width(mut self, f: impl FnOnce(f32) -> f32) -> Size2 {
        self.width = f(self.width);
        self
    }

    pub fn map_height(mut self, f: impl FnOnce(f32) -> f32) -> Size2 {
        self.height = f(self.height);
        self
    }

    pub fn min(self, other: Size2) -> Size2 {
        Size2::new(self.width.min(other.width), self.height.min(other.height))
    }

    pub fn max(self, other: Size2) -> Size2 {
        Size2::new(self.width.max(other.width), self.height.max(other.height))
    }

    pub fn clamp(self, min: Size2, max: Size2) -> Size2 {
        max.min(min.max(self))
    }
}

impl Add for Size2 {
    type Output = Size2;

    fn add(mut self, rhs: Size2) -> Size2 {
        self.width += rhs.width;
        self.height += rhs.height;
        self
    }
}

impl AddAssign for Size2 {
    fn add_assign(&mut self, rhs: Size2) {
        self.width += rhs.width;
        self.height += rhs.height;
    }
}

impl Sub for Size2 {
    type Output = Size2;

    fn sub(mut self, rhs: Size2) -> Size2 {
        self.width -= rhs.width;
        self.height -= rhs.height;
        self
    }
}

impl SubAssign for Size2 {
    fn sub_assign(&mut self, rhs: Size2) {
        self.width -= rhs.width;
        self.height -= rhs.height;
    }
}
