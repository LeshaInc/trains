// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

pub use rendy::wsi::winit::{
    self, ElementState, KeyboardInput, ModifiersState, MouseButton, ScanCode, VirtualKeyCode,
    WindowEvent,
};

use std::{collections::HashSet, ops::Deref};

use either::Either;
use nalgebra::Point2;
use rendy::wsi::winit::dpi::LogicalPosition;
use specs::{prelude::*, shrev::EventChannel};

#[derive(Debug, Clone)]
pub struct KeyboardState {
    pressed_scancodes: HashSet<ScanCode>,
    pressed_keycodes: HashSet<VirtualKeyCode>,
    modifiers: ModifiersState,
}

pub trait Key {
    fn key(&self) -> Either<ScanCode, VirtualKeyCode>;
}

impl Key for ScanCode {
    fn key(&self) -> Either<ScanCode, VirtualKeyCode> {
        Either::Left(*self)
    }
}

impl Key for VirtualKeyCode {
    fn key(&self) -> Either<ScanCode, VirtualKeyCode> {
        Either::Right(*self)
    }
}

impl<T: Key> Key for &T {
    fn key(&self) -> Either<ScanCode, VirtualKeyCode> {
        (*self).key()
    }
}

impl KeyboardState {
    pub fn update_modifiers(&mut self, modifiers: ModifiersState) {
        self.modifiers = modifiers;
    }

    pub fn handle_event(&mut self, event: KeyboardInput) {
        self.update_modifiers(event.modifiers);

        match event.state {
            ElementState::Pressed => {
                if let Some(code) = event.virtual_keycode {
                    self.pressed_keycodes.insert(code);
                }

                self.pressed_scancodes.insert(event.scancode);
            }

            ElementState::Released => {
                if let Some(code) = event.virtual_keycode {
                    self.pressed_keycodes.remove(&code);
                }

                self.pressed_scancodes.remove(&event.scancode);
            }
        }
    }

    pub fn is_key_down(&self, key: impl Key) -> bool {
        match key.key() {
            Either::Left(scancode) => self.pressed_scancodes.contains(&scancode),
            Either::Right(keycode) => self.pressed_keycodes.contains(&keycode),
        }
    }

    pub fn is_alt_down(&self) -> bool {
        self.modifiers.alt
    }

    pub fn is_shift_down(&self) -> bool {
        self.modifiers.shift
    }

    pub fn is_ctrl_down(&self) -> bool {
        self.modifiers.ctrl
    }

    pub fn is_logo_down(&self) -> bool {
        self.modifiers.logo
    }
}

impl Default for KeyboardState {
    fn default() -> KeyboardState {
        KeyboardState {
            pressed_keycodes: HashSet::with_capacity(128),
            pressed_scancodes: HashSet::with_capacity(128),
            modifiers: ModifiersState::default(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct MouseState {
    pressed_buttons: HashSet<MouseButton>,
    position: Point2<f32>,
}

impl MouseState {
    pub fn handle_move_event(&mut self, position: LogicalPosition) {
        self.position = Point2::new(position.x as _, position.y as _);
    }

    pub fn handle_button_event(&mut self, state: ElementState, button: MouseButton) {
        match state {
            ElementState::Pressed => {
                self.pressed_buttons.insert(button);
            }

            ElementState::Released => {
                self.pressed_buttons.remove(&button);
            }
        }
    }

    pub fn is_mouse_button_down(&self, button: MouseButton) -> bool {
        self.pressed_buttons.contains(&button)
    }

    pub fn mouse_position(&self) -> Point2<f32> {
        self.position
    }
}

impl Default for MouseState {
    fn default() -> MouseState {
        MouseState {
            pressed_buttons: HashSet::with_capacity(4),
            position: Point2::new(0.0, 0.0),
        }
    }
}

#[derive(Debug, Clone)]
pub struct InputState {
    keyboard: KeyboardState,
    mouse: MouseState,
    is_focused: bool,
}

impl InputState {
    pub fn handle_event(&mut self, event: &WindowEvent) {
        match *event {
            WindowEvent::Focused(focused) => self.is_focused = focused,
            WindowEvent::KeyboardInput { input, .. } => self.keyboard.handle_event(input),

            WindowEvent::CursorMoved {
                modifiers,
                position,
                ..
            } => {
                self.keyboard.update_modifiers(modifiers);
                self.mouse.handle_move_event(position);
            }

            WindowEvent::MouseInput {
                modifiers,
                state,
                button,
                ..
            } => {
                self.keyboard.update_modifiers(modifiers);
                self.mouse.handle_button_event(state, button);
            }

            _ => {}
        }
    }

    pub fn is_focused(&self) -> bool {
        self.is_focused
    }

    pub fn keyboard_state(&self) -> &KeyboardState {
        &self.keyboard
    }

    pub fn mouse_state(&self) -> &MouseState {
        &self.mouse
    }
}

impl InputState {
    pub fn is_key_down(&self, key: impl Key) -> bool {
        self.keyboard.is_key_down(key)
    }

    pub fn is_alt_down(&self) -> bool {
        self.keyboard.is_alt_down()
    }

    pub fn is_shift_down(&self) -> bool {
        self.keyboard.is_shift_down()
    }

    pub fn is_ctrl_down(&self) -> bool {
        self.keyboard.is_ctrl_down()
    }

    pub fn is_logo_down(&self) -> bool {
        self.keyboard.is_logo_down()
    }

    pub fn is_key_up(&self, key: impl Key) -> bool {
        !self.keyboard.is_key_down(key)
    }

    pub fn is_alt_up(&self) -> bool {
        !self.keyboard.is_alt_down()
    }

    pub fn is_shift_up(&self) -> bool {
        !self.keyboard.is_shift_down()
    }

    pub fn is_ctrl_up(&self) -> bool {
        !self.keyboard.is_ctrl_down()
    }

    pub fn is_logo_up(&self) -> bool {
        !self.keyboard.is_logo_down()
    }
}

impl InputState {
    pub fn mouse_position(&self) -> Point2<f32> {
        self.mouse.mouse_position()
    }

    pub fn is_mouse_button_down(&self, button: MouseButton) -> bool {
        self.mouse.is_mouse_button_down(button)
    }

    pub fn is_lmb_down(&self) -> bool {
        self.mouse.is_mouse_button_down(MouseButton::Left)
    }

    pub fn is_mmb_down(&self) -> bool {
        self.mouse.is_mouse_button_down(MouseButton::Middle)
    }

    pub fn is_rmb_down(&self) -> bool {
        self.mouse.is_mouse_button_down(MouseButton::Right)
    }

    pub fn is_mouse_button_up(&self, button: MouseButton) -> bool {
        !self.mouse.is_mouse_button_down(button)
    }

    pub fn is_lmb_up(&self) -> bool {
        !self.mouse.is_mouse_button_down(MouseButton::Left)
    }

    pub fn is_mmb_up(&self) -> bool {
        !self.mouse.is_mouse_button_down(MouseButton::Middle)
    }

    pub fn is_rmb_up(&self) -> bool {
        !self.mouse.is_mouse_button_down(MouseButton::Right)
    }
}

impl Default for InputState {
    fn default() -> InputState {
        InputState {
            keyboard: KeyboardState::default(),
            mouse: MouseState::default(),
            is_focused: true,
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Input {
    state: InputState,
    mouse_events: Vec<(MouseButton, ElementState, ModifiersState)>,
    keyboard_events: Vec<KeyboardInput>,
}

impl Input {
    pub fn state(&self) -> &InputState {
        &self.state
    }

    pub fn next_tick(&mut self) {
        self.mouse_events.clear();
        self.keyboard_events.clear();
    }

    pub fn handle_event(&mut self, event: &WindowEvent) {
        self.state.handle_event(event);

        match event {
            WindowEvent::MouseInput {
                state,
                button,
                modifiers,
                ..
            } => self.mouse_events.push((*button, *state, *modifiers)),

            WindowEvent::KeyboardInput { input, .. } => self.keyboard_events.push(*input),

            _ => (),
        }
    }

    pub fn has_clicked(&self, button: MouseButton) -> bool {
        self.mouse_events
            .iter()
            .any(|(btn, state, _)| btn == &button && state == &ElementState::Released)
    }

    pub fn has_left_clicked(&self) -> bool {
        self.has_clicked(MouseButton::Left)
    }

    pub fn has_middle_clicked(&self) -> bool {
        self.has_clicked(MouseButton::Middle)
    }

    pub fn has_right_clicked(&self) -> bool {
        self.has_clicked(MouseButton::Right)
    }

    fn had_key_event(&self, key: impl Key, state: ElementState) -> bool {
        self.keyboard_events.iter().any(|input| {
            input.state == state
                && match (key.key(), input.virtual_keycode) {
                    (Either::Left(scancode), _) => input.scancode == scancode,
                    (Either::Right(keycode), Some(k)) => keycode == k,
                    _ => false,
                }
        })
    }

    pub fn has_pressed(&self, key: impl Key) -> bool {
        self.had_key_event(key, ElementState::Pressed)
    }

    pub fn has_released(&self, key: impl Key) -> bool {
        self.had_key_event(key, ElementState::Released)
    }
}

impl Deref for Input {
    type Target = InputState;

    fn deref(&self) -> &InputState {
        &self.state
    }
}

#[derive(Debug, Default)]
pub struct InputSystem {
    reader: Option<ReaderId<WindowEvent>>,
}

impl InputSystem {
    pub fn new() -> InputSystem {
        InputSystem::default()
    }
}

impl<'a> System<'a> for InputSystem {
    type SystemData = (Write<'a, Input>, Read<'a, EventChannel<WindowEvent>>);

    fn run(&mut self, (mut input, events): Self::SystemData) {
        input.next_tick();

        for event in events.read(self.reader.as_mut().unwrap()) {
            input.handle_event(event);
        }
    }

    fn setup(&mut self, world: &mut World) {
        Self::SystemData::setup(world);

        let mut events = Write::<EventChannel<WindowEvent>>::fetch(world);
        self.reader = Some(events.register_reader());
    }
}
