// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::f32::EPSILON;

use nalgebra::{
    Matrix3, Matrix4, Perspective3, Point3, Projective3, Rotation3, Unit, Vector2, Vector3,
};

#[derive(Copy, Clone)]
pub struct Camera {
    pub position: Point3<f32>,
    pub direction: Vector3<f32>,
    pub perspective: Perspective3<f32>,
    pub dimensions: Vector2<f32>,
}

impl Camera {
    pub fn set_dimensions(&mut self, width: f32, height: f32) {
        self.perspective.set_aspect(width / height);
        self.dimensions = Vector2::new(width, height);
    }

    pub fn new_look_at(position: Point3<f32>, target: Point3<f32>, aspect: f32) -> Camera {
        let direction = (target - position).normalize();

        Camera {
            position,
            direction,
            perspective: Perspective3::new(aspect, 45f32.to_radians(), 0.1, 100.0),
            dimensions: Vector2::new(800.0, 600.0),
        }
    }

    pub fn view_matrix(&self) -> Projective3<f32> {
        Projective3::from_matrix_unchecked(Matrix4::look_at_rh(
            &self.position,
            &(self.position + self.direction),
            &Vector3::z(),
        ))
    }

    pub fn projection_matrix(&self) -> Projective3<f32> {
        let mut perspective = self.perspective.to_homogeneous();
        perspective.append_translation_mut(&Vector3::new(0.0, 0.0, 1.0));
        perspective.append_nonuniform_scaling_mut(&Vector3::new(1.0, -1.0, 0.5));
        Projective3::from_matrix_unchecked(perspective)
    }

    pub fn projection_matrix_2d(&self) -> Matrix3<f32> {
        Matrix3::from([
            [2.0 / (self.dimensions.x - 1.0), 0.0, -1.0],
            [0.0, 2.0 / (self.dimensions.y - 1.0), -1.0],
            [0.0, 0.0, 1.0],
        ])
        .transpose()
    }

    pub fn look(&mut self, dx: f32, dy: f32) {
        let mut rot = Rotation3::identity();

        if dx.abs() > 0.0 {
            rot *= Rotation3::from_euler_angles(0.0, 0.0, -dx);
        }

        if dy.abs() > 0.0 {
            let dot = Vector3::z().dot(&self.direction);
            let invalid = (1.0 - dot) < 0.01 && dy < 0.0 || (dot + 1.0) < 0.01 && dy > 0.0;

            if !invalid {
                rot *= Rotation3::from_axis_angle(&Unit::new_normalize(self.right()), -dy);
            }
        }

        self.direction = rot.transform_vector(&self.direction);
    }

    pub fn fly(&mut self, amount: f32, forward: f32, right: f32) {
        let direction = self.direction * forward + self.right() * right;
        if let Some(d) = direction.try_normalize(EPSILON) {
            self.position += d * amount;
        }
    }

    pub fn right(&self) -> Vector3<f32> {
        self.direction.cross(&Vector3::z()).normalize()
    }

    pub fn up(&self) -> Vector3<f32> {
        self.direction.cross(&self.right()).normalize()
    }
}
