// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::cmp::Ordering;

use rendy::{
    command::Families,
    factory::{BasicHeapsConfigure, Config, DevicesConfigure, Factory, OneGraphicsQueue},
    hal::{
        adapter::{Adapter, DeviceType},
        Backend,
    },
};

use serde::{Deserialize, Serialize};

#[derive(Debug, NewType)]
#[repr(transparent)]
pub struct GpuFactory<B: Backend>(Factory<B>);

#[derive(Debug, NewType)]
#[repr(transparent)]
pub struct GpuFamilies<B: Backend>(Families<B>);

#[derive(Debug, Serialize, Deserialize)]
#[serde(default)]
pub struct GpuConfig {
    adapter: usize,
}

impl Default for GpuConfig {
    fn default() -> GpuConfig {
        GpuConfig { adapter: 0 }
    }
}

struct MyDevicesConfigure<'a> {
    config: &'a GpuConfig,
}

impl<'a> MyDevicesConfigure<'a> {
    pub fn new(config: &GpuConfig) -> MyDevicesConfigure<'_> {
        MyDevicesConfigure { config }
    }
}

fn adapter_score<B: Backend>(adapter: &Adapter<B>) -> u8 {
    match adapter.info.device_type {
        DeviceType::DiscreteGpu => 4,
        DeviceType::IntegratedGpu => 3,
        DeviceType::VirtualGpu => 2,
        DeviceType::Cpu => 1,
        _ => 0,
    }
}

fn cmp_adapter<B: Backend>(lhs: &Adapter<B>, rhs: &Adapter<B>) -> Ordering {
    adapter_score(rhs)
        .cmp(&adapter_score(lhs))
        .then(lhs.info.name.cmp(&rhs.info.name))
}

impl<'a> DevicesConfigure for MyDevicesConfigure<'a> {
    fn pick<B: Backend>(&self, adapters: &[Adapter<B>]) -> usize {
        let mut adapters = adapters.iter().enumerate().collect::<Vec<_>>();
        adapters.sort_by(|(_, l), (_, r)| cmp_adapter(l, r));

        let selected = self.config.adapter.min(adapters.len() - 1);

        log::info!("Available GPUs:");
        for (i, (_, adapter)) in adapters.iter().enumerate() {
            let (n, t, v, d) = (
                &adapter.info.name,
                &adapter.info.device_type,
                adapter.info.vendor,
                adapter.info.device,
            );

            if i == selected {
                log::info!("*{}: {} type={:?} vendor={} device={}", i, n, t, v, d);
            } else {
                log::info!(" {}: {} type={:?} vendor={} device={}", i, n, t, v, d);
            }
        }

        adapters[selected].0
    }
}

pub fn init<B: Backend>(config: GpuConfig) -> (GpuFactory<B>, GpuFamilies<B>) {
    let config = Config {
        devices: MyDevicesConfigure::new(&config),
        heaps: BasicHeapsConfigure,
        queues: OneGraphicsQueue,
    };

    let (factory, families) = rendy::factory::init(config).unwrap();
    (GpuFactory(factory), GpuFamilies(families))
}
