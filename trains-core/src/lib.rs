// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#[macro_use]
extern crate newtype;

mod bounds;
mod size;

pub mod app;
pub mod camera;
pub mod gpu;
pub mod input;

pub use app::{Application, DeltaTime, FramesPerSecond, State, Transition, UpdatesPerSecond};
pub use bounds::Bounds2;
pub use camera::Camera;
pub use gpu::{GpuConfig, GpuFactory, GpuFamilies};
pub use input::*;
pub use size::Size2;

pub type Size = Size2;
pub type Bounds = Bounds2;
