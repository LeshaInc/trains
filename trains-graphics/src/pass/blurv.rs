// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use rendy::shader::ShaderSetBuilder;
use trains_shadermgr::{include_shader, Shader};

use super::ProcessingEffect;

const VERTEX: Shader = include_shader!(vertex, "processing.vert");
const FRAGMENT: Shader = include_shader!(fragment, "blurv.frag");

lazy_static::lazy_static! {
    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&VERTEX).unwrap()
        .with_fragment(&FRAGMENT).unwrap();
}

pub struct BlurV;

impl ProcessingEffect for BlurV {
    fn shaders() -> &'static ShaderSetBuilder {
        &SHADERS
    }
}
