// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::mem::size_of;

use rendy::{
    command::{DrawCommand, QueueId, RenderPassEncoder},
    factory::{BufferState, Factory, ImageState},
    graph::{
        render::{
            Layout, PrepareResult, SetLayout, SimpleGraphicsPipeline, SimpleGraphicsPipelineDesc,
        },
        GraphContext, NodeBuffer, NodeImage,
    },
    hal::{
        buffer::{Access as BufferAccess, Usage as BufferUsage},
        format::{Aspects, Format, Swizzle},
        image::{Access as ImageAccess, Offset, Usage as ImageUsage},
        pso::{
            Descriptor, DescriptorSetLayoutBinding, DescriptorSetWrite, DescriptorType, ElemStride,
            Element, PipelineStage, ShaderStageFlags, VertexInputRate,
        },
        Backend, Device,
    },
    memory::Data,
    mesh::AsVertex,
    resource::{
        self, Buffer, BufferInfo, DescriptorSet, DescriptorSetLayout, Escape, Extent, Filter,
        Handle, Image, ImageInfo, ImageView, ImageViewInfo, Kind, Layout as ImageLayout,
        SamplerInfo, SubresourceLayers, SubresourceRange, Tiling, ViewCapabilities, ViewKind,
        WrapMode,
    },
    shader::{ShaderSet, ShaderSetBuilder},
};

use failure::Fallible;
use glyph_brush::{rusttype::Rect, BrushAction, BrushError};
use nalgebra::{Matrix3, Vector2};
use specs::prelude::*;

use trains_shadermgr::{include_shader, Shader};

use crate::{
    graphics2d::{GlyphInstance, Graphics2D},
    view_data::ViewData,
};

const VERTEX: Shader = include_shader!(vertex, "text2d.vert");
const FRAGMENT: Shader = include_shader!(fragment, "text2d.frag");

lazy_static::lazy_static! {
    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&VERTEX).unwrap()
        .with_fragment(&FRAGMENT).unwrap();
}

type FontAtlas<B> = (Handle<Image<B>>, Escape<ImageView<B>>);

fn create_atlas<B: Backend>(
    factory: &Factory<B>,
    descriptor_set: &Escape<DescriptorSet<B>>,
    size: (u32, u32),
) -> Fallible<FontAtlas<B>> {
    let sampler = factory.get_sampler(SamplerInfo::new(Filter::Linear, WrapMode::Clamp))?;

    let image: Handle<_> = factory
        .create_image(
            ImageInfo {
                kind: Kind::D2(size.0, size.1, 1, 1),
                format: Format::R8Unorm,
                levels: 1,
                tiling: Tiling::Optimal,
                view_caps: ViewCapabilities::empty(),
                usage: ImageUsage::SAMPLED | ImageUsage::TRANSFER_DST,
            },
            Data,
        )?
        .into();

    let image_view = factory.create_image_view(
        image.clone(),
        ImageViewInfo {
            view_kind: ViewKind::D2,
            format: Format::R8Unorm,
            swizzle: Swizzle::NO,
            range: SubresourceRange {
                aspects: Aspects::COLOR,
                levels: 0..1,
                layers: 0..1,
            },
        },
    )?;

    let descriptors = Some(Descriptor::CombinedImageSampler(
        image_view.raw(),
        resource::Layout::ShaderReadOnlyOptimal,
        sampler.raw(),
    ));

    unsafe {
        factory.write_descriptor_sets(vec![DescriptorSetWrite {
            set: descriptor_set.raw(),
            binding: 0,
            array_offset: 0,
            descriptors,
        }]);
    }

    Ok((image, image_view))
}

fn upload<B: Backend>(
    factory: &Factory<B>,
    queue: QueueId,
    image: &Handle<Image<B>>,
    rect: Rect<u32>,
    data: &[u8],
) -> Fallible<()> {
    unsafe {
        factory.upload_image(
            image.clone(),
            rect.width(),
            rect.height(),
            SubresourceLayers {
                aspects: Aspects::COLOR,
                level: 0,
                layers: 0..1,
            },
            Offset {
                x: rect.min.x as _,
                y: rect.min.y as _,
                z: 0,
            },
            Extent {
                width: rect.width(),
                height: rect.height(),
                depth: 1,
            },
            data,
            ImageLayout::ShaderReadOnlyOptimal,
            ImageState::new(queue, ImageLayout::ShaderReadOnlyOptimal)
                .with_stage(PipelineStage::FRAGMENT_SHADER)
                .with_access(ImageAccess::SHADER_READ),
        )
    }
}

#[derive(Debug, Default)]
pub struct Text2DPipelineDesc;

impl<B: Backend> SimpleGraphicsPipelineDesc<B, World> for Text2DPipelineDesc {
    type Pipeline = Text2DPipeline<B>;

    fn load_shader_set(&self, factory: &mut Factory<B>, _world: &World) -> ShaderSet<B> {
        SHADERS.build(factory, Default::default()).unwrap()
    }

    fn vertices(&self) -> Vec<(Vec<Element<Format>>, ElemStride, VertexInputRate)> {
        vec![GlyphInstance::vertex().gfx_vertex_input_desc(VertexInputRate::Instance(1))]
    }

    fn layout(&self) -> Layout {
        Layout {
            sets: vec![
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::UniformBuffer,
                        count: 1,
                        stage_flags: ShaderStageFlags::ALL,
                        immutable_samplers: false,
                    }],
                },
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::CombinedImageSampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false,
                    }],
                },
            ],
            push_constants: vec![],
        }
    }

    fn build(
        self,
        _ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        _queue: QueueId,
        world: &World,
        _buffers: Vec<NodeBuffer>,
        _images: Vec<NodeImage>,
        set_layouts: &[Handle<DescriptorSetLayout<B>>],
    ) -> Fallible<Self::Pipeline> {
        let descriptor_set = factory
            .create_descriptor_set(set_layouts[1].clone())
            .unwrap();

        let capacity = 8 * 1024;

        let vertex_buffer = factory.create_buffer(
            BufferInfo {
                size: (capacity * size_of::<GlyphInstance>()) as u64,
                usage: BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            },
            Data,
        )?;

        let indirect_buffer = factory.create_buffer(
            BufferInfo {
                size: size_of::<DrawCommand>() as u64,
                usage: BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            },
            Data,
        )?;

        let atlas = create_atlas(factory, &descriptor_set, (256, 256))?;

        let mut g = Write::<Graphics2D>::fetch(world);
        g.recreate_glyph_brush();

        Ok(Text2DPipeline {
            indirect_buffer,
            vertex_buffer,
            descriptor_set,
            atlas,
            capacity,
            len: 0,
        })
    }
}

#[derive(Debug)]
pub struct Text2DPipeline<B: Backend> {
    indirect_buffer: Escape<Buffer<B>>,
    vertex_buffer: Escape<Buffer<B>>,
    descriptor_set: Escape<DescriptorSet<B>>,
    atlas: FontAtlas<B>,
    capacity: usize,
    len: usize,
}

impl<B: Backend> SimpleGraphicsPipeline<B, World> for Text2DPipeline<B> {
    type Desc = Text2DPipelineDesc;

    fn prepare(
        &mut self,
        factory: &Factory<B>,
        queue: QueueId,
        _set_layouts: &[Handle<DescriptorSetLayout<B>>],
        _index: usize,
        world: &World,
    ) -> PrepareResult {
        let mut g = Write::<Graphics2D>::fetch(world);

        let res = g.glyph_brush.process_queued(
            |r, d| upload(factory, queue, &self.atlas.0, r, d).unwrap(),
            |v| {
                let px = v.pixel_coords;
                let tx = v.tex_coords;

                let trans = Vector2::new(px.min.x as f32, px.min.y as _);
                let scale = Vector2::new(px.width() as f32, px.height() as _);
                let t = Matrix3::new_translation(&trans) * Matrix3::new_nonuniform_scaling(&scale);
                let transform = t.remove_row(2).transpose().into();

                GlyphInstance {
                    transform,
                    tex_coords: [tx.min.x, tx.min.y, tx.width(), tx.height()],
                    z_depth: v.z,
                    color: v.color,
                }
            },
        );

        match res {
            Ok(BrushAction::Draw(data)) => unsafe {
                if data.len() > self.capacity {
                    self.capacity = self.len.next_power_of_two();
                    log::debug!("Recreating text2d vbuf; new cap = {}", self.capacity);

                    self.vertex_buffer = factory
                        .create_buffer(
                            BufferInfo {
                                size: (self.capacity * size_of::<GlyphInstance>()) as u64,
                                usage: BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
                            },
                            Data,
                        )
                        .unwrap();
                }

                if !data.is_empty() {
                    factory
                        .upload_buffer(
                            &self.vertex_buffer,
                            0,
                            data.as_slice(),
                            Some(
                                BufferState::new(queue)
                                    .with_stage(PipelineStage::VERTEX_SHADER)
                                    .with_access(BufferAccess::SHADER_READ),
                            ),
                            BufferState::new(queue)
                                .with_stage(PipelineStage::VERTEX_SHADER)
                                .with_access(BufferAccess::SHADER_READ),
                        )
                        .unwrap();
                }

                self.len = data.len();
            },

            Err(BrushError::TextureTooSmall { suggested }) => {
                g.glyph_brush.resize_texture(suggested.0, suggested.1);
                self.atlas = create_atlas(factory, &self.descriptor_set, suggested).unwrap();
                self.len = 0;
            }

            _ => {}
        }

        unsafe {
            factory
                .upload_buffer(
                    &self.indirect_buffer,
                    0,
                    &[DrawCommand {
                        vertex_count: 6,
                        instance_count: self.len as _,
                        first_vertex: 0,
                        first_instance: 0,
                    }],
                    Some(
                        BufferState::new(queue)
                            .with_stage(PipelineStage::VERTEX_SHADER)
                            .with_access(BufferAccess::SHADER_READ),
                    ),
                    BufferState::new(queue)
                        .with_stage(PipelineStage::VERTEX_SHADER)
                        .with_access(BufferAccess::SHADER_READ),
                )
                .unwrap();
        }

        PrepareResult::DrawReuse
    }

    fn draw(
        &mut self,
        layout: &B::PipelineLayout,
        mut encoder: RenderPassEncoder<B>,
        _index: usize,
        world: &World,
    ) {
        let vd = world.fetch::<ViewData<B>>();

        unsafe {
            encoder.bind_graphics_descriptor_sets(
                layout,
                0,
                Some(vd.descriptor_set.raw())
                    .into_iter()
                    .chain(Some(self.descriptor_set.raw())),
                None,
            );
            encoder.bind_vertex_buffers(0, Some((self.vertex_buffer.raw(), 0)));
            encoder.draw_indirect(
                self.indirect_buffer.raw(),
                0,
                1,
                size_of::<DrawCommand>() as _,
            );
        }
    }

    fn dispose(self, _factory: &mut Factory<B>, _world: &World) {}
}
