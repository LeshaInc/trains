// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::mem::size_of;

use rendy::{
    command::{QueueId, RenderPassEncoder},
    factory::{BufferState, Factory},
    graph::{
        render::{
            Layout, PrepareResult, SetLayout, SimpleGraphicsPipeline, SimpleGraphicsPipelineDesc,
        },
        GraphContext, NodeBuffer, NodeImage,
    },
    hal::{
        buffer::{Access, Usage as BufferUsage},
        format::Format,
        pso::{
            ColorBlendDesc, Descriptor, DescriptorSetLayoutBinding, DescriptorSetWrite,
            DescriptorType, ElemStride, Element, PipelineStage, ShaderStageFlags, VertexInputRate,
        },
        Backend, Device, IndexType,
    },
    memory::Data,
    resource::{
        self, Buffer, BufferInfo, DescriptorSet, DescriptorSetLayout, Escape, Filter, Handle,
        Sampler, SamplerInfo, WrapMode,
    },
    shader::{ShaderSet, ShaderSetBuilder},
};

use failure::Fallible;
use specs::prelude::*;

use trains_shadermgr::{include_shader, Shader};
use trains_terrain::Terrain;

use crate::view_data::ViewData;

const VERTEX: Shader = include_shader!(vertex, "terrain.vert");
const FRAGMENT: Shader = include_shader!(fragment, "terrain.frag");

lazy_static::lazy_static! {
    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&VERTEX).unwrap()
        .with_fragment(&FRAGMENT).unwrap();

    // static ref SHADER_REFLECTION: SpirvReflection = SHADERS.reflect().unwrap();
}

#[repr(C)]
#[derive(Debug)]
pub struct Vertex {
    pos: [f32; 2],
}

#[derive(Debug, Default)]
pub struct TerrainPipelineDesc;

impl<B: Backend> SimpleGraphicsPipelineDesc<B, World> for TerrainPipelineDesc {
    type Pipeline = TerrainPipeline<B>;

    fn load_shader_set(&self, factory: &mut Factory<B>, _world: &World) -> ShaderSet<B> {
        SHADERS.build(factory, Default::default()).unwrap()
    }

    fn vertices(&self) -> Vec<(Vec<Element<Format>>, ElemStride, VertexInputRate)> {
        return vec![(
            vec![Element {
                format: Format::Rg32Sfloat,
                offset: 0,
            }],
            8,
            VertexInputRate::Vertex,
        )];
    }

    fn colors(&self) -> Vec<ColorBlendDesc> {
        (0..4).map(|_| ColorBlendDesc::EMPTY).collect()
    }

    fn layout(&self) -> Layout {
        // spirv reflection is broken
        // SHADER_REFLECTION.layout().unwrap()

        Layout {
            sets: vec![
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::UniformBuffer,
                        count: 1,
                        stage_flags: ShaderStageFlags::ALL,
                        immutable_samplers: false,
                    }],
                },
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::CombinedImageSampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::VERTEX | ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false,
                    }],
                },
            ],
            push_constants: vec![],
        }
    }

    fn build(
        self,
        _ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        queue: QueueId,
        _world: &World,
        _buffers: Vec<NodeBuffer>,
        _images: Vec<NodeImage>,
        set_layouts: &[Handle<DescriptorSetLayout<B>>],
    ) -> Fallible<Self::Pipeline> {
        let sampler = factory.get_sampler(SamplerInfo::new(Filter::Linear, WrapMode::Clamp))?;

        let descriptor_set = factory
            .create_descriptor_set(set_layouts[1].clone())
            .unwrap();

        let vertex_buffer = factory.create_buffer(
            BufferInfo {
                size: 1001 * 1001 * size_of::<Vertex>() as u64,
                usage: BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            },
            Data,
        )?;

        let mut vertices = Vec::with_capacity(1001 * 1001);

        for y in 0..1001 {
            for x in 0..1001 {
                vertices.push(Vertex {
                    pos: [x as f32 / 1000.0, y as f32 / 1000.0],
                })
            }
        }

        unsafe {
            factory.upload_buffer(
                &vertex_buffer,
                0,
                &vertices,
                None,
                BufferState::new(queue)
                    .with_stage(PipelineStage::VERTEX_INPUT)
                    .with_access(Access::VERTEX_BUFFER_READ),
            )?;
        }

        let index_buffer = factory.create_buffer(
            BufferInfo {
                size: 6 * 1000 * 1000 * 4,
                usage: BufferUsage::INDEX | BufferUsage::TRANSFER_DST,
            },
            Data,
        )?;

        let mut indices = Vec::with_capacity(6 * 1000 * 1000);

        for y in 0..1000u32 {
            for x in 0..1000u32 {
                indices.push(x + 1001 * y);
                indices.push(x + 1 + 1001 * y);
                indices.push(x + 1001 * (y + 1));
                indices.push(x + 1001 * (y + 1));
                indices.push(x + 1 + 1001 * y);
                indices.push(x + 1 + 1001 * (y + 1));
            }
        }

        unsafe {
            factory.upload_buffer(
                &index_buffer,
                0,
                &indices,
                None,
                BufferState::new(queue)
                    .with_stage(PipelineStage::VERTEX_INPUT)
                    .with_access(Access::VERTEX_BUFFER_READ),
            )?;
        }

        Ok(TerrainPipeline {
            vertex_buffer,
            index_buffer,
            descriptor_set,
            sampler,
            loaded: false,
        })
    }
}

#[derive(Debug)]
pub struct TerrainPipeline<B: Backend> {
    vertex_buffer: Escape<Buffer<B>>,
    index_buffer: Escape<Buffer<B>>,
    descriptor_set: Escape<DescriptorSet<B>>,
    sampler: Handle<Sampler<B>>,
    loaded: bool,
}

impl<B: Backend> SimpleGraphicsPipeline<B, World> for TerrainPipeline<B> {
    type Desc = TerrainPipelineDesc;

    fn prepare(
        &mut self,
        factory: &Factory<B>,
        _queue: QueueId,
        _set_layouts: &[Handle<DescriptorSetLayout<B>>],
        _index: usize,
        world: &World,
    ) -> PrepareResult {
        let mut terrain = world.fetch_mut::<Terrain<B>>();
        let chunk = match terrain.get_chunk((0, 0)) {
            Some(v) => v,
            None => {
                terrain.schedule_chunk((0, 0));
                return PrepareResult::DrawRecord;
            }
        };

        unsafe {
            factory.write_descriptor_sets(vec![DescriptorSetWrite {
                set: self.descriptor_set.raw(),
                binding: 0,
                array_offset: 0,
                descriptors: Some(Descriptor::CombinedImageSampler(
                    chunk.image_view.raw(),
                    resource::Layout::ShaderReadOnlyOptimal,
                    self.sampler.raw(),
                )),
            }]);
            self.loaded = true;
        }

        PrepareResult::DrawRecord
    }

    fn draw(
        &mut self,
        layout: &B::PipelineLayout,
        mut encoder: RenderPassEncoder<B>,
        _index: usize,
        world: &World,
    ) {
        if !self.loaded {
            return;
        }

        let vd = world.fetch::<ViewData<B>>();

        unsafe {
            encoder.bind_graphics_descriptor_sets(
                layout,
                0,
                Some(vd.descriptor_set.raw())
                    .into_iter()
                    .chain(Some(self.descriptor_set.raw())),
                None,
            );

            encoder.bind_vertex_buffers(0, Some((self.vertex_buffer.raw(), 0)));
            encoder.bind_index_buffer(self.index_buffer.raw(), 0, IndexType::U32);
            encoder.draw_indexed(0..6_000_000, 0, 0..1);
        }
    }

    fn dispose(self, _factory: &mut Factory<B>, _world: &World) {}
}
