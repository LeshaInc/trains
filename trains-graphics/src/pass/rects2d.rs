// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::mem::size_of;

use rendy::{
    command::{QueueId, RenderPassEncoder},
    factory::{BufferState, Factory},
    graph::{
        render::{
            Layout, PrepareResult, SetLayout, SimpleGraphicsPipeline, SimpleGraphicsPipelineDesc,
        },
        GraphContext, ImageAccess, NodeBuffer, NodeImage,
    },
    hal::{
        buffer::{Access as BufferAccess, Usage as BufferUsage},
        format::{Aspects, Format, Swizzle},
        image::{Access as HalImageAccess, Usage as ImageUsage},
        pso::{
            Descriptor, DescriptorSetLayoutBinding, DescriptorSetWrite, DescriptorType, ElemStride,
            Element, PipelineStage, ShaderStageFlags, VertexInputRate,
        },
        Backend, Device,
    },
    memory::Data,
    mesh::AsVertex,
    resource::{
        self, Buffer, BufferInfo, DescriptorSet, DescriptorSetLayout, Escape, Filter, Handle,
        ImageView, ImageViewInfo, Layout as ImageLayout, SamplerInfo, SubresourceRange, ViewKind,
        WrapMode,
    },
    shader::{ShaderSet, ShaderSetBuilder},
};

use failure::Fallible;
use specs::prelude::*;

use trains_shadermgr::{include_shader, Shader};

use crate::{
    graphics2d::{Graphics2D, Rect},
    view_data::ViewData,
};

const VERTEX: Shader = include_shader!(vertex, "rects2d.vert");
const FRAGMENT: Shader = include_shader!(fragment, "rects2d.frag");

lazy_static::lazy_static! {
    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&VERTEX).unwrap()
        .with_fragment(&FRAGMENT).unwrap();

    // static ref SHADER_REFLECTION: SpirvReflection = SHADERS.reflect().unwrap();
}

#[derive(Debug, Default)]
pub struct Rects2DPipelineDesc;

impl<B: Backend> SimpleGraphicsPipelineDesc<B, World> for Rects2DPipelineDesc {
    type Pipeline = Rects2DPipeline<B>;

    fn load_shader_set(&self, factory: &mut Factory<B>, _world: &World) -> ShaderSet<B> {
        SHADERS.build(factory, Default::default()).unwrap()
    }

    fn vertices(&self) -> Vec<(Vec<Element<Format>>, ElemStride, VertexInputRate)> {
        vec![Rect::vertex().gfx_vertex_input_desc(VertexInputRate::Instance(1))]
    }

    fn images(&self) -> Vec<ImageAccess> {
        vec![ImageAccess {
            access: HalImageAccess::INPUT_ATTACHMENT_READ,
            usage: ImageUsage::SAMPLED,
            layout: ImageLayout::ShaderReadOnlyOptimal,
            stages: PipelineStage::FRAGMENT_SHADER,
        }]
    }

    fn layout(&self) -> Layout {
        // spirv reflection is broken
        // SHADER_REFLECTION.layout().unwrap()

        Layout {
            sets: vec![
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::UniformBuffer,
                        count: 1,
                        stage_flags: ShaderStageFlags::ALL,
                        immutable_samplers: false,
                    }],
                },
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::CombinedImageSampler,
                        count: 1,
                        stage_flags: ShaderStageFlags::FRAGMENT,
                        immutable_samplers: false,
                    }],
                },
            ],
            push_constants: vec![],
        }
    }

    fn build(
        self,
        ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        _queue: QueueId,
        _world: &World,
        _buffers: Vec<NodeBuffer>,
        images: Vec<NodeImage>,
        set_layouts: &[Handle<DescriptorSetLayout<B>>],
    ) -> Fallible<Self::Pipeline> {
        let sampler = factory.get_sampler(SamplerInfo::new(Filter::Linear, WrapMode::Clamp))?;

        let descriptor_set = factory
            .create_descriptor_set(set_layouts[1].clone())
            .unwrap();

        let image_res = ctx.get_image(images[0].id).unwrap();
        let image_view = factory
            .create_image_view(
                image_res.clone(),
                ImageViewInfo {
                    view_kind: ViewKind::D2,
                    format: image_res.format(),
                    swizzle: Swizzle::NO,
                    range: SubresourceRange {
                        aspects: Aspects::COLOR,
                        levels: 0..1,
                        layers: 0..1,
                    },
                },
            )
            .unwrap();

        let descriptors = Some(Descriptor::CombinedImageSampler(
            image_view.raw(),
            resource::Layout::ShaderReadOnlyOptimal,
            sampler.raw(),
        ));

        unsafe {
            factory.write_descriptor_sets(vec![DescriptorSetWrite {
                set: descriptor_set.raw(),
                binding: 0,
                array_offset: 0,
                descriptors,
            }]);
        }

        let capacity = 1024;

        let vertex_buffer = factory.create_buffer(
            BufferInfo {
                size: (capacity * size_of::<Rect>()) as u64,
                usage: BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
            },
            Data,
        )?;

        Ok(Rects2DPipeline {
            vertex_buffer,
            descriptor_set,
            image_view,
            capacity,
            len: 0,
        })
    }
}

#[derive(Debug)]
pub struct Rects2DPipeline<B: Backend> {
    vertex_buffer: Escape<Buffer<B>>,
    descriptor_set: Escape<DescriptorSet<B>>,
    image_view: Escape<ImageView<B>>,
    capacity: usize,
    len: usize,
}

impl<B: Backend> SimpleGraphicsPipeline<B, World> for Rects2DPipeline<B> {
    type Desc = Rects2DPipelineDesc;

    fn prepare(
        &mut self,
        factory: &Factory<B>,
        queue: QueueId,
        _set_layouts: &[Handle<DescriptorSetLayout<B>>],
        _index: usize,
        world: &World,
    ) -> PrepareResult {
        let g = Read::<Graphics2D>::fetch(world);

        if g.rects.len() == self.len && !g.rects.has_changed() {
            return PrepareResult::DrawReuse;
        }

        let old_len = self.len;
        self.len = g.rects.len();

        if self.len > self.capacity {
            self.capacity = self.len.next_power_of_two();
            self.vertex_buffer = factory
                .create_buffer(
                    BufferInfo {
                        size: (self.capacity * size_of::<Rect>()) as u64,
                        usage: BufferUsage::VERTEX | BufferUsage::TRANSFER_DST,
                    },
                    Data,
                )
                .unwrap();
        }

        unsafe {
            factory
                .upload_buffer(
                    &self.vertex_buffer,
                    0,
                    g.rects.as_slice(),
                    Some(
                        BufferState::new(queue)
                            .with_stage(PipelineStage::VERTEX_SHADER)
                            .with_access(BufferAccess::SHADER_READ),
                    ),
                    BufferState::new(queue)
                        .with_stage(PipelineStage::VERTEX_SHADER)
                        .with_access(BufferAccess::SHADER_READ),
                )
                .unwrap();
        }

        if self.len != old_len && (old_len == 0 || self.len == 0) {
            PrepareResult::DrawRecord
        } else {
            PrepareResult::DrawReuse
        }
    }

    fn draw(
        &mut self,
        layout: &B::PipelineLayout,
        mut encoder: RenderPassEncoder<B>,
        _index: usize,
        world: &World,
    ) {
        if self.len == 0 {
            return;
        }

        let vd = world.fetch::<ViewData<B>>();

        unsafe {
            encoder.bind_graphics_descriptor_sets(
                layout,
                0,
                Some(vd.descriptor_set.raw())
                    .into_iter()
                    .chain(Some(self.descriptor_set.raw())),
                None,
            );
            encoder.bind_vertex_buffers(0, Some((self.vertex_buffer.raw(), 0)));
            encoder.draw(0..6, 0..self.len as _);
        }
    }

    fn dispose(self, _factory: &mut Factory<B>, _world: &World) {}
}
