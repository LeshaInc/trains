// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use rendy::{
    command::{QueueId, RenderPassEncoder},
    factory::Factory,
    graph::{
        render::{
            Layout, PrepareResult, SetLayout, SimpleGraphicsPipeline, SimpleGraphicsPipelineDesc,
        },
        GraphContext, ImageAccess, NodeBuffer, NodeImage,
    },
    hal::{
        format::{Aspects, Swizzle},
        image::{Access, Usage},
        pso::{
            DepthStencilDesc, Descriptor, DescriptorSetLayoutBinding, DescriptorSetWrite,
            DescriptorType, PipelineStage, ShaderStageFlags,
        },
        Backend, Device,
    },
    resource::{
        self, DescriptorSet, DescriptorSetLayout, Escape, Filter, Handle, ImageView, ImageViewInfo,
        Layout as ImageLayout, Sampler, SamplerInfo, SubresourceRange, ViewKind, WrapMode,
    },
    shader::{ShaderSet, ShaderSetBuilder},
};

use failure::Fallible;
use specs::prelude::*;

use crate::view_data::ViewData;
use trains_shadermgr::{include_shader, Shader};

const VERTEX: Shader = include_shader!(vertex, "deferred.vert");
const FRAGMENT: Shader = include_shader!(fragment, "deferred.frag");

lazy_static::lazy_static! {
    static ref SHADERS: ShaderSetBuilder = ShaderSetBuilder::default()
        .with_vertex(&VERTEX).unwrap()
        .with_fragment(&FRAGMENT).unwrap();

    // static ref SHADER_REFLECTION: SpirvReflection = SHADERS.reflect().unwrap();
}

#[derive(Debug, Default)]
pub struct DeferredPipelineDesc;

impl<B: Backend> SimpleGraphicsPipelineDesc<B, World> for DeferredPipelineDesc {
    type Pipeline = DeferredPipeline<B>;

    fn load_shader_set(&self, factory: &mut Factory<B>, _world: &World) -> ShaderSet<B> {
        SHADERS.build(factory, Default::default()).unwrap()
    }

    fn depth_stencil(&self) -> Option<DepthStencilDesc> {
        None
    }

    fn images(&self) -> Vec<ImageAccess> {
        (0..4)
            .map(|_| ImageAccess {
                access: Access::INPUT_ATTACHMENT_READ,
                usage: Usage::SAMPLED,
                layout: ImageLayout::ShaderReadOnlyOptimal,
                stages: PipelineStage::FRAGMENT_SHADER,
            })
            .collect()
    }

    fn layout(&self) -> Layout {
        // spirv reflection is broken
        // SHADER_REFLECTION.layout().unwrap()

        Layout {
            sets: vec![
                SetLayout {
                    bindings: vec![DescriptorSetLayoutBinding {
                        binding: 0,
                        ty: DescriptorType::UniformBuffer,
                        count: 1,
                        stage_flags: ShaderStageFlags::ALL,
                        immutable_samplers: false,
                    }],
                },
                SetLayout {
                    bindings: vec![
                        DescriptorSetLayoutBinding {
                            binding: 0,
                            ty: DescriptorType::CombinedImageSampler,
                            count: 1,
                            stage_flags: ShaderStageFlags::FRAGMENT,
                            immutable_samplers: false,
                        },
                        DescriptorSetLayoutBinding {
                            binding: 1,
                            ty: DescriptorType::CombinedImageSampler,
                            count: 1,
                            stage_flags: ShaderStageFlags::FRAGMENT,
                            immutable_samplers: false,
                        },
                        DescriptorSetLayoutBinding {
                            binding: 2,
                            ty: DescriptorType::CombinedImageSampler,
                            count: 1,
                            stage_flags: ShaderStageFlags::FRAGMENT,
                            immutable_samplers: false,
                        },
                        DescriptorSetLayoutBinding {
                            binding: 3,
                            ty: DescriptorType::CombinedImageSampler,
                            count: 1,
                            stage_flags: ShaderStageFlags::FRAGMENT,
                            immutable_samplers: false,
                        },
                    ],
                },
            ],
            push_constants: vec![],
        }
    }

    fn build(
        self,
        ctx: &GraphContext<B>,
        factory: &mut Factory<B>,
        _queue: QueueId,
        _world: &World,
        _buffers: Vec<NodeBuffer>,
        images: Vec<NodeImage>,
        set_layouts: &[Handle<DescriptorSetLayout<B>>],
    ) -> Fallible<Self::Pipeline> {
        let sampler = factory.get_sampler(SamplerInfo::new(Filter::Linear, WrapMode::Clamp))?;

        let descriptor_set = factory
            .create_descriptor_set(set_layouts[1].clone())
            .unwrap();

        let views = (0..4)
            .map(|i| {
                let image = ctx.get_image(images[i].id).unwrap();
                factory
                    .create_image_view(
                        image.clone(),
                        ImageViewInfo {
                            view_kind: ViewKind::D2,
                            format: image.format(),
                            swizzle: Swizzle::NO,
                            range: SubresourceRange {
                                aspects: if image.format().is_color() {
                                    Aspects::COLOR
                                } else {
                                    Aspects::DEPTH
                                },
                                levels: 0..1,
                                layers: 0..1,
                            },
                        },
                    )
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let descriptors = views.iter().map(|view| {
            Descriptor::CombinedImageSampler(
                view.raw(),
                resource::Layout::ShaderReadOnlyOptimal,
                sampler.raw(),
            )
        });

        unsafe {
            factory.write_descriptor_sets(vec![DescriptorSetWrite {
                set: descriptor_set.raw(),
                binding: 0,
                array_offset: 0,
                descriptors,
            }]);
        }

        Ok(DeferredPipeline {
            descriptor_set,
            sampler,
            views,
        })
    }
}

#[derive(Debug)]
pub struct DeferredPipeline<B: Backend> {
    descriptor_set: Escape<DescriptorSet<B>>,
    sampler: Handle<Sampler<B>>,
    views: Vec<Escape<ImageView<B>>>,
}

impl<B: Backend> SimpleGraphicsPipeline<B, World> for DeferredPipeline<B> {
    type Desc = DeferredPipelineDesc;

    fn prepare(
        &mut self,
        _factory: &Factory<B>,
        _queue: QueueId,
        _set_layouts: &[Handle<DescriptorSetLayout<B>>],
        _index: usize,
        _world: &World,
    ) -> PrepareResult {
        PrepareResult::DrawReuse
    }

    fn draw(
        &mut self,
        layout: &B::PipelineLayout,
        mut encoder: RenderPassEncoder<B>,
        _index: usize,
        world: &World,
    ) {
        let vd = world.fetch::<ViewData<B>>();

        unsafe {
            encoder.bind_graphics_descriptor_sets(
                layout,
                0,
                Some(vd.descriptor_set.raw())
                    .into_iter()
                    .chain(Some(self.descriptor_set.raw())),
                None,
            );

            encoder.draw(0..6, 0..1);
        }
    }

    fn dispose(self, _factory: &mut Factory<B>, _world: &World) {}
}
