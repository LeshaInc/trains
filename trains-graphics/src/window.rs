// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{
    fmt::{self, Display, Formatter},
    ops::{Deref, DerefMut},
};

use rendy::{hal::image::Kind, wsi::winit::Window};

#[derive(Derivative)]
#[derivative(Debug = "transparent")]
pub struct WindowResource(Window);

impl Into<Window> for WindowResource {
    fn into(self) -> Window {
        self.0
    }
}

impl From<Window> for WindowResource {
    fn from(window: Window) -> WindowResource {
        WindowResource(window)
    }
}

impl Deref for WindowResource {
    type Target = Window;

    fn deref(&self) -> &Window {
        &self.0
    }
}

impl DerefMut for WindowResource {
    fn deref_mut(&mut self) -> &mut Window {
        &mut self.0
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct WindowDimensions {
    width: f64,
    height: f64,
    hidpi_factor: f64,
    dirty: bool,
}

impl Display for WindowDimensions {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{}x{} hidpi={}",
            self.width, self.height, self.hidpi_factor
        )
    }
}

impl WindowDimensions {
    pub fn fetch(window: &Window) -> WindowDimensions {
        let inner_size = window
            .get_inner_size()
            .expect("Can't get inner size of closed window");

        let hidpi_factor = window.get_hidpi_factor();

        WindowDimensions {
            width: inner_size.width,
            height: inner_size.height,
            hidpi_factor,
            dirty: true,
        }
    }

    pub fn hidpi_factor(&self) -> f64 {
        self.hidpi_factor
    }

    pub fn logical_size(&self) -> (f64, f64) {
        (self.width, self.height)
    }

    pub fn physical_size(&self) -> (u32, u32) {
        let width = self.width * self.hidpi_factor;
        let height = self.height * self.hidpi_factor;
        (width as _, height as _)
    }

    pub fn image_kind(&self) -> Kind {
        let (width, height) = self.physical_size();
        Kind::D2(width, height, 1, 1)
    }

    pub fn is_dirty(&self) -> bool {
        self.dirty
    }

    pub fn set_dirty(&mut self) {
        self.dirty = true;
    }

    pub fn clear_dirty(&mut self) {
        self.dirty = false;
    }

    pub fn update(&mut self, width: f64, height: f64) {
        self.width = width;
        self.height = height;
        self.set_dirty();
    }

    pub fn update_hidpi_factor(&mut self, hidpi_factor: f64) {
        self.hidpi_factor = hidpi_factor;
        self.set_dirty();
    }
}
