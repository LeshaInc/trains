// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::mem::size_of;

use rendy::{
    descriptor::{DescriptorSetLayoutBinding, DescriptorType},
    factory::Factory,
    hal::{
        buffer::Usage,
        pso::{Descriptor, DescriptorSetWrite, ShaderStageFlags},
        Backend, Device,
    },
    memory::Dynamic,
    resource::{Buffer, BufferInfo, DescriptorSet, DescriptorSetLayout, Escape, Handle},
};

use failure::Fallible;
use glsl_layout::{mat3, mat4, vec3, AsStd140};
use trains_core::Camera;

#[derive(Debug, Copy, Clone, Default, AsStd140)]
pub struct ViewUniforms {
    pub view_projection: mat4,
    pub view_position: vec3,
    pub projection_2d: mat3,
}

#[derive(Debug)]
pub struct ViewData<B: Backend> {
    pub uniform_buffer: Escape<Buffer<B>>,
    pub descriptor_set: Escape<DescriptorSet<B>>,
    pub descriptor_set_layout: Handle<DescriptorSetLayout<B>>,
    pub uniforms: ViewUniforms,
}

impl<B: Backend> ViewData<B> {
    pub fn new(factory: &mut Factory<B>) -> Fallible<ViewData<B>> {
        let descriptor_set_layout: Handle<_> = factory
            .create_descriptor_set_layout(vec![DescriptorSetLayoutBinding {
                binding: 0,
                ty: DescriptorType::UniformBuffer,
                count: 1,
                stage_flags: ShaderStageFlags::ALL,
                immutable_samplers: false,
            }])?
            .into();

        let descriptor_set: Escape<_> =
            factory.create_descriptor_set(descriptor_set_layout.clone())?;

        let uniform_buffer: Escape<_> = factory.create_buffer(
            BufferInfo {
                size: size_of::<<ViewUniforms as AsStd140>::Std140>() as _,
                usage: Usage::UNIFORM,
            },
            Dynamic,
        )?;

        unsafe {
            factory.write_descriptor_sets(vec![DescriptorSetWrite {
                set: descriptor_set.raw(),
                binding: 0,
                array_offset: 0,
                descriptors: Some(Descriptor::Buffer(uniform_buffer.raw(), None..None)),
            }]);
        }

        let data = ViewData {
            uniform_buffer,
            descriptor_set,
            descriptor_set_layout,
            uniforms: Default::default(),
        };

        Ok(data)
    }

    pub fn update(&mut self, factory: &mut Factory<B>, camera: &Camera) {
        let view = camera.view_matrix();
        let proj = camera.projection_matrix();
        let view_proj: [[f32; 4]; 4] = (proj * view).to_homogeneous().into();
        let view_pos: [f32; 3] = camera.position.coords.into();
        let proj_2d: [[f32; 3]; 3] = camera.projection_matrix_2d().into();

        self.uniforms.view_projection = view_proj.into();
        self.uniforms.view_position = view_pos.into();
        self.uniforms.projection_2d = proj_2d.into();

        unsafe {
            factory
                .upload_visible_buffer(&mut self.uniform_buffer, 0, &[self.uniforms.std140()])
                .unwrap()
        };
    }
}
