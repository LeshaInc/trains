// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use rendy::{graph::Graph, hal::Backend};
use specs::prelude::*;

use trains_core::{Camera, GpuFactory, GpuFamilies};

use crate::{graph::GraphFactory, view_data::ViewData, window::WindowDimensions};

#[derive(Derivative)]
#[derivative(Default(bound = ""))]
pub struct RenderingSystem<B: Backend> {
    graph: Option<Graph<B, World>>,
    graph_factory: GraphFactory,
}

impl<B: Backend> RenderingSystem<B> {
    pub fn new() -> RenderingSystem<B> {
        Default::default()
    }

    fn rebuild_graph(&mut self, world: &World) {
        let instant = std::time::Instant::now();

        let mut factory = WriteExpect::<GpuFactory<B>>::fetch(world);
        let mut families = WriteExpect::<GpuFamilies<B>>::fetch(world);

        if let Some(graph) = self.graph.take() {
            graph.dispose(&mut factory, world);
        }

        let graph = self
            .graph_factory
            .build(&mut factory, &mut families, world)
            .expect("Failed to build frame graph");

        log::debug!("Built frame graph in {:?}", instant.elapsed());

        self.graph = Some(graph);
    }
}

impl<'a, B: Backend> RunNow<'a> for RenderingSystem<B> {
    fn run_now(&mut self, world: &World) {
        let mut dimensions = WriteExpect::<WindowDimensions>::fetch(world);

        if dimensions.is_dirty() {
            log::debug!(
                "Window dimensions changed to {}, rebuilding frame graph",
                &*dimensions
            );

            dimensions.clear_dirty();
            drop(dimensions);

            self.rebuild_graph(world);
        }

        if self.graph.is_none() {
            log::debug!("Building initial frame graph");
            self.rebuild_graph(world);
        }

        let mut factory = WriteExpect::<GpuFactory<B>>::fetch(world);
        let mut families = WriteExpect::<GpuFamilies<B>>::fetch(world);

        {
            let mut view_data = WriteExpect::<ViewData<B>>::fetch(world);
            let camera = ReadExpect::<Camera>::fetch(world);

            view_data.update(&mut factory, &camera);
        }

        factory.maintain(&mut families);
        self.graph
            .as_mut()
            .unwrap()
            .run(&mut factory, &mut families, world);
    }

    fn setup(&mut self, world: &mut World) {
        let mut factory = WriteExpect::<GpuFactory<B>>::fetch(world);
        let view_data = ViewData::new(&mut factory).unwrap();
        drop(factory);
        world.insert(view_data);
    }

    fn dispose(self: Box<Self>, world: &mut World) {
        let mut factory = WriteExpect::<GpuFactory<B>>::fetch(world);

        log::debug!("Freeing frame graph resources");
        factory.wait_idle().unwrap();
        self.graph.unwrap().dispose(&mut factory, world);
    }
}
