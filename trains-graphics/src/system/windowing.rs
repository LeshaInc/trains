// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

pub use rendy::wsi::winit::{KeyboardInput, WindowEvent};

use rendy::wsi::winit::{Event, EventsLoop, WindowBuilder};

use specs::{prelude::*, shrev::EventChannel};

use trains_core::Camera;

use crate::window::{WindowDimensions, WindowResource};

pub struct WindowingSystem {
    events_loop: EventsLoop,
}

impl WindowingSystem {
    pub fn new() -> WindowingSystem {
        WindowingSystem {
            events_loop: EventsLoop::new(),
        }
    }
}

impl Default for WindowingSystem {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(SystemData)]
pub struct Data<'a> {
    window: ReadExpect<'a, WindowResource>,
    dimensions: WriteExpect<'a, WindowDimensions>,
    events: Write<'a, EventChannel<WindowEvent>>,
    camera: Option<Write<'a, Camera>>,
}

impl<'a> System<'a> for WindowingSystem {
    type SystemData = Data<'a>;

    fn run(&mut self, mut data: Self::SystemData) {
        let window_id = data.window.id();

        self.events_loop.poll_events(|event| match event {
            Event::WindowEvent {
                window_id: id,
                ref event,
            } if id == window_id => {
                data.events.single_write(event.clone());

                match event {
                    WindowEvent::Resized(size) => {
                        data.dimensions.update(size.width, size.height);

                        if let Some(camera) = &mut data.camera {
                            let (width, height) = data.dimensions.logical_size();
                            camera.set_dimensions(width as _, height as _);
                        }
                    }

                    WindowEvent::HiDpiFactorChanged(fac) => {
                        data.dimensions.update_hidpi_factor(*fac)
                    }

                    _ => (),
                }
            }
            _ => (),
        });
    }

    fn setup(&mut self, world: &mut World) {
        Self::SystemData::setup(world);

        let window = WindowBuilder::new()
            .with_dimensions((800, 600).into())
            .build(&self.events_loop)
            .unwrap();

        world.insert(WindowDimensions::fetch(&window));
        world.insert(WindowResource::from(window));
    }
}
