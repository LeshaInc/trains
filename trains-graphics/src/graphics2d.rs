// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use rendy::{
    hal::format::Format,
    mesh::{AsVertex, VertexFormat},
};

use glyph_brush::{rusttype::Scale, FontId, GlyphBrush, GlyphBrushBuilder, GlyphCruncher, Section};
use nalgebra::{Matrix3, Vector2};
use specs::prelude::*;

use trains_core::Size;

use crate::window::WindowDimensions;

#[derive(Debug, Derivative)]
#[derivative(Default(bound = ""))]
pub(crate) struct InstanceBuffer<V: AsVertex> {
    vertices: Vec<V>,
    changed: bool,
    len: usize,
}

impl<V: AsVertex> InstanceBuffer<V> {
    pub fn clear(&mut self) {
        self.changed = false;
        self.len = 0;
    }

    pub fn add(&mut self, vertex: V) {
        if !self.changed && self.vertices.get(self.len) == Some(&vertex) {
            self.len += 1;
            return;
        }

        if !self.changed {
            self.vertices.drain(self.len..);
        }

        self.changed = true;

        self.vertices.push(vertex);
        self.len += 1;
    }

    pub fn has_changed(&self) -> bool {
        self.changed
    }

    pub fn len(&self) -> usize {
        self.len
    }

    pub fn as_slice(&self) -> &[V] {
        &self.vertices[0..self.len]
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Rect {
    pub transform: [[f32; 3]; 2],
    pub z_depth: f32,
    pub color: [f32; 4],
    pub border_radius: f32,
    pub border_thickness: f32,
    pub border_color: [f32; 3],
}

impl AsVertex for Rect {
    fn vertex() -> VertexFormat {
        VertexFormat::new(vec![
            (Format::Rgb32Sfloat, "transform0"),
            (Format::Rgb32Sfloat, "transform1"),
            (Format::R32Sfloat, "z_depth"),
            (Format::Rgba32Sfloat, "color"),
            (Format::R32Sfloat, "border_radius"),
            (Format::R32Sfloat, "border_thickness"),
            (Format::Rgb32Sfloat, "border_color"),
        ])
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, Default, PartialEq, PartialOrd)]
pub struct GlyphInstance {
    pub transform: [[f32; 3]; 2],
    pub tex_coords: [f32; 4],
    pub z_depth: f32,
    pub color: [f32; 4],
}

impl AsVertex for GlyphInstance {
    fn vertex() -> VertexFormat {
        VertexFormat::new(vec![
            (Format::Rgb32Sfloat, "transform0"),
            (Format::Rgb32Sfloat, "transform1"),
            (Format::Rgba32Sfloat, "tex_coords"),
            (Format::R32Sfloat, "z_depth"),
            (Format::Rgba32Sfloat, "color"),
        ])
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Font {
    Normal,
    Monospace,
}

impl Font {
    pub fn get_id(self) -> FontId {
        match self {
            Font::Normal => FontId(0),
            Font::Monospace => FontId(1),
        }
    }
}

#[derive(Debug)]
pub struct Graphics2D {
    pub(crate) rects: InstanceBuffer<Rect>,
    pub(crate) glyph_brush: GlyphBrush<'static, GlyphInstance>,
    stack: Vec<State>,
    z_depth: f32,
    dimensions: WindowDimensions,
}

#[derive(Clone, Copy, Debug)]
pub struct Style {
    pub rect_color: [f32; 4],
    pub text_color: [f32; 4],
    pub font_size: Vector2<f32>,
    pub border_radius: f32,
    pub border_thickness: f32,
    pub border_color: [f32; 3],
    pub font: Font,
}

impl Default for Style {
    fn default() -> Style {
        Style {
            rect_color: [1.0, 1.0, 1.0, 1.0],
            text_color: [0.1, 0.1, 0.1, 1.0],
            font_size: Vector2::new(40.0, 40.0),
            border_radius: 0.0,
            border_thickness: 0.0,
            border_color: [0.0; 3],
            font: Font::Normal,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct State {
    pub transform: Matrix3<f32>,
    pub style: Style,
}

impl Default for State {
    fn default() -> State {
        State {
            transform: Matrix3::identity(),
            style: Style::default(),
        }
    }
}

const Z_SUB: f32 = 1.0 / 10000.0;
const NORMAL_FONT: &[u8] = include_bytes!("../../vendor/FiraSans/FiraSans-Regular.ttf");
const MONOSPACE_FONT: &[u8] = include_bytes!("../../vendor/FiraMono/FiraMono-Regular.ttf");

fn create_glyph_brush() -> GlyphBrush<'static, GlyphInstance> {
    let mut brush = GlyphBrushBuilder::using_font_bytes(NORMAL_FONT)
        .gpu_cache_align_4x4(true)
        .build();

    brush.add_font_bytes(MONOSPACE_FONT);

    brush
}

impl Graphics2D {
    pub fn new() -> Graphics2D {
        Graphics2D {
            rects: Default::default(),
            glyph_brush: create_glyph_brush(),
            stack: vec![State::default()],
            z_depth: 1.0,
            dimensions: Default::default(),
        }
    }

    pub(crate) fn recreate_glyph_brush(&mut self) {
        self.glyph_brush = create_glyph_brush();
    }

    pub fn begin(&mut self, world: &World) {
        self.z_depth = 1.0;
        self.rects.clear();
        self.dimensions = *Read::<WindowDimensions>::fetch(world);
    }

    pub fn state(&self) -> &State {
        self.stack.last().unwrap()
    }

    pub fn state_mut(&mut self) -> &mut State {
        self.stack.last_mut().unwrap()
    }

    pub fn style(&self) -> &Style {
        &self.state().style
    }

    pub fn style_mut(&mut self) -> &mut Style {
        &mut self.state_mut().style
    }

    pub fn apply_style(&mut self, style: Style) {
        *self.style_mut() = style;
    }

    pub fn push(&mut self) {
        self.stack.push(*self.state())
    }

    pub fn pop(&mut self) {
        self.stack.pop();
        if self.stack.is_empty() {
            panic!("Attempted to pop root Graphics2D state");
        }
    }

    pub fn set_font_size(&mut self, s: f32) {
        self.style_mut().font_size = Vector2::new(s, s);
    }

    pub fn set_text_color(&mut self, color: [f32; 4]) {
        self.style_mut().text_color = color;
    }

    pub fn set_rect_color(&mut self, color: [f32; 4]) {
        self.style_mut().rect_color = color;
    }

    pub fn set_border_radius(&mut self, radius: f32) {
        self.style_mut().border_radius = radius;
    }

    pub fn set_border_thickness(&mut self, thickness: f32) {
        self.style_mut().border_thickness = thickness;
    }

    pub fn set_border_color(&mut self, color: [f32; 3]) {
        self.style_mut().border_color = color;
    }

    pub fn set_font(&mut self, font: Font) {
        self.style_mut().font = font;
    }

    pub fn rect<P, S>(&mut self, pos: P, size: S)
    where
        P: Into<Vector2<f32>>,
        S: Into<Vector2<f32>>,
    {
        let pos = pos.into();
        let size = size.into();
        let transform = Matrix3::new_translation(&pos) * Matrix3::new_nonuniform_scaling(&size);
        let transform = transform.remove_row(2).transpose().into();

        self.z_depth -= Z_SUB;

        self.rects.add(Rect {
            transform,
            color: self.style().rect_color,
            border_radius: self.style().border_radius,
            border_thickness: self.style().border_thickness,
            border_color: self.style().border_color,
            z_depth: self.z_depth,
        });
    }

    pub fn text<P, S>(&mut self, pos: P, text: S)
    where
        P: Into<Vector2<f32>>,
        S: AsRef<str>,
    {
        let pos = pos.into();
        let text = text.as_ref();

        self.z_depth -= Z_SUB;

        let section = self.make_text_section(pos.x, pos.y, text);
        self.glyph_brush.queue(section)
    }

    pub fn measure_text<S: AsRef<str>>(&mut self, text: S) -> Size {
        let text = text.as_ref();
        let section = self.make_text_section(0.0, 0.0, text);
        self.glyph_brush
            .glyph_bounds(section)
            .map(|r| Size::new(r.width(), r.height()))
            .unwrap_or(Size::ZERO)
    }

    fn make_text_section<'a>(&self, x: f32, y: f32, text: &'a str) -> Section<'a> {
        let font_size = self.style().font_size;

        Section {
            text,
            screen_position: (x, y),
            color: self.style().text_color,
            scale: Scale {
                x: font_size.x,
                y: font_size.y,
            },
            z: self.z_depth,
            font_id: self.style().font.get_id(),
            ..Default::default()
        }
    }

    pub fn window_size(&self) -> (f64, f64) {
        self.dimensions.logical_size()
    }

    pub fn hidpi_factor(&self) -> f64 {
        self.dimensions.hidpi_factor()
    }
}

impl Default for Graphics2D {
    fn default() -> Graphics2D {
        Graphics2D::new()
    }
}
