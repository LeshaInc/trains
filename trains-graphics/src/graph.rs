// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use rendy::{
    command::Families,
    factory::Factory,
    graph::{
        present::PresentNode,
        render::{RenderGroupBuilder, SimpleGraphicsPipeline},
        Graph, GraphBuilder,
    },
    hal::{
        command::{ClearDepthStencil, ClearValue},
        format::Format,
        Backend,
    },
};

use failure::Fallible;
use specs::prelude::*;

use super::pass::*;
use crate::window::{WindowDimensions, WindowResource};

#[derive(Default)]
pub struct GraphFactory {
    surface_format: Option<Format>,
}

impl GraphFactory {
    pub fn new() -> GraphFactory {
        Default::default()
    }

    pub fn build<B: Backend>(
        &mut self,
        factory: &mut Factory<B>,
        families: &mut Families<B>,
        world: &World,
    ) -> Fallible<Graph<B, World>> {
        let window = ReadExpect::<WindowResource>::fetch(world);
        let dimensions = ReadExpect::<WindowDimensions>::fetch(world);
        let window_kind = dimensions.image_kind();

        let surface = factory.create_surface(&window);
        let surface_format = self
            .surface_format
            .unwrap_or_else(|| factory.get_surface_format(&surface));
        self.surface_format = Some(surface_format);

        let mut gb = GraphBuilder::new();

        let out_color = gb.create_image(window_kind, 1, surface_format, None);

        let color0 = gb.create_image(window_kind, 1, Format::Rgba32Sfloat, None);
        let color1 = gb.create_image(window_kind, 1, Format::Rgba32Sfloat, None);
        let color2 = gb.create_image(window_kind, 1, Format::Rgba32Sfloat, None);
        let color3 = gb.create_image(window_kind, 1, Format::Rgba32Sfloat, None);

        let depth = gb.create_image(
            window_kind,
            1,
            Format::D16Unorm,
            Some(ClearValue::DepthStencil(ClearDepthStencil(1.0, 0))),
        );

        let depth2d = gb.create_image(
            window_kind,
            1,
            Format::D16Unorm,
            Some(ClearValue::DepthStencil(ClearDepthStencil(1.0, 0))),
        );

        let position = gb.create_image(
            window_kind,
            1,
            Format::Rgba32Sfloat,
            Some(ClearValue::Color([0.0, 0.0, 0.0, 1.0].into())),
        );

        let albedo = gb.create_image(
            window_kind,
            1,
            Format::Rgba8Unorm,
            Some(ClearValue::Color([0.0, 0.0, 0.0, 1.0].into())),
        );

        let normal = gb.create_image(
            window_kind,
            1,
            Format::Rgba8Unorm,
            Some(ClearValue::Color([1.0, 1.0, 1.0, 1.0].into())),
        );

        let metalness_roughness_ao = gb.create_image(
            window_kind,
            1,
            Format::Rgba8Unorm,
            Some(ClearValue::Color([0.0, 0.0, 0.0, 1.0].into())),
        );

        let terrain = gb.add_node(
            TerrainPipeline::builder()
                .into_subpass()
                .with_color(position)
                .with_color(albedo)
                .with_color(normal)
                .with_color(metalness_roughness_ao)
                .with_depth_stencil(depth)
                .into_pass(),
        );

        let deferred = gb.add_node(
            DeferredPipeline::builder()
                .with_dependency(terrain)
                .with_image(position)
                .with_image(albedo)
                .with_image(normal)
                .with_image(metalness_roughness_ao)
                .into_subpass()
                .with_color(color0)
                .into_pass(),
        );

        let blurv = gb.add_node(
            ProcessingPipeline::<_, BlurV>::builder()
                .with_dependency(deferred)
                .with_image(color0)
                .into_subpass()
                .with_color(color1)
                .into_pass(),
        );

        let blurh = gb.add_node(
            ProcessingPipeline::<_, BlurH>::builder()
                .with_dependency(blurv)
                .with_image(color1)
                .into_subpass()
                .with_color(color2)
                .into_pass(),
        );

        let fxaa = gb.add_node(
            ProcessingPipeline::<_, FXAA>::builder()
                .with_dependency(deferred)
                .with_image(color0)
                .into_subpass()
                .with_color(color3)
                .into_pass(),
        );

        let tone_mapping = gb.add_node(
            ProcessingPipeline::<_, ToneMapping>::builder()
                .with_dependency(fxaa)
                .with_image(color3)
                .into_subpass()
                .with_color(out_color)
                .into_pass(),
        );

        let rects2d = gb.add_node(
            Rects2DPipeline::builder()
                .with_dependency(blurh)
                .with_dependency(tone_mapping)
                .with_image(color2)
                .into_subpass()
                .with_color(out_color)
                .with_depth_stencil(depth2d)
                .into_pass(),
        );

        let text2d = gb.add_node(
            Text2DPipeline::builder()
                .with_dependency(rects2d)
                .into_subpass()
                .with_color(out_color)
                .with_depth_stencil(depth2d)
                .into_pass(),
        );

        gb.add_node(PresentNode::builder(&factory, surface, out_color).with_dependency(text2d));

        gb.build(factory, families, world)
    }
}
