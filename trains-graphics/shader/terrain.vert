// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inPos;

layout(location = 0) out vec3 fNormal;
layout(location = 1) out vec3 fBary;
layout(location = 2) out vec3 fPosition;

layout(std140, set = 0, binding = 0) uniform ViewData {
    mat4 viewProjection;
    vec3 viewPosition;
} viewData;

layout(set = 1, binding = 0) uniform usampler2D uTerrain;

const vec3 BARYCENTRIC[3] = vec3[3](
    vec3(1.0, 0.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 0.0, 1.0)
);

void main() {
    float heightL = textureOffset(uTerrain, inPos, ivec2( 1,  0)).r;
	float heightR = textureOffset(uTerrain, inPos, ivec2(-1,  0)).r;
	float heightU = textureOffset(uTerrain, inPos, ivec2( 0,  1)).r;
	float heightD = textureOffset(uTerrain, inPos, ivec2( 0, -1)).r;

    fNormal = normalize(vec3(heightR - heightL, heightD - heightU, 64.0 * 4.0));
    fBary = BARYCENTRIC[int(gl_VertexIndex) % 3];

    float height = float(texture(uTerrain, inPos).r) / 65535.0 * 0.5;
    vec3 pos = vec3(inPos * 2.0 - vec2(1.0, 1.0), height);

    fPosition = pos;
    gl_Position = viewData.viewProjection * vec4(pos, 1.0);
}
