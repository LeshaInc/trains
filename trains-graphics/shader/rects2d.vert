// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inTransform0;
layout(location = 1) in vec3 inTransform1;
layout(location = 2) in float inZDepth;
layout(location = 3) in vec4 inColor;
layout(location = 4) in float inBorderRadius;
layout(location = 5) in float inBorderThickness;
layout(location = 6) in vec3 inBorderColor;

layout(location = 0) out vec4 fColor;
layout(location = 1) out vec2 fScreenUV;
layout(location = 2) out vec2 fUV;
layout(location = 3) out vec2 fSize;
layout(location = 4) out float fBorderRadius;
layout(location = 5) out float fBorderThickness;
layout(location = 6) out vec3 fBorderColor;

const vec2 VERTICES[6] = vec2[6](
    vec2(0.0, 0.0),
    vec2(1.0, 0.0),
    vec2(0.0, 1.0),
    vec2(0.0, 1.0),
    vec2(1.0, 0.0),
    vec2(1.0, 1.0)
);

layout(std140, set = 0, binding = 0) uniform ViewData {
    mat4 viewProjection;
    vec3 viewPosition;
    mat3 projection2D;
} viewData;

void main() {
    fColor = inColor;
    fBorderRadius = inBorderRadius;
    fBorderThickness = inBorderThickness;
    fBorderColor = inBorderColor;

    float sx = length(inTransform0.xy);
    float sy = length(inTransform1.xy);
    fSize = vec2(sx, sy);

    mat3 model = transpose(mat3(inTransform0, inTransform1, vec3(0, 0, 1)));
    vec2 uv = VERTICES[gl_VertexIndex];
    vec3 position = viewData.projection2D * model * vec3(uv, 1.0);

    fUV = uv * vec2(sx, sy);
    fScreenUV = position.xy * 0.5 + 0.5;

    gl_Position = vec4(position.xy, inZDepth, 1.0);
}
