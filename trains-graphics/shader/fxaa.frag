// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

const float EDGE_THRESHOLD_MIN = 0.0312;
const float EDGE_THRESHOLD_MAX = 0.125;
const float SUBPIXEL_QUALITY = 0.75;
const int ITERATIONS = 12;

layout(location = 0) in vec2 fUV;
layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler2D uInput;

float QUALITY[12] = float[12](
    1.0,
    1.0,
    1.0,
    1.0,
    1.0,
    1.5,
    2.0,
    2.0,
    2.0,
    2.0,
    4.0,
    8.0
);

float rgb2luma(vec3 rgb) {
    return sqrt(dot(rgb, vec3(0.299, 0.587, 0.114)));
}

// credit: http://blog.simonrodriguez.fr/articles/30-07-2016_implementing_fxaa.html#ref5
void main() {
    ivec2 screenSize = textureSize(uInput, 0);
    vec2 inverseScreenSize = vec2(1.0 / screenSize.x, 1.0 / screenSize.y);

    vec3 inColor = texture(uInput, fUV).rgb;

    // Luma at the current fragment
    float lumaC = rgb2luma(inColor);

    // Luma at the four direct neighbours of the current fragment.
    float lumaD = rgb2luma(textureOffset(uInput, fUV, ivec2( 0,-1)).rgb);
    float lumaU = rgb2luma(textureOffset(uInput, fUV, ivec2( 0, 1)).rgb);
    float lumaL = rgb2luma(textureOffset(uInput, fUV, ivec2(-1, 0)).rgb);
    float lumaR = rgb2luma(textureOffset(uInput, fUV, ivec2( 1, 0)).rgb);

    // Find the maximum and minimum luma around the current fragment.
    float lumaMin = min(lumaC, min(min(lumaD, lumaU), min(lumaL, lumaR)));
    float lumaMax = max(lumaC, max(max(lumaD, lumaU), max(lumaL, lumaR)));

    // Compute the delta.
    float lumaRange = lumaMax - lumaMin;

    // If the luma variation is lower that a threshold (or if we are in a really dark area), we are not on an edge, don't perform any AA.
    if (lumaRange < max(EDGE_THRESHOLD_MIN, lumaMax * EDGE_THRESHOLD_MAX)){
        outColor = vec4(inColor, 1.0);
        return;
    }

    // Query the 4 remaining corners lumas.
    float lumaDL = rgb2luma(textureOffset(uInput, fUV, ivec2(-1,-1)).rgb);
    float lumaUR = rgb2luma(textureOffset(uInput, fUV, ivec2( 1, 1)).rgb);
    float lumaUL = rgb2luma(textureOffset(uInput, fUV, ivec2(-1, 1)).rgb);
    float lumaDR = rgb2luma(textureOffset(uInput, fUV, ivec2( 1,-1)).rgb);

    // Combine the four edges lumas (using intermediary variables for future computations with the same values).
    float lumaDU = lumaD + lumaU;
    float lumaLR = lumaL + lumaR;

    // Same for corners
    float lumaLL = lumaDL + lumaUL;
    float lumaDD = lumaDL + lumaDR;
    float lumaRR = lumaDR + lumaUR;
    float lumaUU = lumaUR + lumaUL;

    // Compute an estimation of the gradient along the horizontal and vertical axis.
    float edgeH = abs(-2.0 * lumaL + lumaLL) + abs(-2.0 * lumaC + lumaDU ) * 2.0 + abs(-2.0 * lumaR + lumaRR);
    float edgeV = abs(-2.0 * lumaU + lumaUU) + abs(-2.0 * lumaC + lumaLR)  * 2.0 + abs(-2.0 * lumaD + lumaDD);

    // Is the local edge horizontal or vertical?
    bool isHorizontal = edgeH >= edgeV;

    // Select the two neighboring texels lumas in the opposite direction to the local edge.
    float luma1 = isHorizontal ? lumaD : lumaL;
    float luma2 = isHorizontal ? lumaU : lumaR;

    // Compute gradients in this direction.
    float gradient1 = luma1 - lumaC;
    float gradient2 = luma2 - lumaC;

    // Which direction is the steepest?
    bool is1Steepest = abs(gradient1) >= abs(gradient2);

    // Gradient in the corresponding direction, normalized.
    float gradientScaled = 0.25 * max(abs(gradient1), abs(gradient2));

    // Choose the step size (one pixel) according to the edge direction.
    float stepLength = isHorizontal ? inverseScreenSize.y : inverseScreenSize.x;

    // Average luma in the correct direction.
    float lumaLocalAverage = 0.0;

    if (is1Steepest) {
        // Switch the direction
        stepLength = -stepLength;
        lumaLocalAverage = 0.5 * (luma1 + lumaC);
    } else {
        lumaLocalAverage = 0.5 * (luma2 + lumaC);
    }

    // Shift UV in the correct direction by half a pixel.
    vec2 UV = fUV;
    if (isHorizontal) {
        UV.y += stepLength * 0.5;
    } else {
        UV.x += stepLength * 0.5;
    }

    // Compute offset (for each iteration step) in the right direction.
    vec2 offset = isHorizontal ? vec2(inverseScreenSize.x, 0.0) : vec2(0.0, inverseScreenSize.y);
    // Compute UVs to explore on each side of the edge, orthogonally. The QUALITY allows us to step faster.
    vec2 UV1 = UV - offset;
    vec2 UV2 = UV + offset;

    // Read the lumas at both current extremities of the exploration segment, and compute the delta wrt to the local average luma.
    float lumaEnd1 = rgb2luma(texture(uInput, UV1).rgb);
    float lumaEnd2 = rgb2luma(texture(uInput, UV2).rgb);
    lumaEnd1 -= lumaLocalAverage;
    lumaEnd2 -= lumaLocalAverage;

    // If the luma deltas at the current extremities are largerthan the local gradient, we have reached the side of the edge.
    bool reached1 = abs(lumaEnd1) >= gradientScaled;
    bool reached2 = abs(lumaEnd2) >= gradientScaled;
    bool reachedBoth = reached1 && reached2;

    // If the side is not reached, we continue to explore in this direction.
    if (!reached1) {
        UV1 -= offset;
    }

    if (!reached2) {
        UV2 += offset;
    }

    if (!reachedBoth) {
        for (int i = 2; i < ITERATIONS; i++){
            // If needed, read luma in 1st direction, compute delta.
            if (!reached1) {
                lumaEnd1 = rgb2luma(texture(uInput, UV1).rgb);
                lumaEnd1 = lumaEnd1 - lumaLocalAverage;
            }

            // If needed, read luma in opposite direction, compute delta.
            if (!reached2) {
                lumaEnd2 = rgb2luma(texture(uInput, UV2).rgb);
                lumaEnd2 = lumaEnd2 - lumaLocalAverage;
            }

            // If the luma deltas at the current extremities is larger than the local gradient, we have reached the side of the edge.
            reached1 = abs(lumaEnd1) >= gradientScaled;
            reached2 = abs(lumaEnd2) >= gradientScaled;
            reachedBoth = reached1 && reached2;

            // If the side is not reached, we continue to explore in this direction, with a variable quality.
            if (!reached1) {
                UV1 -= offset * QUALITY[i];
            }

            if (!reached2) {
                UV2 += offset * QUALITY[i];
            }

            // If both sides have been reached, stop the exploration.
            if (reachedBoth) break;
        }
    }

    // Compute the distances to each extremity of the edge.
    float distance1 = isHorizontal ? (fUV.x - UV1.x) : (fUV.y - UV1.y);
    float distance2 = isHorizontal ? (UV2.x - fUV.x) : (UV2.y - fUV.y);

    // In which direction is the extremity of the edge closer ?
    bool isDirection1 = distance1 < distance2;
    float distanceFinal = min(distance1, distance2);

    // Length of the edge.
    float edgeThickness = distance1 + distance2;

    // UV offset: read in the direction of the closest side of the edge.
    float pixelOffset = -distanceFinal / edgeThickness + 0.5;

    // Is the luma at center smaller than the local average ?
    bool isLumaCenterSmaller = lumaC < lumaLocalAverage;

    // If the luma at center is smaller than at its neighbour, the delta luma at each end should be positive (same variation).
    // (in the direction of the closer side of the edge.)
    bool correctVariation = ((isDirection1 ? lumaEnd1 : lumaEnd2) < 0.0) != isLumaCenterSmaller;

    // If the luma variation is incorrect, do not offset.
    float finalOffset = correctVariation ? pixelOffset : 0.0;

    // Sub-pixel shifting
    // Full weighted average of the luma over the 3x3 neighborhood.
    float lumaAverage = (1.0 / 12.0) * (2.0 * (lumaDU + lumaLR) + lumaLL + lumaRR);
    // Ratio of the delta between the global average and the center luma, over the luma range in the 3x3 neighborhood.
    float subPixelOffset1 = clamp(abs(lumaAverage - lumaC) / lumaRange, 0.0, 1.0);
    float subPixelOffset2 = (-2.0 * subPixelOffset1 + 3.0) * subPixelOffset1 * subPixelOffset1;
    // Compute a sub-pixel offset based on this delta.
    float subPixelOffsetFinal = subPixelOffset2 * subPixelOffset2 * SUBPIXEL_QUALITY;

    // Pick the biggest of the two offsets.
    finalOffset = max(finalOffset,subPixelOffsetFinal);

    // Compute the final UV coordinates.
    vec2 finalUV = fUV;

    if (isHorizontal) {
        finalUV += finalOffset * stepLength;
    } else {
        finalUV += finalOffset * stepLength;
    }

    outColor = texture(uInput, finalUV);
}
