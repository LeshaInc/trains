// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 fColor;
layout(location = 1) in vec2 fScreenUV;
layout(location = 2) in vec2 fUV;
layout(location = 3) in vec2 fSize;
layout(location = 4) in float fBorderRadius;
layout(location = 5) in float fBorderThickness;
layout(location = 6) in vec3 fBorderColor;

layout(location = 0) out vec4 outColor;

layout(set = 1, binding = 0) uniform sampler2D uScreenBlurred;

float sdf(vec2 pos, float radius, float spread) {
    vec2 edge = abs(pos) - vec2(fSize.x, fSize.y) + 2.0 * radius;

    float outside = length(max(edge, vec2(0)));
    float inside = min(max(edge.x, edge.y), 0.0);

    return outside + inside - radius * 2.0;
}

void main() {
    vec3 background = texture(uScreenBlurred, fScreenUV).rgb;

    float dist = sdf(fUV * 2.0 - fSize, fBorderRadius, 0.0);
    float distChange = fwidth(dist) * 0.5;

    float alpha = smoothstep(distChange, -distChange, dist);
    vec3 color = fColor.a * (fColor.rgb - background) + background;

    if (fBorderThickness > 0.0) {
        float border = smoothstep(distChange, -distChange, dist + fBorderThickness * 2.0);
        color = mix(fBorderColor, color, border);
    }

    outColor = vec4(color, alpha);
}
