// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fUV;

layout(location = 0) out vec4 outColor;

layout(std140, set = 0, binding = 0) uniform ViewData {
    mat4 viewProjection;
    vec3 viewPosition;
    mat3 projection2D;
} viewData;

layout(set = 1, binding = 0) uniform sampler2D uPosition;
layout(set = 1, binding = 1) uniform sampler2D uAlbedo;
layout(set = 1, binding = 2) uniform sampler2D uNormal;
layout(set = 1, binding = 3) uniform sampler2D uMetalnessRoughnessAO;

const float PI = 3.14159265359;

// Schlick's approximation of the specular reflection coefficient
// * `aNV` - cosine of the angle between the surface normal and the view direction
// * `R0` - base reflectivity
vec3 fresnel(float aNV, vec3 R0) {
    return R0 + (1.0 - R0) * pow(1.0 - aNV, 5.0);
}

// Trowbridge-Reitz GGX normal distribution function
// * `aNH` - cosine of the angle between the surface normal and the halfway vector
float normalDistributionGGX(float aNH, float roughness) {
    float roughnessSq = roughness * roughness;
    float a = aNH * aNH * (roughnessSq - 1.0) + 1.0;
    return roughnessSq / (PI * a * a);
}

// Schlick GGX geometry function
float geometryGGXSub(float angle, float roughness) {
    roughness += 1;
    float k = roughness * roughness / 4.0;
    return angle / (angle * (1.0 - k) + k);
}

// Schlick-Smith GGX geometry function
// * `aNV` - cosine of the angle between the surface normal and the view direction
// * `aNL` - cosine of the angle between the surface normal and the light direction
float geometryGGX(float aNV, float aNL, float roughness) {
    return geometryGGXSub(aNV, roughness) * geometryGGXSub(aNL, roughness);
}

void main() {
    vec3 position = texture(uPosition, fUV).rgb;
    vec3 albedo = texture(uAlbedo, fUV).rgb;
    vec3 normal = normalize(texture(uNormal, fUV).rgb * 2.0 - 1.0);
    vec3 metalnessRoughnessAO = texture(uMetalnessRoughnessAO, fUV).rgb;
    float metalness = metalnessRoughnessAO.r;
    float roughness = metalnessRoughnessAO.g;
    float ao = metalnessRoughnessAO.b;

    roughness *= roughness;

    vec3 lightColor = vec3(1.0, 1.0, 1.0) * 1.0;
    vec3 lightPos = vec3(1.0, 1.0, 2.0);

    vec3 lightDir = normalize(vec3(0.5, 0.5, 0.9));
    // float lightDist = length(lightDir);
    // lightDir /= lightDist;

    // float attenuation = 1.0 / (lightDist * lightDist);
    // attenuation = 1.0;
    vec3 radiance = lightColor;

    vec3 viewDir = normalize(viewData.viewPosition - position);
    vec3 halfway = normalize(viewDir + lightDir);

    float aNV = max(dot(normal, viewDir), 0.001);
    float aNL = max(dot(normal, lightDir), 0.0);
    float aNH = max(dot(normal, halfway), 0.0);

    vec3 R0 = mix(vec3(0.04), albedo, metalness);

    float D = normalDistributionGGX(aNH, roughness);
    vec3  F = fresnel(aNV, R0);
    float G = geometryGGX(aNV, aNL, roughness);
    vec3 DFG = D * F * G;

    vec3 specular = DFG / max(4.0 * aNV * aNL, 0.001);

    vec3 kS = vec3(F);
    vec3 kD = (vec3(1.0) - kS) * (1.0 - metalness);

    vec3 lightOut = 0.1 * albedo + (kD * albedo / PI + specular) * radiance * aNL;

    outColor = vec4(lightOut, 1.0);
}
