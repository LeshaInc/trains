// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fNormal;
layout(location = 1) in vec3 fBary;
layout(location = 2) in vec3 fPosition;

layout(location = 0) out vec3 outPosition;
layout(location = 1) out vec3 outAlbedo;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec3 outMetalnessRoughnessAO;

layout(std140, set = 0, binding = 0) uniform ViewData {
    mat4 viewProjection;
    vec4 viewPosition;
} viewData;

layout(set = 1, binding = 0) uniform usampler2D uTerrain;

float edgeFactor() {
    vec3 d = fwidth(fBary);
    vec3 a3 = smoothstep(vec3(0.0), d * 1.5, fBary);
    return min(min(a3.x, a3.y), a3.z);
}

void main() {
    float slope = 1.0 - fNormal.z;

    vec3 grassColor = vec3(0.156, 0.5, 0.133);
    vec3 rockColor = vec3(0.49, 0.35, 0.31);

    float minGrassSlope = 0.23;
    float maxGrassHeight = 0.05;
    float blending = 0.5;

    float a = minGrassSlope * (1.0 - blending);
    float b = 1.0 - clamp((slope - a) / (minGrassSlope - a), 0.0, 1.0);
    b *= 1.0 - pow(smoothstep(0.0, maxGrassHeight, fPosition.z), 3.0);

    outAlbedo = mix(rockColor, grassColor, b);

    outPosition = fPosition;
    outNormal = fNormal * 0.5 + 0.5;
    outMetalnessRoughnessAO = vec3(0.0, 0.5, 1.0);
}
