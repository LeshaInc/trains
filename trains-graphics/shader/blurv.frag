// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fUV;
layout(location = 0) out vec4 outColor;

layout(set = 0, binding = 0) uniform sampler2D uInput;

void main() {
    ivec2 size = textureSize(uInput, 0);
    vec2 unit = vec2(1.0 / size.x, 1.0 / size.y);
    vec4 acc = vec4(0.0);

    acc += texture(uInput, fUV + unit * vec2(0, -20)) * 0.0568;
    acc += texture(uInput, fUV + unit * vec2(0, -16)) * 0.0772;
    acc += texture(uInput, fUV + unit * vec2(0, -12)) * 0.0931;
    acc += texture(uInput, fUV + unit * vec2(0,  -8)) * 0.1045;
    acc += texture(uInput, fUV + unit * vec2(0,  -4)) * 0.1113;
    acc += texture(uInput, fUV + unit * vec2(0,   0)) * 0.1136;
    acc += texture(uInput, fUV + unit * vec2(0,   4)) * 0.1113;
    acc += texture(uInput, fUV + unit * vec2(0,   8)) * 0.1045;
    acc += texture(uInput, fUV + unit * vec2(0,  12)) * 0.0931;
    acc += texture(uInput, fUV + unit * vec2(0,  16)) * 0.0772;
    acc += texture(uInput, fUV + unit * vec2(0,  20)) * 0.0568;

    outColor = acc;
}
