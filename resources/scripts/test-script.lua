return {
  update = function()
    g2d.push()

    local width, height = g2d.getWindowSize()

    g2d.setFontSize(16)
    g2d.setFont(g2d.Font.Normal)

    local text = "Hello from Lua!"
    local textWidth, textHeight = g2d.measureText(text)

    g2d.setTextColor(1, 1, 1, 1)
    g2d.text(width - textWidth, height - textHeight, text)

    g2d.pop()
  end
}
