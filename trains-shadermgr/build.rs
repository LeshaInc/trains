// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{
    env,
    fs::{self, File},
    io::Write,
    path::Path,
};

use failure::Fallible;
use shaderc::{CompileOptions, Compiler, OptimizationLevel, ShaderKind, SourceLanguage};
use walkdir::WalkDir;

fn main() -> Fallible<()> {
    if env::var("PROFILE")? == "debug" {
        return Ok(());
    }

    let out_dir = env::var("OUT_DIR")?;
    let search_path = concat!(env!("CARGO_MANIFEST_DIR"), "/shader/");

    for entry in WalkDir::new(search_path).follow_links(true) {
        let entry = entry?;

        if !entry.file_type().is_file() {
            continue;
        }

        let path = entry.path();

        let extension = match path.extension().and_then(|s| s.to_str()) {
            Some(v) => v,
            None => continue,
        };

        let kind = match extension {
            "tesc" => ShaderKind::TessControl,
            "tese" => ShaderKind::TessEvaluation,
            "vert" => ShaderKind::Vertex,
            "geom" => ShaderKind::Geometry,
            "frag" => ShaderKind::Fragment,
            "comp" => ShaderKind::Compute,
            _ => continue,
        };

        let source = fs::read_to_string(entry.path())?;

        let mut compiler = Compiler::new().expect("can't initialize shaderc");
        let mut options = CompileOptions::new().expect("can't initialize shaderc");
        options.set_warnings_as_errors();
        options.set_optimization_level(OptimizationLevel::Performance);
        options.set_source_language(SourceLanguage::GLSL);

        let res = compiler.compile_into_spirv(
            &source,
            kind,
            path.to_str().unwrap(),
            "main",
            Some(&options),
        );

        let artifact = match res {
            Ok(v) => v,
            Err(e) => {
                eprintln!("Could not compile shader: {}\n{}", path.display(), e);
                return Err(e.into());
            }
        };

        let spirv = artifact.as_binary_u8();

        let out_path = Path::new(&out_dir)
            .join("shader")
            .join(path.strip_prefix(search_path).unwrap());
        drop(fs::create_dir_all(out_path.parent().unwrap()));

        let mut file = File::create(out_path)?;
        file.write_all(spirv)?;
    }

    Ok(())
}
