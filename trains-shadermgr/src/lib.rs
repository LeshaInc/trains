// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::borrow::Cow;

use failure::Error;
pub use gfx_hal::pso::ShaderStageFlags;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Shader {
    pub path: &'static str,
    pub stage: ShaderStageFlags,
    pub entry: &'static str,
    pub spirv: Option<&'static [u8]>,
}

impl rendy_shader::Shader for Shader {
    #[cfg(not(debug_assertions))]
    fn spirv(&self) -> Result<Cow<[u8]>, Error> {
        Ok(self.spirv.unwrap().into())
    }

    #[cfg(debug_assertions)]
    fn spirv(&self) -> Result<Cow<[u8]>, Error> {
        use shaderc::{CompileOptions, Compiler, OptimizationLevel, ShaderKind, SourceLanguage};

        let source = std::fs::read_to_string(self.path)?;

        let kind = match self.stage {
            ShaderStageFlags::HULL => ShaderKind::TessControl,
            ShaderStageFlags::DOMAIN => ShaderKind::TessEvaluation,
            ShaderStageFlags::VERTEX => ShaderKind::Vertex,
            ShaderStageFlags::GEOMETRY => ShaderKind::Geometry,
            ShaderStageFlags::FRAGMENT => ShaderKind::Fragment,
            ShaderStageFlags::COMPUTE => ShaderKind::Compute,
            _ => panic!("invalid shader stage"),
        };

        let mut compiler = Compiler::new().expect("can't initialize shaderc");

        let mut options = CompileOptions::new().expect("can't initialize shaderc");
        options.set_warnings_as_errors();
        options.set_optimization_level(OptimizationLevel::Performance);
        options.set_source_language(SourceLanguage::GLSL);

        let res =
            compiler.compile_into_spirv(&source, kind, &self.path, &self.entry, Some(&options));

        let artifact = match res {
            Ok(v) => v,
            Err(e) => {
                log::error!("Could not compile shader: {}\n{}", self.path, e);

                return Err(e.into());
            }
        };

        let spirv = artifact.as_binary_u8().to_vec().into();
        Ok(spirv)
    }

    fn entry(&self) -> &str {
        self.entry
    }

    fn stage(&self) -> ShaderStageFlags {
        self.stage
    }
}

#[macro_export]
#[cfg(debug_assertions)]
macro_rules! include_shader {
    (hull, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::HULL, $name, $entry)
    };

    (domain, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::DOMAIN, $name, $entry)
    };

    (vertex, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::VERTEX, $name, $entry)
    };

    (geometry, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::GEOMETRY, $name, $entry)
    };

    (fragment, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::FRAGMENT, $name, $entry)
    };

    (compute, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::COMPUTE, $name, $entry)
    };

    ($stage:expr, $name:expr, $entry:expr) => {
        Shader {
            path: concat!(env!("CARGO_MANIFEST_DIR"), "/shader/", $name),
            stage: $stage,
            entry: $entry,
            spirv: None,
        }
    };

    ($v:tt, $name:expr) => {
        include_shader!($v, $name, "main")
    };
}

#[macro_export]
#[cfg(not(debug_assertions))]
macro_rules! include_shader {
    (hull, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::HULL, $name, $entry)
    };

    (domain, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::DOMAIN, $name, $entry)
    };

    (vertex, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::VERTEX, $name, $entry)
    };

    (geometry, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::GEOMETRY, $name, $entry)
    };

    (fragment, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::FRAGMENT, $name, $entry)
    };

    (compute, $name:expr, $entry:expr) => {
        include_shader!(trains_shadermgr::ShaderStageFlags::COMPUTE, $name, $entry)
    };

    ($stage:expr, $name:expr, $entry:expr) => {
        Shader {
            path: concat!(env!("CARGO_MANIFEST_DIR"), "/shader/", $name),
            stage: $stage,
            entry: $entry,
            spirv: Some(include_bytes!(concat!(env!("OUT_DIR"), "/shader/", $name))),
        }
    };

    ($v:tt, $name:expr) => {
        include_shader!($v, $name, "main")
    };
}
