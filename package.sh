#!/bin/bash

cmd() {
    echo "$" $@ 1>&2
    $@
}

cmd mkdir -p package

if [ -e target/release/trains ]; then
    cmd cp target/release/trains package/
    cmd cp target/release/libtrains_api.so package/libtrains_api.so
elif [ -e target/debug/trains ]; then
    cmd cp target/debug/trains package/
    cmd cp target/debug/libtrains_api.so package/
elif [ -e target/debug/trains.exe ]; then
    cmd cp target/debug/trains.exe package/
    cmd cp target/debug/trains_api.dll package/
elif [ -e target/release/trains.exe ]; then
    cmd cp target/release/trains.exe package/
    cmd cp target/release/trains_api.dll package/
elif [ -e target/x86_64-pc-windows-gnu/debug/trains.exe ]; then
    cmd cp target/x86_64-pc-windows-gnu/debug/trains.exe package/
    cmd cp target/x86_64-pc-windows-gnu/debug/trains_api.dll package/
elif [ -e target/x86_64-pc-windows-gnu/release/trains.exe ]; then
    cmd cp target/x86_64-pc-windows-gnu/release/trains.exe package/
    cmd cp target/x86_64-pc-windows-gnu/release/trains_api.dll package/
else
    echo "ERROR: build artifacts not found" 1>&2
    exit 1
fi

cmd find resources -type d -exec mkdir -p package/{} \;

extensions=(lua)
for extension in "${extensions[@]}"; do
    cmd find resources -type f -name "*.$extension" -exec cp -f {} package/{} \;
done

cmd cp COPYING package/
