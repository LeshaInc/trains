# Trains

A very ambitious game written in Rust aiming to provide pleasure of OpenTTD
with modern 3D graphics. **Work in progress.**

## Screenshot

![screenshot](docs/screenshot.png)

## Prebuilt binaries

 - [For Linux x86_64](http://leshainc.gitlab.io/trains/trains-linux64.zip)
 - [For Windows x86_64](http://leshainc.gitlab.io/trains/trains-win64.zip)

## Building

[Install stable Rust.](https://www.rust-lang.org/tools/install)

Make sure you have [Git], GNU Make, [CMake], [Python] (either 2.x or 3.x), and a C++11 compiler installed
and available in `PATH`. Additionaly, you will need [Ninja] if building on Windows.

```shell
$ git clone https://gitlab.com/LeshaInc/trains.git --recursive
$ cd trains

# To compile a debug build with slow safety checks and no optimizations:
$ cargo build
$ cd trains-api
$ cargo build
$ cd ..

# To compile an optimized release build without slow safety checks:
$ cargo build --release --features rendy/no-slow-safety-checks
$ cd trains-api
$ cargo build --release
$ cd ..

# After that, run
$ bash package.sh
# The result will be in the `package/` directory (and in the `package.zip` archive)
```

[Git]: https://git-scm.com/
[CMake]: https://cmake.org/
[Python]: https://www.python.org/
[Ninja]: https://ninja-build.org/

## License

This project is licensed under GNU General Public License version 3 or (at your opinion)
any later version. For more info please read [COPYING](COPYING) file in the root of repository.

This project includes source code of LuaJIT, which is released under the MIT license, with
additional patches from OpenResty. For more info please read
[COPYRIGHT](https://github.com/openresty/luajit2/blob/v2.1-agentzh/COPYRIGHT) file in `trains-luajit/luajit2/`.
