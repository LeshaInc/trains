-- This file is part of Trains.
-- Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
--
-- Trains is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- Trains is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with Trains. If not, see <https://www.gnu.org/licenses/>.

--- Trains LuaJIT API.
-- @module api

local API_HEADERS, API_PATH = ...
local ffi = require("ffi")

ffi.cdef(API_HEADERS);
local raw = ffi.load(API_PATH)

local function str2lua(str)
  local converted = ffi.string(str.data, str.length)
  raw.tapi_free_str(str)
  return converted
end

local ApiStr = ffi.typeof("ApiStr");

local function lua2str(str)
  return ApiStr(str, #str, false)
end

local function checkError(code, func)
  if code ~= raw.API_ERROR_OK then
    error(func .. " call failed (error code " .. tonumber(code) .. ")")
  end
end

--- Engine functions
-- @section engine

engine = {}

--- Get engine version.
-- @treturn string Version as `"trains-api x.x.x"`
function engine.getVersion()
  return str2lua(raw.tapi_version())
end

--- 2D graphics functions
-- @section g2d

g2d = {}

---  Draw a rectangle.
--
-- Appearance is affected by @{g2d.setRectColor}, @{g2d.setBorderColor},
-- @{g2d.setBorderRadius}, and @{g2d.setBorderThickness} functions.
--
-- @tparam number x X coordinate of the rectangle
-- @tparam number y Y coordinate of the rectangle
-- @tparam number w Width of the rectangle
-- @tparam number h Height of the rectangle
function g2d.rect(x, y, w, h)
  raw.tg2d_rect(ENGINE, x, y, w, h)
end

---  Draw text.
--
-- Appearance is affected by `g2d.setFontSize`, and `g2d.setTextColor` functions.
--
-- @tparam number x X coordinate of the upper-left text corner
-- @tparam number y Y coordinate of the upper-left text corner
-- @tparam string text Text to draw. Must be a valid UTF-8 byte sequence, otherwise an error is thrown
function g2d.text(x, y, text)
  checkError(raw.tg2d_text(ENGINE, x, y, lua2str(text)), "g2d.text")
end

---  Lossy version of @{g2d.text}
--
-- Appearance is affected by `g2d.setFontSize`, and `g2d.setTextColor` functions.
--
-- @tparam number x X coordinate of the upper-left text corner
-- @tparam number y Y coordinate of the upper-left text corner
-- @tparam string text Text to draw. Invalid UTF-8 is replaced with the replacement character (�)
function g2d.textLossy(x, y, text)
  raw.tg2d_text_lossy(ENGINE, x, y, lua2str(text))
end

local TextMeasurement = ffi.typeof("TextMeasurement");

---  Measure text to get its bounding box.
--
--  Affected by the font size set using @{g2d.setFontSize} function.
--
-- @tparam string text Text to draw. Must be a valid UTF-8 byte sequence, otherwise an error is thrown
-- @treturn number Width
-- @treturn number Height
function g2d.measureText(text)
  local measurement = TextMeasurement();
  checkError(raw.tg2d_measure_text(ENGINE, lua2str(text), measurement), "g2d.measureText")
  return measurement.width, measurement.height
end

---  Lossy version of @{g2d.measureText}.
--
-- @tparam string text Text to draw. Invalid UTF-8 is replaced with the replacement character (�)
-- @treturn number Width
-- @treturn number Height
function g2d.measureTextLossy(text)
  local measurement = TextMeasurement();
  raw.tg2d_measure_text_lossy(ENGINE, lua2str(text), measurement)
  return measurement.width, measurement.height
end


--- Set the text color to use in further @{g2d.text} calls.
--
-- @tparam number r Red color component
-- @tparam number g Green color component
-- @tparam number b Blue color component
-- @tparam number a Alpha value
function g2d.setTextColor(r, g, b, a)
  raw.tg2d_set_text_color(ENGINE, r, g, b, a)
end

--- Set the background color of the rectangles drawn by @{g2d.rect} call.
--
-- @tparam number r Red color component
-- @tparam number g Green color component
-- @tparam number b Blue color component
-- @tparam number a Alpha value
function g2d.setRectColor(r, g, b, a)
  raw.tg2d_set_rect_color(ENGINE, r, g, b, a)
end

---  Set the border color of the rectangles drawn by @{g2d.rect} call.
--
-- For borders to actually show up the thickness must be set to a value
-- greater than zero (see @{g2d.setBorderThickness}).
--
-- Transparent borders are not currently supported (thus no `a` argument).
--
-- @tparam number r Red color component
-- @tparam number g Green color component
-- @tparam number b Blue color component
function g2d.setBorderColor(r, g, b)
  raw.tg2d_set_border_color(ENGINE, r, g, b)
end

---  Set the border radius for the corners of rectangles drawn by @{g2d.rect} call.
-- @tparam number radius Border radius (in pixels). Must be non-negative.
function g2d.setBorderRadius(radius)
  raw.tg2d_set_border_radius(ENGINE, radius)
end

---  Set the border thickness of the rectangles drawn by @{g2d.rect} call.
-- @tparam number thickness Border thickness (in pixels). Must be non-negative.
function g2d.setBorderThickness(thickness)
  raw.tg2d_set_border_thickness(ENGINE, thickness)
end

--- Set the font size to use in further @{g2d.text}, and @{g2d.measureText} calls.
-- @tparam number size Font size (in pixels). Must be non-negative.
function g2d.setFontSize(size)
  raw.tg2d_set_font_size(ENGINE, size)
end

--- Available font variations.
g2d.Font = {
  Normal = raw.FONT_NORMAL, -- Normal font (Fira Sans Regular).
  Monospace = raw.FONT_MONOSPACE -- Monospace font (Fira Mono Regular).
}

--- Set the font to use in further @{g2d.text}, and @{g2d.measureText} calls.
-- @tparam g2d.Font font Font
function g2d.setFont(font)
  raw.tg2d_set_font(ENGINE, font)
end

--- Get logical size of the window (not the framebuffer size).
-- @treturn number Window width
-- @treturn number Window height
function g2d.getWindowSize()
  local width, height = ffi.new("double[1]"), ffi.new("double[1]")
  raw.tg2d_window_size(ENGINE, width, height)
  return width[0], height[0]
end

--- Get window HiDPI factor.
-- @treturn number Window HiDPI factor
function g2d.getHiDPIFactor()
  return raw.tg2d_hidpi_factor(ENGINE)
end

--- Push current state on the stack to restore it later.
-- @see g2d.pop
function g2d.push()
  return raw.tg2d_push(ENGINE)
end

--- Pop state from the stack and set it as the current one.
-- @see g2d.push
function g2d.pop()
  return raw.tg2d_pop(ENGINE)
end

local loadedScripts = {}

local function loadScript(path)
  loadedScripts[path] = dofile(path)
end

local function unloadScript(path)
  loadedScripts[path] = nil
end

local function updateHandler()
  for _, script in pairs(loadedScripts) do
    if type(script) == "table" and type(script.update) == "function" then
      script.update()
    end
  end
end

return updateHandler, loadScript, unloadScript
