// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

mod raw;
mod system;

pub use self::system::{LuaSystem, Scripts};

use std::{ffi::CStr, mem::MaybeUninit, ops::Drop, path::Path};

use bstr::ByteSlice;
use failure::Fail;
use trains_api::EngineContext;

use crate::raw::VMErrorKind;

#[derive(Debug, Fail)]
pub enum VMError {
    #[fail(display = "out of memory")]
    OutOfMemory,

    #[fail(display = "LuaJIT runtime error: {}", message)]
    RuntimeError { message: String },

    #[fail(display = "LuaJIT syntax error: {}", message)]
    SyntaxError { message: String },

    #[fail(display = "LuaJIT error: {}", message)]
    Other { message: String },
}

impl From<raw::VMError> for VMError {
    fn from(raw: raw::VMError) -> VMError {
        let message = unsafe { CStr::from_ptr(raw.message) };
        let message = message.to_string_lossy().into_owned();

        let kind = raw.kind;

        unsafe { raw::vm_destroy_error(raw) };

        match kind {
            VMErrorKind::Ok => panic!("raw vm error is not an error"),
            VMErrorKind::OutOfMemory => VMError::OutOfMemory,
            VMErrorKind::RuntimeError => VMError::RuntimeError { message },
            VMErrorKind::SyntaxError => VMError::SyntaxError { message },
            VMErrorKind::OtherError => VMError::Other { message },
        }
    }
}

pub struct VM {
    raw: *mut raw::VM,
}

fn check_error(error: crate::raw::VMError) -> Result<(), VMError> {
    if error.kind == VMErrorKind::Ok {
        Ok(())
    } else {
        Err(error.into())
    }
}

impl VM {
    pub fn new() -> Result<VM, VMError> {
        let mut raw_error = MaybeUninit::zeroed();
        let vm = unsafe { raw::vm_create(raw_error.as_mut_ptr()) };

        if vm.is_null() {
            let error = unsafe { raw_error.assume_init() };
            Err(error.into())
        } else {
            Ok(VM { raw: vm })
        }
    }

    fn set_engine(&mut self, engine: &mut EngineContext) -> Result<(), VMError> {
        check_error(unsafe { raw::vm_set_engine(self.raw, engine as *mut _ as _) })
    }

    pub fn update(&mut self, engine: &mut EngineContext) -> Result<(), VMError> {
        self.set_engine(engine)?;

        check_error(unsafe { raw::vm_update(self.raw) })
    }

    pub fn load_script(
        &mut self,
        engine: &mut EngineContext,
        path: impl AsRef<Path>,
    ) -> Result<(), VMError> {
        self.set_engine(engine)?;

        let bytes = <[u8]>::from_os_str(&path.as_ref().as_os_str()).expect("Invalid path");
        check_error(unsafe { raw::vm_load_script(self.raw, bytes.as_ptr() as _, bytes.len()) })
    }

    pub fn unload_script(
        &mut self,
        engine: &mut EngineContext,
        path: impl AsRef<Path>,
    ) -> Result<(), VMError> {
        self.set_engine(engine)?;

        let bytes = <[u8]>::from_os_str(&path.as_ref().as_os_str()).expect("Invalid path");
        check_error(unsafe { raw::vm_unload_script(self.raw, bytes.as_ptr() as _, bytes.len()) })
    }
}

impl Drop for VM {
    fn drop(&mut self) {
        unsafe { raw::vm_destroy(self.raw) };
    }
}
