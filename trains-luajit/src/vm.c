/*
 * This file is part of Trains.
 * Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
 *
 * Trains is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Trains is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Trains. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <lj_ctype.h>
#include <lj_obj.h>
#include <lj_cconv.h>
#include <lj_cdata.h>
#include <lj_state.h>

extern const char *vm_get_init_script(void);
extern const char *vm_get_api_headers(void);
extern const char *vm_get_api_path(void);

typedef struct VM {
    lua_State *state;
} VM_t;

typedef enum VMErrorKind {
    VM_OK,
    VM_OUT_OF_MEMORY,
    VM_RUNTIME_ERROR,
    VM_SYNTAX_ERROR,
    VM_OTHER_ERROR
} VMErrorKind_t;

typedef struct VMError {
    VMErrorKind_t kind;
    bool is_message_owned;
    char *message;
} VMError_t;

static char *own_string(const char *str) {
    size_t len = strlen(str);
    char *new_str = malloc(len + 1);
    if (new_str == NULL)
        return NULL;

    memcpy(new_str, str, len + 1);
    return new_str;
}

static VMError_t vm_lua_error(lua_State *state, int code) {
    VMError_t error;

    switch (code) {
        case LUA_OK:
            error.kind = VM_OK;
            break;
        case LUA_ERRSYNTAX:
            error.kind = VM_SYNTAX_ERROR;
            break;
        case LUA_ERRRUN:
            error.kind = VM_RUNTIME_ERROR;
            break;
        case LUA_ERRMEM:
            error.kind = VM_OUT_OF_MEMORY;
            break;
        default:
            error.kind = VM_OTHER_ERROR;
    }

    error.message = (char *) lua_tostring(state, -1);
    if (error.message != NULL) {
        error.message = own_string(error.message);
        error.is_message_owned = true;
        lua_pop(state, 1);
    } else {
        error.message = "unknown error";
        error.is_message_owned = false;
    }

    return error;
}

static VMError_t vm_error(VMErrorKind_t kind, char *message) {
    return (VMError_t){
        .kind = kind,
        .is_message_owned = false,
        .message = message
    };
}

static VMError_t vm_success() {
    return vm_error(VM_OK, "Success");
}

void vm_destroy_error(VMError_t error) {
    if (error.is_message_owned)
        free(error.message);
}

static VMError_t load_engine_ptr(lua_State *state, void *ptr) {
    int status = luaL_dostring(state, "local ty = require('ffi').typeof('struct EngineContext *'); return ty");

    lua_pushvalue(state, 1);
    lua_copy(state, -2, 1);
    lua_remove(state, lua_gettop(state) - 1);

    if (status != 0)
        return vm_lua_error(state, 12345);

    if (lua_type(state, 1) != LUA_TCDATA)
        return vm_error(VM_OTHER_ERROR, "ffi.typeof call failed (LUA_TCDATA check)");

    GCcdata *_cd = cdataV(state->base);
    if (_cd->ctypeid != CTID_CTYPEID)
        return vm_error(VM_OTHER_ERROR, "ffi.typeof call failed (CTID_CTYPEID check)");

    CTypeID ctypeid = *(CTypeID *) cdataptr(_cd);

    CTState *cts = ctype_cts(state);
    CType *ct = ctype_raw(cts, ctypeid);

    CTSize sz;
    lj_ctype_info(cts, ctypeid, &sz);

    GCcdata *cd = lj_cdata_new(cts, ctypeid, sizeof(ptr));
    TValue *o = state->top;

    setcdataV(state, o, cd);
    lj_cconv_ct_init(cts, ct, sz, (uint8_t *) cdataptr(cd), o, 0);
    incr_top(state);

    memcpy((void *) cdataptr(cd), &ptr, sizeof(ptr));

    lua_copy(state, -2, 1);
    lua_remove(state, lua_gettop(state) - 1);

    return vm_success();
}

struct Engine;

VMError_t vm_set_engine(VM_t *vm, struct Engine *engine) {
    VMError_t error = vm_success();

    error = load_engine_ptr(vm->state, engine);

    if (error.kind == VM_OK)
        lua_setglobal(vm->state, "ENGINE");

    return error;
}

VM_t *vm_create(VMError_t *error) {
    int status;

    lua_State *state = luaL_newstate();
    if (state == NULL) {
        *error = vm_error(VM_OUT_OF_MEMORY, "Failed to create LuaJIT state");
        return NULL;
    }

    luaL_openlibs(state);

    const char *init = vm_get_init_script();
    status = luaL_loadstring(state, init);
    if (status != 0) {
        *error = vm_lua_error(state, status);
        lua_close(state);
        return NULL;
    }

    const char *headers = vm_get_api_headers();
    lua_pushstring(state, headers);

    const char *path = vm_get_api_path();
    lua_pushstring(state, path);

    // return order: unloadScript (top of the stack), loadScript, updateHandler
    status = lua_pcall(state, 2, 3, 0);
    if (status != 0) {
        *error = vm_lua_error(state, status);
        lua_close(state);
        return NULL;
    }

    VM_t *vm = malloc(sizeof(VM_t));
    if (vm == NULL)
        return NULL;

    vm->state = state;
    return vm;
}

VMError_t vm_load_script(VM_t *vm, const char *path, size_t path_len) {
    lua_State *L = vm->state;

    lua_pushvalue(L, -2);
    lua_pushlstring(L, path, path_len);

    int status = lua_pcall(L, 1, 0, 0);
    if (status != 0)
        return vm_lua_error(L, status);

    return vm_success();
}

VMError_t vm_unload_script(VM_t *vm, const char *path, size_t path_len) {
    lua_State *L = vm->state;

    lua_pushvalue(L, -1);
    lua_pushlstring(L, path, path_len);

    int status = lua_pcall(L, 1, 0, 0);
    if (status != 0)
        return vm_lua_error(L, status);

    return vm_success();
}

VMError_t vm_update(VM_t *vm) {
    lua_State *L = vm->state;

    lua_pushvalue(L, -3);

    int status = lua_pcall(L, 0, 0, 0);
    if (status != 0)
        return vm_lua_error(L, status);

    return vm_success();
}

void vm_destroy(VM_t *vm) {
    lua_close(vm->state);
}
