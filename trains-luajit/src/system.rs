use std::{
    collections::HashSet,
    path::{Path, PathBuf},
};

use specs::prelude::*;

use crate::{EngineContext, VMError, VM};

#[derive(Debug, Clone, Default)]
pub struct Scripts {
    loaded: HashSet<PathBuf>,
    to_insert: Vec<PathBuf>,
    to_remove: Vec<PathBuf>,
}

impl Scripts {
    pub fn load_script(&mut self, path: impl AsRef<Path>) {
        self.to_insert.push(path.as_ref().to_owned());
    }

    pub fn unload_script(&mut self, path: impl AsRef<Path>) {
        self.to_remove.push(path.as_ref().to_owned());
    }

    pub fn maintain(&mut self, engine: &mut EngineContext, vm: &mut VM) -> Result<(), VMError> {
        for script in self.to_insert.drain(..) {
            log::info!("Loading script {}", script.display());

            vm.load_script(engine, &script)?;
            self.loaded.insert(script);
        }

        for script in self.to_remove.drain(..) {
            log::info!("Unloading script {}", script.display());

            vm.unload_script(engine, &script)?;
            self.loaded.remove(&script);
        }

        Ok(())
    }
}

pub struct LuaSystem {
    vm: VM,
}

impl LuaSystem {
    pub fn new() -> Result<LuaSystem, VMError> {
        Ok(LuaSystem { vm: VM::new()? })
    }
}

impl<'a> RunNow<'a> for LuaSystem {
    fn run_now(&mut self, world: &World) {
        let mut scripts = Write::<Scripts>::fetch(world);
        let mut ctx = EngineContext::new(world);

        scripts
            .maintain(&mut ctx, &mut self.vm)
            .expect("Failed to maintain scripts");

        self.vm
            .update(&mut ctx)
            .expect("Error occurred during Lua update handler execution");
    }

    fn setup(&mut self, world: &mut World) {
        Write::<Scripts>::setup(world);
    }
}
