// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{env, os::raw::*};

use bstr::ByteSlice;

pub enum VM {}

#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum VMErrorKind {
    Ok,
    OutOfMemory,
    RuntimeError,
    SyntaxError,
    OtherError,
}

#[repr(C)]
pub struct VMError {
    pub kind: VMErrorKind,
    pub is_message_owned: bool,
    pub message: *mut c_char,
}

#[no_mangle]
extern "C" fn vm_get_init_script() -> *const c_char {
    concat!(include_str!("init.lua"), "\0").as_ptr() as _
}

#[no_mangle]
extern "C" fn vm_get_api_headers() -> *const c_char {
    concat!(include_str!("../../trains-api/trains.h"), "\0").as_ptr() as _
}

lazy_static::lazy_static! {
    static ref API_PATH: Vec<u8> = {
        let mut path = env::current_exe().expect("Failed to get executable path");
        path.pop();

        #[cfg(target_os = "linux")]
        path.push("libtrains_api.so");

        #[cfg(target_os = "windows")]
        path.push("trains_api.dll");

        #[cfg(target_os = "macos")]
        path.push("trains_api.dylib");

        let mut bytes = <[u8]>::from_os_str(path.as_os_str()).expect("Invalid path").to_vec();
        bytes.push(0);
        bytes
    };
}

#[no_mangle]
extern "C" fn vm_get_api_path() -> *const c_char {
    API_PATH.as_ptr() as _
}

extern "C" {
    pub fn vm_create(error: *mut VMError) -> *mut VM;
    pub fn vm_set_engine(vm: *mut VM, engine: *mut c_void) -> VMError;
    pub fn vm_update(vm: *mut VM) -> VMError;
    pub fn vm_load_script(vm: *mut VM, path: *const c_char, path_len: usize) -> VMError;
    pub fn vm_unload_script(vm: *mut VM, path: *const c_char, path_len: usize) -> VMError;
    pub fn vm_destroy(vm: *mut VM);
    pub fn vm_destroy_error(error: VMError);
}
