// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::{env, process::Command};

const LUAJIT_SRC_PATH: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/luajit2/src/");

fn build_luajit() {
    let mut cmd = Command::new("make");

    cmd.arg("BUILDMODE=static");

    if env::var("TARGET").unwrap() == "x86_64-pc-windows-gnu" {
        cmd.arg("CROSS=x86_64-w64-mingw32-");
        cmd.arg("TARGET_SYS=Windows");
    }

    let output = cmd
        .current_dir(LUAJIT_SRC_PATH)
        .output()
        .expect("failed to execute make");

    assert!(output.status.success(), "failed to build luajit");
}

fn main() {
    build_luajit();

    println!("cargo:rustc-link-search={}", LUAJIT_SRC_PATH);
    println!("cargo:rustc-link-lib=static=luajit");

    cc::Build::new()
        .file("src/vm.c")
        .include("luajit2/src/")
        .compile("trainsvm")
}
