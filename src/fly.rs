use rendy::wsi::winit::{dpi::LogicalPosition, VirtualKeyCode, WindowEvent};
use specs::{prelude::*, shrev::EventChannel};

use trains_core::{Camera, Input};
use trains_graphics::window::{WindowDimensions, WindowResource};

pub struct FlyCameraSystem {
    reader: Option<ReaderId<WindowEvent>>,
    forward: f32,
    right: f32,
    capture: bool,
    old_cursor_pos: LogicalPosition,
}

impl FlyCameraSystem {
    pub fn new() -> FlyCameraSystem {
        FlyCameraSystem {
            reader: None,
            forward: 0.0,
            right: 0.0,
            capture: false,
            old_cursor_pos: (0.0, 0.0).into(),
        }
    }
}

#[derive(SystemData)]
pub struct Data<'a> {
    camera: WriteExpect<'a, Camera>,
    events: Read<'a, EventChannel<WindowEvent>>,
    input: Read<'a, Input>,
    window: ReadExpect<'a, WindowResource>,
    dimensions: ReadExpect<'a, WindowDimensions>,
}

impl<'a> System<'a> for FlyCameraSystem {
    type SystemData = Data<'a>;

    fn run(&mut self, mut data: Self::SystemData) {
        for event in data.events.read(self.reader.as_mut().unwrap()) {
            match event {
                WindowEvent::CursorMoved { position, .. } if self.capture => {
                    let dx = (position.x - self.old_cursor_pos.x) as f32;
                    let dy = (position.y - self.old_cursor_pos.y) as f32;
                    let f = 0.001;

                    data.camera.look(dx * f, dy * f);

                    let (w, h) = data.dimensions.logical_size();
                    let pos = (w / 2.0, h / 2.0).into();
                    data.window.set_cursor_position(pos).unwrap();
                    self.old_cursor_pos = pos;

                    continue;
                }

                _ => (),
            }
        }

        if data.input.has_pressed(VirtualKeyCode::C) {
            self.capture = !self.capture;
            data.window.hide_cursor(self.capture);
            data.window.grab_cursor(self.capture).unwrap();

            let (w, h) = data.dimensions.logical_size();
            let pos = (w / 2.0, h / 2.0).into();
            data.window.set_cursor_position(pos).unwrap();
            self.old_cursor_pos = pos;
        }

        self.forward = 0.0;
        self.right = 0.0;

        if data.input.is_key_down(VirtualKeyCode::W) {
            self.forward += 1.0;
        } else if data.input.is_key_down(VirtualKeyCode::S) {
            self.forward -= 1.0;
        }

        if data.input.is_key_down(VirtualKeyCode::D) {
            self.right += 1.0;
        } else if data.input.is_key_down(VirtualKeyCode::A) {
            self.right -= 1.0;
        }

        data.camera.fly(0.02, self.forward, self.right);
    }

    fn setup(&mut self, world: &mut World) {
        Self::SystemData::setup(world);
        let mut events = Write::<EventChannel<WindowEvent>>::fetch(world);
        self.reader = Some(events.register_reader());
    }
}
