use std::time::{Duration, Instant};

use specs::prelude::*;
use sysinfo::{ProcessExt, ProcessorExt, SystemExt};

use trains_core::{DeltaTime, FramesPerSecond, Input, UpdatesPerSecond, VirtualKeyCode};
use trains_graphics::graphics2d::{Font, Graphics2D};

pub struct DebugOverlay {
    shown: bool,
    sysinfo: sysinfo::System,
    pid: u32,
    last_update: Instant,
}

impl DebugOverlay {
    pub fn new() -> DebugOverlay {
        DebugOverlay {
            shown: false,
            sysinfo: sysinfo::System::new(),
            pid: std::process::id(),
            last_update: Instant::now(),
        }
    }
}

impl Default for DebugOverlay {
    fn default() -> DebugOverlay {
        DebugOverlay::new()
    }
}

#[derive(SystemData)]
pub struct Data<'a> {
    g: Write<'a, Graphics2D>,
    input: Read<'a, Input>,
    fps: Read<'a, FramesPerSecond>,
    ups: Read<'a, UpdatesPerSecond>,
    dt: Read<'a, DeltaTime>,
}

impl<'a> System<'a> for DebugOverlay {
    type SystemData = Data<'a>;

    fn run(&mut self, data: Self::SystemData) {
        if data.input.has_pressed(VirtualKeyCode::F1) {
            self.shown = !self.shown;
            log::info!(
                "Debug overlay {}",
                if self.shown { "shown" } else { "hidden" }
            );
        }

        if !self.shown {
            return;
        }

        if self.last_update.elapsed() > Duration::from_millis(100) {
            self.sysinfo.refresh_system();
            self.sysinfo.refresh_process(self.pid as _);
            self.last_update = Instant::now();
        }

        let process = self.sysinfo.get_process(self.pid as _).unwrap();

        let mut g = data.g;
        g.set_font(Font::Monospace);
        g.set_text_color([1.0; 4]);
        g.set_font_size(16.0);

        let mut pos = 0.0;
        let mut add = |s| {
            g.text([0.0, pos], s);
            pos += 16.0;
        };

        add(format!("FPS: {}", data.fps.0));
        add(format!("UPS: {}", data.ups.0));
        add(format!("frame time: {:?}", data.dt.0));

        let (pmem_val, pmem_suff) = unbytify::bytify(process.memory() * 1024);
        let (umem_val, umem_suff) = unbytify::bytify(self.sysinfo.get_used_memory() * 1024);
        let (tmem_val, tmem_suff) = unbytify::bytify(self.sysinfo.get_total_memory() * 1024);
        add(format!(
            "mem: {} {} ({} {}/{} {})",
            pmem_val, pmem_suff, umem_val, umem_suff, tmem_val, tmem_suff
        ));

        let cpuinfo = self
            .sysinfo
            .get_processor_list()
            .iter()
            .map(|cpu| format!("{:03.1}%", cpu.get_cpu_usage() * 100.0))
            .collect::<Vec<_>>()
            .join(" ");

        add(format!("cpu: {}", cpuinfo));
    }
}
