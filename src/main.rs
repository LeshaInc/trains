// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

#[macro_use]
extern crate shred_derive;

mod cli;
mod debug;
mod fly;
mod state;

use trains_core::{Application, GpuConfig};

#[cfg(all(feature = "vulkan", not(feature = "dx12")))]
type Backend = trains_graphics::rendy::vulkan::Backend;

#[cfg(feature = "dx12")]
type Backend = trains_graphics::rendy::dx12::Backend;

fn load_config() -> GpuConfig {
    let path = std::fs::canonicalize("config.ron").unwrap_or_else(|_| "config.ron".into());

    let contents = match std::fs::read_to_string(&path) {
        Ok(v) => v,
        Err(e) => {
            log::warn!(
                "Cannot read config at {}: {}; using default",
                path.display(),
                e
            );
            return GpuConfig::default();
        }
    };

    match ron::de::from_str(&contents) {
        Ok(v) => v,
        Err(e) => {
            log::warn!("Invalid config at {}: {}; using default", path.display(), e);
            GpuConfig::default()
        }
    }
}

fn main() {
    let opts = cli::parse();

    pretty_env_logger::formatted_timed_builder()
        .parse_filters(
            &opts
                .logging
                .as_ref()
                .map(|s| s.as_str())
                .unwrap_or_else(|| "trains=info"),
        )
        .init();

    let resources = opts
        .resources
        .clone()
        .unwrap_or_else(|| {
            let mut path = std::env::current_exe().expect("Failed to get current executable path");
            path.pop();
            path.push("resources");
            path
        })
        .canonicalize()
        .expect("Failed to canonicalize resources path");

    let mut app = Application::new(state::MainState::new(resources, opts));
    while app.is_running() {
        app.update();
    }
}
