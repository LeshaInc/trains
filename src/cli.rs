use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "trains")]
pub struct Opt {
    /// Override resources path
    #[structopt(short = "r")]
    pub resources: Option<PathBuf>,

    /// Set logging filters
    #[structopt(short = "l", long = "log")]
    pub logging: Option<String>,

    /// Disable erosion (use on integrated GPUs)
    #[structopt(long = "no-erosion")]
    pub no_erosion: bool,
}

pub fn parse() -> Opt {
    Opt::from_args()
}
