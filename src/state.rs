use std::path::PathBuf;

use nalgebra::Point3;
use rendy::wsi::winit::{VirtualKeyCode, WindowEvent};
use specs::{prelude::*, shrev::EventChannel};

use trains_core::{Camera, Input, InputSystem, State, Transition};
use trains_graphics::{graphics2d::Graphics2D, RenderingSystem, WindowingSystem};
use trains_luajit::{LuaSystem, Scripts};
use trains_terrain::{TerrainLoadingSystem, WorldgenConfig};

use crate::{cli::Opt, debug::DebugOverlay, fly::FlyCameraSystem, load_config, Backend};

pub struct MainState {
    dispatcher: Dispatcher<'static, 'static>,
    reader: Option<ReaderId<WindowEvent>>,
    resources: PathBuf,
    opt: Opt,
}

impl MainState {
    pub fn new(resources: PathBuf, opt: Opt) -> MainState {
        let dispatcher = DispatcherBuilder::new()
            .with(InputSystem::new(), "input", &[])
            .with(FlyCameraSystem::new(), "fly-camera", &[])
            .with(TerrainLoadingSystem::<Backend>::new(), "load-terrain", &[])
            .with(DebugOverlay::new(), "debug", &["input"])
            .with_thread_local(WindowingSystem::new())
            .with_thread_local(LuaSystem::new().unwrap())
            .with_thread_local(RenderingSystem::<Backend>::new())
            .build();

        MainState {
            dispatcher,
            reader: None,
            resources,
            opt,
        }
    }
}

impl State for MainState {
    fn on_start(&mut self, world: &mut World) {
        let (factory, families) = trains_core::gpu::init::<Backend>(load_config());
        world.insert(factory);
        world.insert(families);

        world.insert(Camera::new_look_at(
            Point3::new(2.0, 2.0, 2.0),
            Point3::new(0.0, 0.0, 0.0),
            1.333,
        ));

        self.dispatcher.setup(world);

        let mut events = Write::<EventChannel<_>>::fetch(world);
        self.reader = Some(events.register_reader());

        let mut scripts = Write::<Scripts>::fetch(world);
        let mut path = self.resources.clone();
        path.push("scripts");
        path.push("test-script.lua");
        scripts.load_script(path);

        let mut config = Write::<WorldgenConfig>::fetch(world);
        config.enable_erosion = !self.opt.no_erosion;
    }

    fn on_stop(self: Box<Self>, world: &mut World) {
        self.dispatcher.dispose(world);
    }

    fn update(&mut self, world: &mut World) -> Transition {
        Write::<Graphics2D>::fetch(world).begin(world);
        self.dispatcher.dispatch(world);

        let input = Read::<Input>::fetch(world);
        let events = Read::<EventChannel<_>>::fetch(world);

        for event in events.read(self.reader.as_mut().unwrap()) {
            if let WindowEvent::CloseRequested = event {
                return Transition::Quit;
            }
        }

        if input.has_pressed(VirtualKeyCode::E) {
            let mut config = Write::<WorldgenConfig>::fetch(world);
            config.enable_erosion = !config.enable_erosion;
        }

        Transition::None
    }
}
