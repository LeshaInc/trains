// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use trains_core::{Bounds, Size};

use crate::{UiContext, Widget};

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct Padding {
    pub top: f32,
    pub bottom: f32,
    pub left: f32,
    pub right: f32,
}

impl Padding {
    pub const ZERO: Padding = Padding {
        top: 0.0,
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
    };

    pub fn new() -> Self {
        Self::ZERO
    }

    pub fn uniform(mut self, value: f32) -> Self {
        self.top = value;
        self.bottom = value;
        self.left = value;
        self.right = value;
        self
    }

    pub fn with_top(mut self, top: f32) -> Self {
        self.top = top;
        self
    }

    pub fn with_bottom(mut self, bottom: f32) -> Self {
        self.bottom = bottom;
        self
    }

    pub fn with_left(mut self, left: f32) -> Self {
        self.left = left;
        self
    }

    pub fn with_right(mut self, right: f32) -> Self {
        self.right = right;
        self
    }

    pub fn vertical(&self) -> f32 {
        self.top + self.bottom
    }

    pub fn horizontal(&self) -> f32 {
        self.left + self.right
    }

    pub fn inner_bounds(&self, mut outer: Bounds) -> Bounds {
        outer.x += self.left;
        outer.y += self.top;
        outer.width -= self.horizontal();
        outer.height -= self.vertical();
        outer
    }

    pub fn outer_bounds(&self, mut inner: Bounds) -> Bounds {
        inner.x -= self.top;
        inner.y -= self.left;
        inner.width += self.horizontal();
        inner.height += self.vertical();
        inner
    }
}

#[derive(Clone, Debug)]
pub struct PaddingBox<W: Widget> {
    padding: Padding,
    inner: W,
}

impl<W: Widget> PaddingBox<W> {
    pub fn new(inner: W) -> PaddingBox<W> {
        PaddingBox {
            inner,
            padding: Padding::default(),
        }
    }

    pub fn set_padding(&mut self, padding: Padding) {
        self.padding = padding;
    }

    pub fn with_padding(mut self, padding: Padding) -> Self {
        self.padding = padding;
        self
    }

    pub fn with_padding_top(mut self, padding_top: f32) -> Self {
        self.padding.top = padding_top;
        self
    }

    pub fn with_padding_bottom(mut self, padding_bottom: f32) -> Self {
        self.padding.bottom = padding_bottom;
        self
    }

    pub fn with_padding_left(mut self, padding_left: f32) -> Self {
        self.padding.left = padding_left;
        self
    }

    pub fn with_padding_right(mut self, padding_right: f32) -> Self {
        self.padding.right = padding_right;
        self
    }

    fn size_addend(&self) -> Size {
        Size::new(self.padding.horizontal(), self.padding.vertical())
    }
}

impl<W: Widget> Widget for PaddingBox<W> {
    fn draw(&mut self, c: &mut UiContext<'_>, bounds: Bounds) {
        self.inner.draw(c, self.padding.inner_bounds(bounds));
    }

    fn min_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.inner.min_size(c) + self.size_addend()
    }

    fn max_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.inner.max_size(c) + self.size_addend()
    }

    fn flex(&self) -> f32 {
        self.inner.flex()
    }
}
