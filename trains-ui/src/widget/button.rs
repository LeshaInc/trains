// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use trains_core::{Bounds, Size};

use crate::{
    widget::{Label, PaddingBox},
    UiContext, Widget,
};

#[derive(Clone, Debug)]
pub struct Button<'a> {
    inner: PaddingBox<Label<'a>>,
    has_clicked: bool,
}

impl<'a> Button<'a> {
    pub fn new(text: &'a str) -> Button<'a> {
        let inner = PaddingBox::new(Label::new(text))
            .with_padding_top(4.0)
            .with_padding_bottom(4.0)
            .with_padding_left(8.0)
            .with_padding_right(8.0);

        Button {
            inner,
            has_clicked: false,
        }
    }

    pub fn has_clicked(&self) -> bool {
        self.has_clicked
    }
}

impl<'a> Widget for Button<'a> {
    fn draw(&mut self, c: &mut UiContext<'_>, bounds: Bounds) {
        let i = c.input();

        if i.has_left_clicked() && bounds.contains(i.mouse_position()) {
            self.has_clicked = true;
        }

        c.push();

        c.set_border_radius(4.0);
        c.set_border_thickness(1.0);
        c.set_border_color([0.05; 3]);
        c.set_rect_color([0.5, 0.5, 0.5, 1.0]);
        c.rect([bounds.x, bounds.y], [bounds.width, bounds.height]);

        self.inner.draw(c, bounds);

        c.pop();
    }

    fn min_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.inner.min_size(c)
    }

    fn max_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.inner.min_size(c)
    }
}
