// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    widget::{stack::StackContainer, BoxWidget, VerticalStack},
    ContainerFiller, Vertical, Widget,
};

pub type WindowFiller<'a, 'c> = ContainerFiller<'a, 'c, StackContainer<Vertical>>;

pub fn create_window<'a, F>(title: &'a str, mut factory: F) -> impl Widget + 'a
where
    for<'b, 'c> F: FnMut(WindowFiller<'b, 'c>) + 'a,
{
    let stack = VerticalStack::new(move |mut ui| {
        ui.horizontal_stack(|mut ui| {
            ui.label(title);
            ui.spacer(1.0);
            ui.fixed_spacer(20.0);
            ui.label("X")
        });
        ui.vertical_stack(&mut factory);
    });

    BoxWidget::new(stack)
}
