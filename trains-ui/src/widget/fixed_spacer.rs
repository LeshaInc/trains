// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::marker::PhantomData;
use trains_core::{Bounds, Size};

use crate::{AlignedSize, Orientation, UiContext, Widget};

#[derive(Clone, Debug)]
pub struct FixedSpacer<O: Orientation> {
    size: f32,
    _marker: PhantomData<O>,
}

impl<O: Orientation> Default for FixedSpacer<O> {
    fn default() -> FixedSpacer<O> {
        FixedSpacer {
            size: 0.0,
            _marker: PhantomData,
        }
    }
}

impl<O: Orientation> FixedSpacer<O> {
    pub fn new(size: f32) -> FixedSpacer<O> {
        FixedSpacer {
            size,
            _marker: PhantomData,
        }
    }
}

impl<O: Orientation> Widget for FixedSpacer<O> {
    fn draw(&mut self, _c: &mut UiContext<'_>, _bounds: Bounds) {}

    fn min_size(&mut self, _c: &mut UiContext<'_>) -> Size {
        O::unalign(AlignedSize::new(self.size, 0.0))
    }

    fn max_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.min_size(c)
    }
}
