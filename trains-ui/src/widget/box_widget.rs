// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use trains_core::{Bounds, Size};

use crate::{widget::PaddingBox, UiContext, Widget};

#[derive(Clone, Debug)]
pub struct BoxWidget<I: Widget> {
    inner: PaddingBox<I>,
}

impl<I: Widget> BoxWidget<I> {
    pub fn new(inner: I) -> BoxWidget<I> {
        let inner = PaddingBox::new(inner);
        BoxWidget { inner }
    }
}

impl<I: Widget> Widget for BoxWidget<I> {
    fn draw(&mut self, c: &mut UiContext<'_>, bounds: Bounds) {
        let style = *c.style();
        self.inner.set_padding(style.box_padding);

        c.push();

        c.set_border_radius(style.box_border_radius);
        c.set_border_thickness(style.box_border_thickness);
        c.set_border_color(style.box_border_color);
        c.set_rect_color(style.box_background);
        c.rect([bounds.x, bounds.y], [bounds.width, bounds.height]);

        self.inner.draw(c, bounds);

        c.pop();
    }

    fn min_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.inner.set_padding(c.style().box_padding);
        self.inner.min_size(c)
    }

    fn max_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.inner.set_padding(c.style().box_padding);
        self.inner.min_size(c)
    }
}
