// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use trains_core::Bounds;

use crate::{UiContext, Widget};

#[derive(Clone, Debug)]
pub struct Spacer {
    flex: f32,
}

impl Default for Spacer {
    fn default() -> Spacer {
        Spacer { flex: 1.0 }
    }
}

impl Spacer {
    pub fn new(flex: f32) -> Spacer {
        Spacer { flex }
    }
}

impl Widget for Spacer {
    fn draw(&mut self, _c: &mut UiContext<'_>, _bounds: Bounds) {}

    fn flex(&self) -> f32 {
        self.flex
    }
}
