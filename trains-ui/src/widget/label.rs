// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use trains_core::{Bounds, Size};

use crate::{UiContext, Widget};

#[derive(Clone, Debug)]
pub struct Label<'a> {
    text: &'a str,
}

impl<'a> Label<'a> {
    pub fn new(text: &'a str) -> Label<'a> {
        Label { text }
    }
}

impl<'a> Widget for Label<'a> {
    fn draw(&mut self, c: &mut UiContext<'_>, bounds: Bounds) {
        let height = bounds.height;
        let offset = height * (c.style().line_height - 1.2) * 0.5;
        c.text([bounds.x, bounds.y + offset], self.text);
    }

    fn min_size(&mut self, c: &mut UiContext<'_>) -> Size {
        c.measure_text(&self.text)
            .map_height(|h| h * c.style().line_height)
    }

    fn max_size(&mut self, c: &mut UiContext<'_>) -> Size {
        self.min_size(c)
    }
}
