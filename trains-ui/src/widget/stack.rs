// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::f32::EPSILON;

use smallvec::SmallVec;
use trains_core::{Bounds, Size};

use crate::{
    AlignedContainer, AlignedSize, Axis, Container, ContainerFiller, Horizontal, Orientation,
    UiContext, Vertical, Widget,
};

pub type VerticalStack<F> = Stack<Vertical, F>;
pub type HorizontalStack<F> = Stack<Horizontal, F>;

#[derive(Clone, Copy, Debug)]
struct MinMax {
    min_size: AlignedSize,
    max_size: AlignedSize,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum State {
    Initial,
    Draw,
}

impl Default for State {
    fn default() -> State {
        State::Initial
    }
}

#[derive(Debug, Default)]
pub struct StackContainer<O: Orientation> {
    _orientation: O,
    state: State,
    bounds: Bounds,
    min_size: AlignedSize,
    max_size: AlignedSize,
    child_sizes: SmallVec<[AlignedSize; 4]>,
    child_max_sizes: SmallVec<[AlignedSize; 4]>,
    child_flexes: SmallVec<[f32; 4]>,
    index: usize,
    cursor: f32,
    flex_sum: f32,
}

impl<O: Orientation> StackContainer<O> {
    fn handle_child_initial(&mut self, c: &mut UiContext<'_>, child: &mut impl Widget) {
        let min_size = O::align(child.min_size(c));
        let max_size = O::align(child.max_size(c));

        self.child_sizes.push(min_size);
        self.child_max_sizes.push(max_size);
        self.min_size.stack(min_size);
        self.max_size.stack(max_size);
        self.cursor += min_size.major();

        let flex = child.flex();
        self.child_flexes.push(flex);
        self.flex_sum += flex;
    }

    fn transition_draw(&mut self) {
        let container_size = O::align(self.bounds.size());
        let mut free_space = (container_size.major() - self.cursor).max(0.0);

        for _ in 0..1 {
            let unit = free_space / self.flex_sum;

            for ((size, &max_size), &flex) in self
                .child_sizes
                .iter_mut()
                .zip(&self.child_max_sizes)
                .zip(&self.child_flexes)
            {
                free_space -= unit * flex;

                if flex > EPSILON {
                    *size.major_mut() += unit * flex;
                }

                *size.minor_mut() = container_size.minor();
                size.0 = size.min(max_size.0);
            }
        }

        self.state = State::Draw;
        self.index = 0;
        self.cursor = 0.0;
    }

    fn handle_child_draw(&mut self, c: &mut UiContext<'_>, child: &mut impl Widget) {
        let mut bounds = self.bounds;
        let size = self.child_sizes[self.index];

        match O::MAJOR_AXIS {
            Axis::Horizontal => bounds.x += self.cursor,
            Axis::Vertical => bounds.y += self.cursor,
        }

        bounds.set_size(O::unalign(size));
        child.draw(c, bounds);

        self.cursor += size.major();
        self.index += 1;
    }
}

impl<O: Orientation> Container for StackContainer<O> {
    fn handle_child(&mut self, c: &mut UiContext<'_>, child: &mut impl Widget) {
        match self.state {
            State::Initial => self.handle_child_initial(c, child),
            State::Draw => self.handle_child_draw(c, child),
        }
    }
}

impl<O: Orientation> AlignedContainer for StackContainer<O> {
    type Orientation = O;
}

#[derive(Debug)]
pub struct Stack<O, F>
where
    O: Orientation,
    F: FnMut(ContainerFiller<StackContainer<O>>),
{
    factory: F,
    container: StackContainer<O>,
}

impl<O, F> Stack<O, F>
where
    O: Orientation,
    F: FnMut(ContainerFiller<StackContainer<O>>),
{
    pub fn new(factory: F) -> Self {
        Stack {
            factory,
            container: StackContainer::default(),
        }
    }

    fn handle_children(&mut self, c: &mut UiContext<'_>) {
        (self.factory)(ContainerFiller::new(c, &mut self.container));
    }
}

impl<O, F> Widget for Stack<O, F>
where
    O: Orientation,
    F: FnMut(ContainerFiller<StackContainer<O>>),
{
    fn draw(&mut self, c: &mut UiContext<'_>, bounds: Bounds) {
        self.container.bounds = bounds;

        if self.container.state == State::Initial {
            self.handle_children(c);
        }

        self.container.transition_draw();
        self.handle_children(c);
    }

    fn min_size(&mut self, c: &mut UiContext<'_>) -> Size {
        if self.container.state == State::Initial {
            self.handle_children(c);
        }

        O::unalign(self.container.min_size)
    }

    fn max_size(&mut self, c: &mut UiContext<'_>) -> Size {
        if self.container.state == State::Initial {
            self.handle_children(c);
        }

        O::unalign(self.container.max_size)
    }
}
