// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use crate::{
    widget::{
        stack::StackContainer, Button, FixedSpacer, HorizontalStack, Label, Spacer, VerticalStack,
        Widget,
    },
    Horizontal, Orientation, StyleBuilder, UiContext, UiStyle, Vertical,
};

pub trait Container {
    fn handle_child(&mut self, c: &mut UiContext<'_>, child: &mut impl Widget);
}

pub trait AlignedContainer: Container {
    type Orientation: Orientation;
}

pub struct ContainerFiller<'a, 'c, C: Container> {
    context: &'a mut UiContext<'c>,
    container: &'a mut C,
}

impl<'a, 'c, C: Container> ContainerFiller<'a, 'c, C> {
    pub fn new(context: &'a mut UiContext<'c>, container: &'a mut C) -> ContainerFiller<'a, 'c, C> {
        ContainerFiller { context, container }
    }

    pub fn style_scope<'f>(&'f mut self) -> StyleScope<'f, 'a, 'c, C> {
        StyleScope::new(self)
    }

    pub fn style(&self) -> &UiStyle {
        self.context.style()
    }

    pub fn style_mut(&mut self) -> &mut UiStyle {
        self.context.style_mut()
    }

    pub fn set_style(&mut self, style: UiStyle) {
        self.context.set_style(style);
    }

    pub fn apply_style(&mut self) {
        self.context.apply_style();
    }

    pub fn widget(&mut self, widget: &mut impl Widget) {
        self.container.handle_child(self.context, widget);
    }

    pub fn label(&mut self, text: impl AsRef<str>) {
        self.widget(&mut Label::new(text.as_ref()));
    }

    pub fn button(&mut self, text: impl AsRef<str>) -> bool {
        let mut button = Button::new(text.as_ref());
        self.widget(&mut button);
        button.has_clicked()
    }

    pub fn spacer(&mut self, flex: f32) {
        self.widget(&mut Spacer::new(flex));
    }

    pub fn vertical_stack<F>(&mut self, factory: F)
    where
        F: FnMut(ContainerFiller<StackContainer<Vertical>>),
    {
        self.widget(&mut VerticalStack::new(factory));
    }

    pub fn horizontal_stack<F>(&mut self, factory: F)
    where
        F: FnMut(ContainerFiller<StackContainer<Horizontal>>),
    {
        self.widget(&mut HorizontalStack::new(factory));
    }
}

impl<'a, 'c, C: AlignedContainer> ContainerFiller<'a, 'c, C> {
    pub fn fixed_spacer(&mut self, size: f32) {
        self.widget(&mut FixedSpacer::<C::Orientation>::new(size));
    }
}

// TODO: less lifetimes
pub struct StyleScope<'f, 'a, 'c, C: Container> {
    filler: &'f mut ContainerFiller<'a, 'c, C>,
    old_style: UiStyle,
}

impl<'f, 'a, 'c, C: Container> StyleScope<'f, 'a, 'c, C> {
    pub(crate) fn new(filler: &'f mut ContainerFiller<'a, 'c, C>) -> Self {
        let old_style = *filler.style();
        StyleScope { filler, old_style }
    }

    pub fn run(self, f: impl FnOnce(&mut ContainerFiller<C>)) {
        self.filler.apply_style();
        f(self.filler);
        self.filler.set_style(self.old_style);
        self.filler.apply_style();
    }
}

impl<'f, 'a, 'c, C: Container> StyleBuilder for StyleScope<'f, 'a, 'c, C> {
    fn style_mut(&mut self) -> &mut UiStyle {
        self.filler.style_mut()
    }
}
