// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::ops::{Deref, DerefMut};

use trains_core::Input;
use trains_graphics::Graphics2D;

use crate::UiStyle;

#[derive(Debug)]
pub struct UiContext<'a> {
    graphics: &'a mut Graphics2D,
    input: &'a Input,
    style: UiStyle,
}

impl<'a> UiContext<'a> {
    pub fn new(graphics: &'a mut Graphics2D, input: &'a Input) -> UiContext<'a> {
        let style = UiStyle::default();
        style.apply(graphics);

        UiContext {
            graphics,
            style,
            input,
        }
    }

    pub fn input(&self) -> &Input {
        self.input
    }

    pub fn style(&self) -> &UiStyle {
        &self.style
    }

    pub fn style_mut(&mut self) -> &mut UiStyle {
        &mut self.style
    }

    pub fn set_style(&mut self, style: UiStyle) {
        self.style = style;
    }

    pub fn apply_style(&mut self) {
        self.style.apply(self.graphics);
    }
}

impl<'a> Deref for UiContext<'a> {
    type Target = Graphics2D;

    fn deref(&self) -> &Self::Target {
        &self.graphics
    }
}

impl<'a> DerefMut for UiContext<'a> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.graphics
    }
}
