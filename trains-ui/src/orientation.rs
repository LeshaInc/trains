// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Debug;

use trains_core::Size;

#[derive(Clone, Copy, Debug, Default, PartialEq, NewType)]
pub struct AlignedSize(pub Size);

impl AlignedSize {
    pub const ZERO: AlignedSize = AlignedSize(Size::ZERO);
    pub const INFINITE: AlignedSize = AlignedSize(Size::INFINITE);

    pub fn new(width: f32, height: f32) -> AlignedSize {
        AlignedSize(Size { width, height })
    }

    pub fn major(self) -> f32 {
        self.0.width
    }

    pub fn major_mut(&mut self) -> &mut f32 {
        &mut self.0.width
    }

    pub fn set_major(&mut self, v: f32) {
        self.0.width = v;
    }

    pub fn minor(self) -> f32 {
        self.0.height
    }

    pub fn minor_mut(&mut self) -> &mut f32 {
        &mut self.0.height
    }

    pub fn set_minor(&mut self, v: f32) {
        self.0.height = v;
    }

    pub fn stack(&mut self, other: AlignedSize) {
        *self.major_mut() += other.major();
        self.set_minor(self.minor().max(other.minor()));
    }
}

pub enum Axis {
    Vertical,
    Horizontal,
}

pub trait Orientation: Clone + Copy + Debug + Default + 'static {
    const MAJOR_AXIS: Axis;

    fn align(size: Size) -> AlignedSize {
        match Self::MAJOR_AXIS {
            Axis::Vertical => AlignedSize::new(size.height, size.width),
            Axis::Horizontal => AlignedSize::new(size.width, size.height),
        }
    }

    fn unalign(size: AlignedSize) -> Size {
        match Self::MAJOR_AXIS {
            Axis::Vertical => Size::new(size.height, size.width),
            Axis::Horizontal => Size::new(size.width, size.height),
        }
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Vertical;

impl Orientation for Vertical {
    const MAJOR_AXIS: Axis = Axis::Vertical;
}

#[derive(Copy, Clone, Debug, Default)]
pub struct Horizontal;

impl Orientation for Horizontal {
    const MAJOR_AXIS: Axis = Axis::Horizontal;
}
