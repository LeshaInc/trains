// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

pub mod box_widget;
pub mod button;
pub mod fixed_spacer;
pub mod label;
pub mod padding;
pub mod spacer;
pub mod stack;
pub mod window;

pub use self::{
    box_widget::BoxWidget,
    button::Button,
    fixed_spacer::FixedSpacer,
    label::Label,
    padding::{Padding, PaddingBox},
    spacer::Spacer,
    stack::{HorizontalStack, Stack, VerticalStack},
    window::create_window,
};

use trains_core::{Bounds, Size};

use crate::UiContext;

pub trait Widget {
    fn draw(&mut self, c: &mut UiContext<'_>, bounds: Bounds);

    fn min_size(&mut self, _c: &mut UiContext<'_>) -> Size {
        Size::ZERO
    }

    fn max_size(&mut self, _c: &mut UiContext<'_>) -> Size {
        Size::INFINITE
    }

    fn flex(&self) -> f32 {
        0.0
    }
}
