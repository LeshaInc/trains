// This file is part of Trains.
// Copyright (C) 2019 LeshaInc <includeurl@gmail.com>
//
// Trains is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Trains is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Trains. If not, see <https://www.gnu.org/licenses/>.

use crate::widget::Padding;

use trains_graphics::Graphics2D;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct UiStyle {
    pub font_size: f32,
    pub text_color: [f32; 4],
    pub line_height: f32,
    pub box_padding: Padding,
    pub box_background: [f32; 4],
    pub box_border_color: [f32; 3],
    pub box_border_thickness: f32,
    pub box_border_radius: f32,
}

impl Default for UiStyle {
    fn default() -> UiStyle {
        UiStyle {
            font_size: 16.0,
            text_color: [0.0, 0.0, 0.0, 1.0],
            line_height: 1.2,
            box_padding: Padding::new().uniform(10.0),
            box_background: [1.0, 1.0, 1.0, 0.5],
            box_border_color: [1.0; 3],
            box_border_thickness: 0.0,
            box_border_radius: 8.0,
        }
    }
}

impl UiStyle {
    pub fn new() -> UiStyle {
        UiStyle::default()
    }

    pub fn apply(&self, g: &mut Graphics2D) {
        g.set_font_size(self.font_size);
        g.set_text_color(self.text_color);
    }
}

pub trait StyleBuilder: Sized {
    fn style_mut(&mut self) -> &mut UiStyle;

    fn with_style(mut self, style: &UiStyle) -> Self {
        *self.style_mut() = *style;
        self
    }

    fn with_font_size(mut self, font_size: f32) -> Self {
        self.style_mut().font_size = font_size;
        self
    }

    fn with_text_color(mut self, text_color: [f32; 4]) -> Self {
        self.style_mut().text_color = text_color;
        self
    }
}

impl StyleBuilder for UiStyle {
    fn style_mut(&mut self) -> &mut UiStyle {
        self
    }
}
